<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//web
Route::get('/', 'Web\HomeController@index')->name('home');
Route::get('/whatis', 'Web\HomeController@whatis')->name('whatis');
Route::get('/howtowork', 'Web\HomeController@howtowork')->name('howtowork');
Route::get('/services', 'Web\HomeController@services')->name('services');
Route::get('stepone', 'Web\HomeController@stepone')->name('stepone');
Route::get('/selecthearts', 'Web\HomeController@selecthearts')->name('selecthearts');
Route::get('/ContactUs', 'Web\ContactUsController@index')->name('ContactUs');
Route::get('/select_season/{id?}', 'Web\HomeController@select_season')->name('home.select_season');
Route::get('/select_eyd/{title?}', 'Web\HomeController@select_eyd')->name('home.select_eyd');
Route::get('/select_age/{title?}/{id?}', 'Web\HomeController@select_age')->name('home.select_age');
Route::get('/basket_shop', 'Web\HomeController@Basket_shop')->name('home.Basket_shop');
Route::get('/Login', 'Web\LoginController@index')->name('home.Login');
Route::post('/Login', 'Web\LoginController@login')->name('Login');
Route::get('/select_products/{id?}', 'Web\select_productsController@index')->name('select_products');
Route::get('/select_products_season', 'Web\select_productsController@season')->name('select_products_season');
Route::get('/occasion', 'Web\OccasionController@index')->name('occasion');
Route::get('/occasion_description', 'Web\OccasionController@occasion_description')->name('occasion_description');
Route::get('/occasion_about', 'Web\OccasionController@occasion_about')->name('occasion_about');
Route::get('/select_products/{id?}', 'Web\select_productsController@index')->name('select_products');
Route::get('/select_all_products', 'Web\select_all_productsController@index')->name('all_products');
Route::get('/comment/{id?}', 'Web\CommentController@index')->name('nazarat');
Route::get('/custom/{id?}', 'Web\CustomController@index')->name('custom');
Route::get('/AllCard', 'Web\AllCardController@index')->name('all_card');
Route::get('/Customize_2', 'Web\Customize_2Controller@index')->name('Customize_2');
Route::get('/Basket_shop', 'Web\Basket_shopController@index')->name('Basket_shop');
//Route::get('/Basket_shop.hedyeajax/{id?}', 'Web\Basket_shopController@hedyeajax')->name('Basket_shop.idajax');
Route::get('/Paid', 'Web\PaidController@index')->name('Paid');
Route::get('/Kado', 'Web\Kado_Controller@index')->name('Web.Kado');
Route::get('/Register', 'Web\RegisterController@index')->name('home.Register');
Route::post('/Register', 'Web\RegisterController@Register');
Route:: get('/getcity/{id?}', 'Web\HomeController@load_city');
Route:: get('/savecity/{id?}', 'Web\HomeController@savecity');
Route:: get('/saveKado/{id?}', 'Web\HomeController@saveKado');
Route:: get('/saveprice/{id?}', 'Web\HomeController@savecharge');
Route:: get('/codeoff/{id?}', 'Web\HomeController@codeoff');


Route::get('/about_us', function () {
    return view('Web.about_us');
});

Route::get('/contact_us', function () {
    return view('Web.contact_us');
});
//end web

//Admin
Route:: group(['prefix' => 'Admin', 'namespace' => 'Admin'], function () {
    ///ABOUT
    Route:: get('/AboutUs', 'AboutUsController@index')->name('Admin.AboutUs');
    Route:: get('/DateAbout/{id?}', 'AboutUsController@DateAbout')->name('Admin.DateAbout');
    Route:: post('/AboutUs', 'AboutUsController@EditAbout')->name('Admin.EditAbout');
    ///CONTACT
    Route:: post('/ContactUs', 'ContactUsController@EditContact')->name('Admin.EditContact');
    Route:: get('/DateContact/{id?}', 'ContactUsController@DateContact')->name('Admin.DateContact');
    ///DASHBOARD
    Route:: get('/Dashboard', 'DashboardController@index')->name('Admin.Dashboard');
    ///GiftSHOP
    Route:: post('/GiftShop', 'CadoShopController@EditCadoShop')->name('Admin.EditCado');
    Route:: get('/DataCadoShop/{id?}', 'CadoShopController@DataCadoShop')->name('Admin.DataCadoShop');
    ///MiddleManagers
    Route:: get('/MiddleManagers', 'ListMiddleManagersController@index')->name('Admin.MiddleManagers');
    Route:: get('/manager_edit/{id?}', 'ListMiddleManagersController@load_info')->name('Admin.show_manager');
    Route:: get('/manager_delete/{id?}', 'ListMiddleManagersController@manager_delete')->name('Admin.delete_manager');
    Route:: post('/MiddleManagers', 'ListMiddleManagersController@create_manager')->name('Admin.MiddleManagers');
    Route:: post('/MiddleManagers_edit', 'ListMiddleManagersController@MiddleManagers_edit')->name('Admin.MiddleManagers_edit');
    Route:: get('/change_pass_manager/{user_id?}/{user_pass?}', 'ListMiddleManagersController@change_pass_manager')->name('Admin.MiddleManagers_edit_pass');
    ///Orders
    Route:: get('/Orders', 'ListOrdersController@index')->name('Admin.Orders');

    Route:: get('/DeleteOrders/{id?}', 'ListOrdersController@DeleteOrders')->name('Admin.DeleteOrders');
    ///Transaction
    Route:: get('/Transaction', 'ListTransactionController@index')->name('Admin.Transaction');
    //ManageSEND
    Route:: get('/Managesend', 'ManagersSendPriceController@index')->name('Admin.ManageSEND');
    Route:: post('/Managesend', 'ManagersSendPriceController@add_code_off')->name('Admin.Managesend_add');
    Route:: get('/Managesend_edit/{id?}', 'ManagersSendPriceController@edit_code_off')->name('Admin.EditSEND');
    Route:: post('/Manage_code_of_edit/{id?}', 'ManagersSendPriceController@code_off_edit')->name('Admin.code_off_edit');
    Route:: get('/Managesend_delete/{id?}', 'ManagersSendPriceController@delete_code_off')->name('Admin.delete_code_off');
    //send message and email
    Route:: get('/Manage_message', 'Manage_messageController@index')->name('Admin.Manage_message');
    Route:: post('/Manage_message', 'Manage_messageController@mail')->name('Admin.Manage_message');
    Route:: post('/Manage_sms', 'Manage_messageController@send_sms')->name('Admin.Manage_sms');


    ///ListUser
    Route:: get('/ListUser', 'ListUserController@index')->name('Admin.ListUser');
    Route:: get('/ListUser/Delete/{id?}', 'ListUserController@DeleteUser')->name('Admin.DTU');
    Route:: get('/ListUser/Change/{user_id?}/{status?}', 'ListUserController@ChangeStatus')->name('Admin.CHS');
    Route:: post('/ListUser', 'ListUserController@UpData')->name('Admin.ListUser.post');
    Route:: get('/DataUser/{id?}', 'ListUserController@DataUserAjax')->name('Admin.DataUser.ajax');
    ///Advertising
    Route:: get('/Advertising', 'ManagersAdvertisingController@index')->name('Admin.Advertising');
    Route:: post('/Advertising', 'ManagersAdvertisingController@edit_adv')->name('Admin.Advertising_edt');
    Route:: post('/Advertising_add', 'ManagersAdvertisingController@add_adv')->name('Admin.Advertising_add');
    Route:: get('/load_advertising/{id?}', 'ManagersAdvertisingController@load_advertising')->name('Admin.Advertising_edit');
    Route:: get('/Advertising/{id?}', 'ManagersAdvertisingController@Advertising_delete')->name('Admin.Advertising_delete');
    ///AgeCategory
    Route:: get('/AgeCategory', 'ManagersAgeCategoryController@index')->name('Admin.AgeCategory');
    Route:: get('/AgeData/{id?}', 'ManagersAgeCategoryController@AgeData')->name('Admin.AgeData');
    Route:: post('/AgeCategory', 'ManagersAgeCategoryController@Add_age')->name('Admin.AgeCategory_add');
    Route:: post('/AgeEdit', 'ManagersAgeCategoryController@AgeEdit')->name('Admin.AgeEdit');
    Route:: get('/AgeCategory/{id?}', 'ManagersAgeCategoryController@age_delete')->name('Admin.AgeCategory_delete');
    ///Cards
    Route:: get('/Cards', 'ManagersCardsController@index')->name('Admin.Cards');
    Route:: get('/DataCard/{id?}', 'ManagersCardsController@DataCardAjax')->name('Admin.DataCardAjax');
    Route:: post('/AddCard', 'ManagersCardsController@AddCard')->name('Admin.AddCard');
    Route:: post('/EditCard', 'ManagersCardsController@UpData')->name('Admin.EditCard');
    Route:: get('/DeleteCard/{id?}', 'ManagersCardsController@DeleteCard')->name('Admin.DeleteCard');
    ///Category
    Route:: get('/Category', 'ManagersCategoryController@index')->name('Admin.Category');
    Route:: post('/Category', 'ManagersCategoryController@Add_cat')->name('Admin.Category');
    Route:: post('/EditCat', 'ManagersCategoryController@EditCat')->name('Admin.CategoryEdit');
    Route::get('/CategoryData/{id?}', 'ManagersCategoryController@CategoryData')->name('Admin.CategoryData');
    Route:: get('/Child_cat/{id?}', 'ManagersCategoryController@load_child')->name('Admin.Category_ajax');
    Route:: get('/Category/{id?}', 'ManagersCategoryController@delete')->name('Admin.Category_delete');
    ///Comment
    Route:: get('/Comment', 'ManagersCommentController@index')->name('Admin.Comment');
    Route:: get('/ChangeComment/{id?}/{status?}', 'ManagersCommentController@ChangeComment')->name('Admin.ChangeComment');
    Route:: get('/DeleteComment/{id?}', 'ManagersCommentController@DeleteComment')->name('Admin.DeleteComment');
    ///Occasion
    Route:: get('/Occasion', 'ManagersOccasionController@index')->name('Admin.Occasion');
    Route:: post('/Occasion', 'ManagersOccasionController@Add_occasion')->name('Admin.Occasion');
    Route:: get('/load_parity/{id?}/{priority?}', 'ManagersOccasionController@load_parity')->name('Admin.Occasion_load_parity');
    Route:: get('/load_info', 'ManagersOccasionController@load_info')->name('Admin.load_info');
    Route:: get('/Occasion/{id?}', 'ManagersOccasionController@delete_occasion')->name('Admin.Occasion_delete');
    ///TextCard
    Route:: get('/TextCard', 'ManagersTextCardController@index')->name('Admin.TextCard');
    Route:: post('/TextCard', 'ManagersTextCardController@Add_sentece')->name('Admin.TextCard');
    Route:: post('/TextCard_edit', 'ManagersTextCardController@edit_sentece')->name('Admin.TextCard_edit_modal');
    Route:: get('/TextCard/{id?}', 'ManagersTextCardController@TextCard_delete')->name('Admin.TextCard_delete');
    Route:: get('/TextCard_edit/{id?}', 'ManagersTextCardController@TextCard_edit')->name('Admin.TextCard_edit');
    ///TypeCategory
    Route:: get('/TypeCategory', 'ManagersTypeCategoryController@index')->name('Admin.TypeCategory');
    ///Services
    Route:: get('/Services', 'ServicesController@index')->name('Admin.Services');

    //

});

//end Admin

//user
Route::get('/user', 'user\Controller@index')->name('user.index');

//end user




