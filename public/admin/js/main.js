//////////////////////////////////////////////////////////////


$(document).ready(function () {


// toggle-nav
    $('.toggle-nav').click(function (e) {
        $(this).toggleClass('active');
        $('.menu ul').toggleClass('active');

        e.preventDefault();
    });


// Sidebar
    $('.SidebarTxt').css('display', 'none');
    $('#closeSidebar').hide();
    $('.sidebar-btn').click(function () {
        if ($('#SIDEBAR').width() > 50) {
            $('#CONTENT').css('margin-right', '50px');
            $('.SidebarTxt').hide();
            $('#openSidebar').show();
            $('#closeSidebar').hide();
            $('#SIDEBAR').animate({width: '50px'})
        }
        else {
            $('#CONTENT').css('margin-right', '270px');
            $('.SidebarTxt').show();
            $('#openSidebar').hide();
            $('#closeSidebar').show();
            $('#SIDEBAR').animate({width: '270px'})
        }
    });


    $(document).on("click", "#load_modal_delete_cls", function (e) {
        var link = $(this).data('link');
        $("#delete_user_ajax").attr('href', link);
    });
    $(document).on("change", ".code_off_ajax", function (e) {

        if ($(".code_off_ajax").val() < 0 || $(".code_off_ajax").val() > 100) {
            $("#msg_pass_id").html('عدد وارد شده باید بین 0 تا 100 باشد ...');
            e.preventDefault();
        }else{
            $(".msg_error").html('');
        }


    });

    $(document).on("change", "#code_off_by_ajax", function (e) {

        if ($("#code_off_by_ajax").val() < 0 || $("#code_off_by_ajax").val() > 100) {
            $("#msg_pass_id").html('عدد وارد شده باید بین 0 تا 100 باشد ...');
            e.preventDefault();
        }else{
            $(".msg_error").html('');
        }


    });

    $(document).on("change", ".col_id_by_ajax", function () {
        var id = $(this).data("id");
        var priority = $(this).val();
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/load_parity/" + id + '/' + priority,
            type: "GET",

            success: function (data) {
            },
            error: function (e) {
                alert("error");
            }
        });

    });

    $(document).on("click", "#edit_code_off_by_ajax", function () {
        var id = $(this).data("id");
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/Managesend_edit/" + id,
            type: "GET",

            success: function (data) {
                data = (JSON.parse(data));
                $("#code_off_by_ajax").val(data['codeoff_percent']);
                $("#code_off_ajax_id").val(data['codeoff_id']);
            },
            error: function (e) {
                alert("error");
            }
        });

    });

    $(document).on("click", "#show_advertising_by_ajax", function () {
        var id = $(this).data("id");
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/load_advertising/" + id,
            type: "GET",

            success: function (data) {
                data = (JSON.parse(data));
                var img = "../admin/uploads/Advertising/" + data['Ads_image'];
                $("#position_by_ajax").val(data['Ads_address']);
                $("#Ads_image").attr('src', img);
                $("#advertising_ajax_id").val(data['adv_id']);
                // alert(data);
            },
            error: function (e) {
                alert("error");
            }
        });

    });

    $(document).on("click", "#edit_sentence_by_ajax", function () {
        var id = $(this).data("id");

        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/TextCard_edit/" + id,
            type: "GET",

            success: function (data) {

                data = (JSON.parse(data));
                $("#sentence_by_ajax").val(data['Sentences_title']);
                $("#sentence_ajax_id").val(data['Sentences_id']);

            },
            error: function (e) {
                alert("error");
            }
        });

    });

    $(document).on("change", ".load_info_by_ajax", function () {
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/load_info",
            type: "GET",

            success: function (data) {
                var str = "<tr><tddata-title = 'ردیف'></td><td data-title = 'نام'></td><tddata-title = 'اولویت بندی'><input type='number' class='input-form wide' value='' id='col_id_by_ajax_in_filenab' data-id='' ></td></tr>";

                $("#first_show_ajax").hide();
                $("#last_show_id").show();
                $("#last_show_id").html(data);

            },
            error: function (e) {
                alert("error");
            }
        });

    });


    $(".child_category_ajax").hide();

    $(document).on("change", ".parent_cat_ajax", function (e) {
        var id = $(".parent_cat_ajax").val();

        if (id == 0) {
            $(".child_category_ajax").hide();
            $(".show_or_hide_by_ajax").show();

        } else {
            $.ajax({
                url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/Child_cat/" + id,
                type: "GET"
                //  ty: token
                ,

                success: function (data) {
                    data = (JSON.parse(data));
                    if ((data) === '') {
                        $(".child_category_ajax").hide();
                        $(".show_or_hide_by_ajax").hide();
                    } else {
                        $(".child_category_ajax").show();
                        $(".show_or_hide_by_ajax").hide();
                        $("#child_of_parent_cat").html(data);
                    }


                }
                ,
                error: function (e) {
                    alert("خطا در بارگذاری...");
                }
            });

        }

    });

    $(document).on("click", "#select_all_by_ajax", function (e) {

        if ($(this).is(':checked')) {
            $(".check_cmt").each(function () {
                $(this).prop('checked', 'checked');
            });
        }
        else {
            $(".check_cmt").each(function () {
                $(this).prop('checked', '');
            });
        }
    });

    $(document).on("click", ".select_all_by_ajax", function (e) {

        if ($(this).is(':checked')) {
            $(".check_cmt").each(function () {
                $(this).prop('checked', 'checked');
            });
        }
        else {
            $(".check_cmt").each(function () {
                $(this).prop('checked', '');
            });
        }
    });
    $(document).on("change", ".reply_pass_ajax", function (e) {
        if ($(".new_pass").val() !== $(".reply_pass_ajax").val()) {
            $(".msg_error").html('رمز وارد شده همخانی ندارد');
            e.preventDefault();
        } else {
            $(".msg_error").html('');

        }
    });
    $(document).on("change", ".reply_pass_ajax_id", function (e) {
        if ($(".new_pass_ajax").val() !== $(".reply_pass_ajax_id").val()) {
            $(".msg_error").html('رمز وارد شده همخانی ندارد');
            e.preventDefault();
        } else {
            $(".msg_error").html('');

        }
    });
    $(document).on("change", ".new_pass_ajax", function (e) {
        if ($(".new_pass_ajax").val().length < 6) {
            $(".msg_error").html('رمز وارد شده حداقل باید 6 کارکتر باشد');
            e.preventDefault();
        } else {
            $(".msg_error").html('');

        }
    });
    $(document).on("change", ".new_pass", function (e) {
        if ($(".new_pass").val().length < 6) {
            $(".msg_error").html('رمز وارد شده حداقل باید 6 کارکتر باشد');
            e.preventDefault();
        } else {
            $(".msg_error").html('');

        }
    });
    $(document).on("click", "#load_modal_delete_cls", function (e) {
        var link = $(this).data('link');
        $("#delete_user_ajax").attr('href', link);
    });
    $(document).on("click", "#change_pass_id_ajax", function (e) {
        var id = $(this).data('id');
        $("#change_pass_ajax").val(id);
    });

    $(document).on("click", "#change_pass_ajax_form", function () {
        var user_id = $("#change_pass_ajax").val();
        var user_pass = $(".new_pass_ajax").val();

        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/change_pass_manager/" + user_id + '/' + user_pass,
            type: "GET"
            ,
            success: function (data) {
                $(".msg_success").html('رمز با موفقیت تغییر یافت .. '),
                    $(".js-modal-close, .modal-overlay").click(function () {
                        $(".modal-box, .modal-overlay").fadeOut(500, function () {
                            $(".modal-overlay").remove();
                        });
                    });
            },
            error: function (e) {
                $(".msg_error").html(e).fadeIn();
            }

        });
    });


    $(document).on("click", "#show_manager_by_ajax", function () {
        var id = $(this).data('id');
        // var token = $(this).data('token');
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/manager_edit/" + id,
            type: "GET"
            //  ty: token
            ,
            success: function (data) {
                data = (JSON.parse(data));

                $("#hidden_id_ajax").val(id);
                if (data['Admin_dashboard'] == 'on') {
                    $("#dashboard_page_ajax").attr('checked', 'checked');
                } else {
                    $("#dashboard_page_ajax").empty()
                }
                if (data['Admin_listUser'] == 'on') {
                    $("#list_user_ajax").attr('checked', 'checked');
                } else {
                    $("#list_user_ajax").empty()
                }
                if (data['Admin_orders'] == 'on') {
                    $("#orders_list_ajax").attr('checked', 'checked');
                } else {
                    $("#orders_list_ajax").empty()
                }
                if (data['Admin_middle_managers'] == 'on') {
                    $("#modir_miani_ajax").attr('checked', 'checked');
                } else {
                    $("#modir_miani_ajax").empty()
                }
                if (data['Admin_category'] == 'on') {
                    $("#manage_cat_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_cat_ajax").empty()
                }
                if (data['Admin_cards'] == 'on') {
                    $("#manage_card_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_card_ajax").empty()
                }
                if (data['Admin_occasion'] == 'on') {
                    $("#manage_occasion_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_occasion_ajax").empty()
                }
                if (data['Admin_age'] == 'on') {
                    $("#manage_age_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_age_ajax").empty()
                }
                if (data['Admin_textCard'] == 'on') {
                    $("#manage_Sentence_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_Sentence_ajax").empty()
                }
                if (data['Admin_comment'] == 'on') {
                    $("#manage_comment_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_comment_ajax").empty()
                }
                if (data['Admin_advertising'] == 'on') {
                    $("#manage_adv_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_adv_ajax").empty()
                }
                if (data['Admin_transaction'] == 'on') {
                    $("#manage_transaction_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_transaction_ajax").empty()
                }
                if (data['Admin_page_settings'] == 'on') {
                    $("#manage_pages_ajax").attr('checked', 'checked');
                } else {
                    $("#manage_pages_ajax").empty()
                }

            }
            ,
            error: function (e) {
                alert("خطا در بارگذاری...");
            }
        });

    });


// toggle-nav
    $('.toggle-nav').click(function (e) {
        $(this).toggleClass('active');
        $('.menu ul').toggleClass('active');

        e.preventDefault();
    });


// add field
    var max_fields = 100; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function (e) { //on add input button click
        e.preventDefault();
        if (x < max_fields) { //max input box allowed
            x++; //text box increment
            $(wrapper).append(
                '<div>' +
                '<a href="#" class="remove_field">x</a>' +
                '<input class="input-form wide" type="file" name="mytext[]"/>' +
                '</div>'
            ); //add input box
        }
    });

    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    })


// Modal
    var appendthis = ("<div class='modal-overlay js-modal-close'></div>");

    $('a[data-modal-id]').click(function (e) {
        e.preventDefault();
        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        //$(".js-modalbox").fadeIn(500);
        var modalBox = $(this).attr('data-modal-id');
        $('#' + modalBox).fadeIn($(this).data());
    });


    $(".js-modal-close, .modal-overlay").click(function () {
        $(".modal-box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
        });
    });

    $(window).resize(function () {
        $(".modal-box").css({
            top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
            left: ($(window).width() - $(".modal-box").outerWidth()) / 2
        });
    });

    $(window).resize();


// ck ediror
    if ($("#editor1").length > 0) {

        CKEDITOR.replace('editor1', {
            customConfig: ''
        });

    }


// slider
    if ($(".one.owl-carousel").length > 0) {
        $('.one.owl-carousel').owlCarousel({
            rtl: true,
            loop: true,
            lazyLoad: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1000,
            // autoplayHoverPause:true,
            navText: [" <i class='fa fa-angle-left' aria-hidden='true'></i> ",
                " <i class='fa fa-angle-right' aria-hidden='true'></i> "],
            responsiveClass: true,

            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1000: {
                    items: 1,
                    nav: true,
                    loop: true
                }
            }
        })
    }

    if ($(".four.owl-carousel").length > 0) {
        $('.four.owl-carousel').owlCarousel({
            rtl: true,
            loop: true,
            margin: 10,
            navText: [" <i class='fa fa-chevron-left' aria-hidden='true'></i> ",
                " <i class='fa fa-chevron-right' aria-hidden='true'></i> "],
            responsiveClass: true,

            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                550: {
                    items: 2,
                    nav: true
                },
                850: {
                    items: 3,
                    nav: true
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: true
                }
            }
        })
    }

//afshar------------

    $(document).on("click", "#edit_user_of_list", function () {
        var user_id = $(this).data("id");

        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/DataUser/" + user_id,
            type: "GET"
            ,
            success: function (data) {
                data = (JSON.parse(data));
                $("#name_id").val(data['user_name']);
                $("#family_id").val(data['user_family']);
                $("#email_id").val(data['user_email']);
                $("#type_id").val(data['user_type']);
                $("#phone_id").val(data['user_phone']);
                $("#id_user_hidden_ajax").val(data['user_id']);

            },
            error: function (e) {
                $(".msg_error").html(e).fadeIn();
            }

        });
    });

    $(document).on("click", "#edit_category_of_list", function () {
        var id = $(this).data("id");

        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/CategoryData/" + id,
            type: "GET"
            ,
            success: function (data) {
                data = (JSON.parse(data));
                $("#EditTextCatId").val(data['cat_name']);
                $("#EditId_ajax").val(data['cat_id']);
                var image = window.location.protocol + "//" + window.location.host + "/Card_Shop/public/uploads/category/";
                $("#edit_img").attr("src", image + data['image']);

            },
            error: function (e) {
                $(".msg_error").html(e).fadeIn();
            }

        });
    });


    $(document).on("click", "#edit_age_of_list", function () {
        var id = $(this).data("id");

        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/AgeData/" + id,
            type: "GET"
            ,
            success: function (data) {
                data = (JSON.parse(data));
                $("#EditTextAgeId").val(data['YearGroup_title']);
                $("#EditId_ajax").val(data['YearGroup_id']);

            },
            error: function (e) {
                $(".msg_error").html(e).fadeIn();
            }

        });
    });


    $(document).on("click", "#edit_card_ajax", function () {
        var card_id = $(this).data("id");

        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/DataCard/" + card_id,
            type: "GET"
            ,
            success: function (data) {
                data = (JSON.parse(data));
                $("#oca_id_ajax").val(data['card_occasion_id']).html(data['card_occasion']);
                if ((data['card_occasion']) == 'تولد') {
                    $("#season_editID").show();
                } else {
                    $("#season_editID").hide();
                }
                $("#cat_id_ajax").val(data['card_category_id']).html(data['card_category']);
                $("#age_id_ajax").val(data['card_age_category_id']).html(data['card_age_category']);
                $("#sex_id_ajax").val(data['card_sexuality']).html(data['card_sexuality']);
                if (data['card_season'] == 1) {
                    var season = 'بهار';
                }
                if (data['card_season'] == 2) {
                    season = 'تابستان';
                }
                if (data['card_season'] == 3) {
                    season = 'پاییز';
                }
                if (data['card_season'] == 4) {
                    season = 'زمستان';
                }
                $("#season_id_ajax").val(data['card_season']).html(season);
                $("#text_id_ajax").val(data['card_title']);
                $("#card_id_ajax").val(data['card_id']);

            },
            error: function (e) {
                $(".msg_error").html(e).fadeIn();
            }

        });
    });
    $(document).on("click", "#edit_About_ajax", function () {
        var id = $(this).data("id");
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/DateAbout/" + id,
            type: "GET"
            ,
            success: function (data) {
                data = (JSON.parse(data));
                $("#About_ajax_id").val(data['about_id']);
                $("#TextAboutEditUsID").val(data['about_text']);

            },
            error: function (e) {
                $(".msg_error").html(e).fadeIn();
            }

        });
    });
    $(document).on("click", "#edit_Contact_ajax", function () {
        var id = $(this).data("id");
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/DateContact/" + id,
            type: "GET"
            ,
            success: function (data) {
                data = (JSON.parse(data));
                $("#contact_ajax_id").val(data['contact_id']);
                $("#EditTextContactID").val(data['contact_text']);

            },
            error: function (e) {
                $(".msg_error").html(e).fadeIn();
            }

        });
    });

    $(document).on("click", "#edit_Cado_ajax", function () {
        var id = $(this).data("id");
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Admin/DataCadoShop/" + id,
            type: "GET"
            ,
            success: function (data) {
                data = (JSON.parse(data));
                $("#cado_ajax_id").val(data['Cadoshop_id']);
                $("#EditTextCadoID").val(data['Cadoshop_text']);

            },
            error: function (e) {
                $(".msg_error").html(e).fadeIn();
            }

        });
    });

    $(document).on('change', '#CardOccasion_id', function () {
        if ($(this).find('option:selected').text() == 'تولد') {
            $("#seasonID").show();
        } else {
            $("#seasonID").hide();

        }

    });


    $(document).on('change', '#occasion_editID', function () {
        if ($(this).find('option:selected').text() == 'تولد') {
            $("#season_editID").show();
        } else {
            $("#season_editID").hide();
            $("#season_id_ajax").val(0);
        }

    });


    $(document).on("click", "#delete_user_of_list", function () {
        var link = $(this).data("link");
        $("#delete_user_ajax").attr('href', link);
    });
    $(document).on("click", "#delete_card_of_list", function () {
        var link = $(this).data("link");
        $("#delete_card_ajax").attr('href', link);
    });
    $(document).on("click", "#delete_order_of_list", function () {
        var link = $(this).data("link");
        $("#DeleteOrderAjax").attr('href', link);
    });
    $(document).on("click", "#delete_comment_of_list", function () {
        var link = $(this).data("link");
        $("#DeleteCommentAjax").attr('href', link);
    });

// end afshar------------

});








