//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// ....................... js plugins functions ................//
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

// accordion_2
     if($(".accordion-2").length>0){

        $(".accordion-2").accordion({
             collapsible: true,
             heightStyle: "content"
          });
      }

     new WOW().init();

     $(".res_menu").on("click",function(){
        $("nav ul").slideToggle();
     });
     $(".res_menu").on("click",function(){
        $("nav ul").slideToggle();
     });

  // menu ------------------------

    $(window).scroll(function () {
        var $h = $(window).scrollTop();

        if ( $h < 200 )
        {
          $('nav').removeClass("fixnav")
          $('nav').addClass("navbar")
          }
        else {
          $('nav').removeClass("navbar")
          $('nav').addClass("fixnav")
          }

    });

  // ---------------------------------

  $(document).ready(function(){




    var $h = $(window).scrollTop();


    if ( $h == 0 )
        {
          $('nav').removeClass("fixnav")
          $('nav').addClass("navbar")
        }


    // Add smooth scrolling to all links
      $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();

          // Store hash
          var hash = this.hash;

          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });

  });

  // owl slider
    $(document).ready(function() {

        $(".ostan").on("change",function(){
            var value = $(this).val();
            $.ajax({
                type:'GET',
                url:window.location.protocol + "//" + window.location.host + "/Card_Shop/public/getcity/"+value,
                success:function(data){
                    $(".city").html(data);
                }
            });
        });

        $(".city").on("change",function(){
            var value = $(this).val();
            $.ajax({
                type:'GET',
                url:window.location.protocol + "//" + window.location.host + "/Card_Shop/public/savecity/"+value
            });
        });

        $(".kado_price").on("click",function(){
            var value = parseInt($(this).find("span").html());
            $.ajax({
                type:'GET',
                url:window.location.protocol + "//" + window.location.host + "/Card_Shop/public/saveKado/"+value
            });
        });

        $(".last_step").on("click",function(e){
            if($(".load_hedye_by_ajax").val().length==0){
                alert("میلغ شارژ هدیه را پر نکرده اید");
                e.preventDefault();
            }else{
                var value = parseInt($(".load_hedye_by_ajax").val());
                $.ajax({
                    type:'GET',
                    url:window.location.protocol + "//" + window.location.host + "/Card_Shop/public/saveprice/"+value
                });
            }
        });

        $("#off_maker").on("click",function (e) {
            e.preventDefault();
            if($("#code_off").val().length==0){
                alert("کد تخفیفی هنوز وارد نشده است");
            }else{
                var value = $("#code_off").val();
                $.ajax({
                    type:'GET',
                    url:window.location.protocol + "//" + window.location.host + "/Card_Shop/public/codeoff/"+value,
                    success:function(data){
                        if(data==0){
                            alert("این کد وجود ندارد");
                        }else{
                            $(".takhfif_perecent").find("b").html(data);
                            calc_basket();
                        }
                    }
                });
            }

        });





        $("#caption_onimage").on("input",function(){
            $("#caption").html($(this).val());
        });

        function calc_basket(){
            var price = $("#how_much").val() * parseInt($(".main_price").html());
            var cityprice = 0;
            var kadoprice = 0;
            var takhfif = 0;
            if($(".city_price").length>0){
                cityprice = parseInt($(".city_price").find("b").html());
            }
            if($(".kado_price").length>0){
                kadoprice = parseInt($(".kado_price").find("b").html());
            }
            takhfif = parseInt($(".takhfif_perecent").find("b").html());
            var sumall = 0;

            if(takhfif!=0){
                 sumall = (((price + cityprice+kadoprice)*100)/takhfif)-(price + cityprice+kadoprice);
            }else{
                sumall = (price + cityprice+kadoprice);
            }

            $(".price_all").html(price);
            $(".price_all_by_takhfif").html(sumall);
        }

        $(document).on("change", "#how_much",function () {
            calc_basket();
        });

        calc_basket();

        $(document).on("click", ".send_hedye_by_ajax", function () {
            var id =   $(".load_hedye_by_ajax").val();
            $.ajax({
                url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Kado/" + id,
                type: "GET"
                ,
                success: function (data) {


                },
                error: function (e) {
                }

            });
        });


        $(document).on("click", ".send_hedye_email_by_ajax", function () {
            var id =   $(".load_hedye_by_ajax").val();
            alert(id);
            $.ajax({
                url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Basket_shop/" + id,
                type: "GET"
                ,
                success: function (data) {


                },
                error: function (e) {
                }

            });
        });



        //afshar------------------
        $('#password_re_id').on('change', function () {
            if ($('#password_id').val() != $('#password_re_id').val()) {
                $('#message').html('تکرار کلمه عبور با کلمه عبور همخوانی ندارد').css('color', 'red');
                $('#password_re_id').val('');
            } else {
                $('#message').html('');
            }

        });
        $('#password_id').on('input', function () {
            if ($('#password_id').val().length < 6) {
                $('#message').html(' کلمه عبور حداقل 6 حرف باشد').css('color', 'red');
            } else {
                $('#message').html('');
            }

        });
        $('#phone_id').on('input', function () {
            if ($('#phone_id').val().length < 11) {
                $('#message').html(' شماره موبایل وارد شده اشتباه است').css('color', 'red');
            } else if ($('#phone_id').val().substring(0, 2) !== '09') {
                $('#message').html('قالب شماره همراه صحیح نیست').css('color', 'red');
            }
            else if ($('#phone_id').val().length > 11) {
                $('#message').html('قالب شماره همراه صحیح نیست').css('color', 'red');
            } else {
                $('#message').html('');
            }
        });

//end afshar---------------------


     new WOW().init();

     if($(".one.owl-carousel").length>0){
          $('.one.owl-carousel').owlCarousel({
              rtl:true,
              loop:true,
              margin:10,
              autoplay:true,
              autoplayTimeout:3000,
              navText: [" <i class='fa fa-chevron-left' aria-hidden='true'></i> ",
                        " <i class='fa fa-chevron-right' aria-hidden='true'></i> "],
              responsiveClass:true,
              responsive:{
                  0:{
                      items:1,
                      nav:true
                  },
                  600:{
                      items:1,
                      nav:true
                  },
                  1000:{
                      items:1,
                      nav:true,
                      loop:true
                  }
              }
          })
      }

      if($(".four.owl-carousel").length>0){
          $('.four.owl-carousel').owlCarousel({
              rtl:true,
              loop:true,
              margin:10,
              navText: [" <i class='fa fa-chevron-left' aria-hidden='true'></i> ",
                        " <i class='fa fa-chevron-right' aria-hidden='true'></i> "],
              responsiveClass:true,
              autoplay:true,
              autoplayTimeout:3000,
              responsive:{
                  0:{
                      items:1,
                      nav:true
                  },
                  550:{
                      items:2,
                      nav:true
                  },
                  850:{
                      items:3,
                      nav:true
                  },
                  1000:{
                      items:4,
                      nav:true,
                      loop:true
                  }
              }
          })
      }
    });


// mighty slider js

    function percentToValue(percent, total) {
        return parseInt((total / 100) * percent);
    }


    function degreeToRadian(degree) {
        return ((degree - 90) * Math.PI) / 180;
    }

    jQuery(document).ready(function($) {
        var $win = $(window),
            isTouch = !!('ontouchstart' in window),
            clickEvent = isTouch ? 'tap' : 'click';

        (function(){
            // Global slider's DOM elements
            var $example = $('#example'),
                $frame = $('.frame', $example),
                $slides = $('.slide_element', $frame).children(),
                $thumbnailsBar = $('div#thumbnails ul', $example),
                $timerEL = $('canvas', $example),
                ctx = $timerEL[0] && $timerEL[0].getContext("2d"),
                slideSize = '49%',
                lastIndex = -1;


            function drawArc(angle) {
                var startingAngle = degreeToRadian(0),
                    endingAngle = degreeToRadian(angle),
                    size = 160,
                    center = size / 2;

                //360Bar
                ctx.clearRect(0, 0, size, size);
                ctx.beginPath();
                ctx.arc(center, center, center-4, startingAngle, endingAngle, false);
                ctx.lineWidth = 8;
                ctx.strokeStyle = "#aaa";
                ctx.lineCap = "round";
                ctx.stroke();
                ctx.closePath();
            }

            // Calling mightySlider via jQuery proxy
            $frame.mightySlider({
                speed: 1500,
                startAt: 2,
                autoScale: 1,
                easing: 'easeOutExpo',

                // Navigation options
                navigation: {
                    slideSize: slideSize,
                    keyboardNavBy: 'slides',
                    activateOn: clickEvent
                },

                // Thumbnails options
                thumbnails: {
                    thumbnailsBar: $thumbnailsBar,
                    thumbnailNav: 'forceCentered',
                    activateOn: clickEvent,
                    scrollBy: 0
                },

                // Dragging options
                dragging: {
                    mouseDragging: 0,
                    onePage: 1
                },

                // Buttons options
                buttons: !isTouch ? {
                    prev: $('a.mSPrev', $frame),
                    next: $('a.mSNext', $frame)
                } : {},

                // Cycling options
                cycling: {
                    cycleBy: 'slides'
                }
            },

            // Register callbacks to the events
            {
                // Register mightySlider :active event callback
                active: function(name, index) {
                    if (lastIndex !== index) {
                        // Hide the timer
                        $timerEL.stop().css({ opacity: 0 });

                        // Remove next and previous classes from the slides
                        $slides.removeClass('next_1 next_2 prev_1 prev_2');

                        // Detect next and prev slides
                        var next1 = this.slides[index + 1],
                            next2 = this.slides[index + 2],
                            prev1 = this.slides[index - 1],
                            prev2 = this.slides[index - 2];

                        // Add next and previous classes to the slides
                        next1 && $(next1.element).addClass('next_1');
                        next2 && $(next2.element).addClass('next_2');
                        prev1 && $(prev1.element).addClass('prev_1');
                        prev2 && $(prev2.element).addClass('prev_2');
                    }

                    lastIndex = index;
                },

                // Register mightySlider :moveEnd event callback
                moveEnd: function() {
                    // Reset cycling progress time elapsed
                    this.progressElapsed = 0;
                    // Fade in the timer
                    $timerEL.animate({ opacity: 1 }, 800);
                },

                // Register mightySlider :progress event callback
                progress: function(name, progress) {
                    // Draw circle bar timer based on progress
                    drawArc(360 - (360 / 1 * progress));
                },

                // Register mightySlider :initialize and :resize event callback
                'initialize resize': function(name) {
                    var self = this,
                        frameSize = self.relative.frameSize,
                        slideSizePixel = percentToValue(slideSize.replace('%', ''), frameSize),
                        remainedSpace = (frameSize - slideSizePixel),
                        margin = (slideSizePixel - remainedSpace / 3) / 2;

                    // Sets slides margin
                    $slides.css('margin', '0 -' + margin + 'px');
                    // Reload immediate
                    self.reload(1);
                }
            });
        })();
    });


// tabs
    function box1(x1, x2) {
        var a, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (a = 0; a < tabcontent.length; a++) {
            tabcontent[a].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (a = 0; a < tablinks.length; a++) {
            tablinks[a].className = tablinks[a].className.replace(" active", "");
        }

        document.getElementById(x2).style.display = "block";
        x1.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it

      if($("#defaultOpen").length>0){
      document.getElementById("defaultOpen").click();
      }

// accordein
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var accordionPanel = this.nextElementSibling;
    if (accordionPanel.style.maxHeight){
      accordionPanel.style.maxHeight = null;
    } else {
      accordionPanel.style.maxHeight = accordionPanel.scrollHeight + "px";
    }
  }
}



function openNav() {
    document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
}


function initMap() {
    var uluru = {lat: 36.205929, lng: 58.788432};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}

// range-slider

$(function () {
      if($("#range").length>0){
          $("#range").ionRangeSlider({
            // hide_min_max: true,
            keyboard: true,
            min: 0 ,
            max: 6000 ,
            from: 1000 ,
            to: 4000 ,
            type: 'double',
            step: 1 ,
            prefix: " ريال ",
            prettify_enabled: true,
            prettify_separator: ".",
            // grid: true
        });
      }


    });



// =================

/*
  selectFile.js v1.0
  (c) 2017 by Thielicious
  
  A JavaScript function which lets you customize the browse button and its selection text. 
  It is known that the browse button `<input type=file>` is not directly customizable. 
  This function simply emulates this button using an ordinary input button as a trigger.
*/


var selectFile = function() {

  var regex = /[^\\]+$/;

  this.choose,
  this.selected;

  this.msg = (str) => {
    var prefix = '[selectFile.js]\n\nError: ';
    return alert(prefix+str);
  }

  this.check = () => {
    if (this.choose && this.selected != null) {
      var choose = document.getElementById(this.choose),
        selected = document.getElementById(this.selected);
      choose.addEventListener("change",() => {
        if (choose.value != "") {
          selected.innerHTML = choose.value.match(regex);
        }
      });
    } else {
      this.msg("Targets not set.");
    }
  }

  selectFile.prototype.targets = (trigger, filetext) => {
    this.choose = trigger;
    this.selected = filetext;
  }

  selectFile.prototype.simulate = () => {
    if (this.choose != null) {
      var choose = document.getElementById(this.choose);
      if (typeof choose != "undefined") {
        choose.click();
        this.check();
      } else {
        this.msg("Could not find element "+this.choose);
      }
    } else {
      this.msg("Targets not set.");
    }
  }

};

// input-file chaneged
var getFile = new selectFile;
getFile.targets('choose','selected');


// modal
var modal = document.getElementById('myModal');

var btn = document.getElementById("myBtn");

var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
    modal.style.display = "block";
}

span.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// google-map
function myMap() {
      var mapOptions = {
          center: new google.maps.LatLng(51.5, -0.12),
          zoom: 10
      }
      var map = new google.maps.Map(document.getElementById("map"), mapOptions);
      }

// load-img
var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
};

$(document).ready(function() {


    $(document).on("change", "#how_much",function () {
        if($("#how_much").val() == 'choice'){
            var price = 39000;
            var takhfif =39000;
            var takhfif_perecent = 0;
            $(".price_all").html(price);
            $(".price_all_by_takhfif").html(takhfif);
            $(".takhfif_perecent").html(takhfif_perecent);
        }else {
            var price = $("#how_much").val() * 39000;
            var takhfif = $("#how_much").val() * 39000 * 4 / 5;
            var takhfif_perecent = $("#how_much").val() * 39000 * 1 / 5;
            $(".price_all").html(price);
            $(".price_all_by_takhfif").html(takhfif);
            $(".takhfif_perecent").html(takhfif_perecent);
        }
    });

    $(document).on("click", ".send_hedye_by_ajax", function () {
        var id =   $(".load_hedye_by_ajax").val();
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Kado/" + id,
            type: "GET"
            ,
            success: function (data) {


            },
            error: function (e) {
            }

        });
    });


    $(document).on("click", ".send_hedye_email_by_ajax", function () {
        var id =   $(".load_hedye_by_ajax").val();
        alert(id);
        $.ajax({
            url: window.location.protocol + "//" + window.location.host + "/Card_Shop/public/Basket_shop/" + id,
            type: "GET"
            ,
            success: function (data) {


            },
            error: function (e) {
            }

        });
    });

});
