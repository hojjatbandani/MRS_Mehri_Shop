@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2>مدیریت کارت ها</h2>
                    <div class="flash-message">
                        @include('Web.partials.errors')
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>

                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ثبت کارت ها</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">لیست کارت ها</label>
                    <section class="tabcontent" id="content1">
                        <form action="{{route('Admin.AddCard')}}" enctype="multipart/form-data" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="label-form wide">مناسبت</label>
                                    <select id='CardOccasion_id' name="occasion" class="input-form wide js-example-tags">
                                        <?php foreach ($Oca as $R_OCA) {
                                            echo "<option  id='OptionOccasion_id' value=\"$R_OCA[occasion_id]\" >$R_OCA[occasion_text]</option>";
                                        }?>
                                    </select>
                                    <div style="display: none" id="seasonID" >
                                    <label class="label-form wide">فصل</label>
                                    <select name="season" class="input-form wide js-example-tags">
                                        <option value="0">انتخاب کنید</option>
                                        <option value="1">بهار</option>
                                        <option value="2">تابستان</option>
                                        <option value="3">پاییز</option>
                                        <option value="4">زمستان</option>
                                    </select>
                                    </div>
                                    <label class="label-form wide">دسته بندی</label>
                                    <select name="category" class="input-form wide js-example-tags">
                                        <?php foreach ($Cat as $R_Cat) {
                                            echo "<option value=\"$R_Cat[cat_id]\" >$R_Cat[cat_name]</option>";
                                        }?>
                                    </select>
                                    </div>
                                    <label class="label-form wide">گروه سنی</label>
                                    <select name="AgeCategory" class="input-form wide js-example-tags">
                                        <?php foreach ($YG as $R_YG) {
                                            echo "<option value=\"$R_YG[YearGroup_id]\" >$R_YG[YearGroup_title]</option>";
                                        }?>
                                    </select>
                                    <label class="label-form wide">جنسیت</label>
                                    <select name="Sex" class="input-form wide js-example-tags">
                                        <option value="1">آقا</option>
                                        <option value="0">خانم</option>
                                    </select>

                                    <label class="label-form wide">متن روی کارت</label>
                                    <input type="text" name="TextCard" class="input-form wide">

                                    <label class="label-form wide">عکس کارت</label>
                                    <input type="file" name="pic_front" class="input-form wide">

                                    <label class="label-form wide">عکس پشت کارت</label>
                                    <input type="file" name="pic_behind" class="input-form wide">

                                </div>
                <footer class="panel-footer">
                    <div class="center">
                        <input type="submit" class="btn btn-blue" value="ثبت">
                    </div>
                </footer>
                </form>
                    </section>

                    <section class="tabcontent" id="content2">

                        <div class="row">
                            <div class="col-md-12 h-pading">
                                <table class="table-defult">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>متن کارت</th>
                                        <th>مناسبت</th>
                                        <th>دسته بندی</th>
                                        <th>گروه سنی</th>
                                        <th>جنسیت</th>
                                        <th>عکس کارت</th>
                                        <th>عکس پشت کارت</th>
                                        <th>ویرایش</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($list as $count) {
                                        if ($count['card_sexuality'] == '0') {
                                            $sex = 'خانم';
                                        } else {
                                            $sex = 'آقا';
                                        }
                                        echo "<tr>
                                        <td>$count[card_id]</td>
                                        <td>$count[card_title]</td>
                                        <td>$count[card_occasion]</td>
                                        <td> $count[card_category]</td>
                                        <td>$count[card_age_category]</td>
                                        <td> $sex </td>
                                        <td><img style='width:100px;border:1px solid #ccc;border-radius:5px;' src='".route('home')."/uploads/cards/front/".$count['card_front_image']."' alt='عکس یافت نشد'></td>
                                        <td><img style='width:100px;border:1px solid #ccc;border-radius:5px;' src='".route('home')."/uploads/cards/behind/".$count['card_behind_image']."' alt='عکس یافت نشد'></td>
                                        <td><a class='js-open-modal edit-icon' href='#'
                                               data-modal-id='edit_card_modal' data-id='$count[card_id]'  id='edit_card_ajax'><span class='icon-edit'></span></a></td>
                                        <td><a id='delete_card_of_list' class='js-open-modal colse-icon' href='' data-link=".route('Admin.DeleteCard',$id=$count['card_id'])."
                                               data-modal-id='popup-close'><span class='icon-close'></span></a></td>
                                    </tr>
                                  ";
                                    }?>
                                    </tbody>
                                </table>
                                <div class="pagination center">
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->


    <div id="edit_card_modal" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش کارت</h3>
        </header>
        <div class="modal-body">
            <form action="{{route('Admin.EditCard')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <label class="label-form wide">مناسبت</label>
                        <select id="occasion_editID" name="occasion_edit" class="input-form wide js-example-tags">
                            <option id="oca_id_ajax" value=""></option>
                            <?php foreach ($Oca as $R_OCA) {
                                echo "<option  value=\"$R_OCA[occasion_id]\" >$R_OCA[occasion_text]</option>";
                            }?>

                        </select>
                        <div style="display: none" id="season_editID" >
                            <label class="label-form wide">فصل</label>
                            <select name="season_edit" class="input-form wide js-example-tags">
                                <option id="season_id_ajax" value=""></option>
                                <option value="1">بهار</option>
                                <option value="2">تابستان</option>
                                <option value="3">پاییز</option>
                                <option value="4">زمستان</option>
                            </select>
                        </div>
                            <label class="label-form wide">گروه سنی</label>
                            <select  name="AgeCategory_edit" class="input-form wide js-example-tags">
                                <?php foreach ($YG as $R_YG) {
                                    echo "<option id='age_id_ajax' value=\"$R_YG[YearGroup_id]\" >$R_YG[YearGroup_title]</option>";
                                }?>
                            </select>
                        <label class="label-form wide">دسته بندی</label>
                        <select  name="category_edit" class="input-form wide js-example-tags">
                            <?php foreach ($Cat as $R_Cat) {
                                echo "<option id='cat_id_ajax' value=\"$R_Cat[cat_id]\" >$R_Cat[cat_name]</option>";
                            }?>

                        </select>
                        <label class="label-form wide">جنسیت</label>
                        <select  name="Sex_edit" class="input-form wide js-example-tags">
                            <option value="1">آقا</option>
                            <option value="0">خانم</option>
                        </select>

                        <label class="label-form wide">متن روی کارت</label>
                        <input id="text_id_ajax" type="text" name="TextCard_edit" class="input-form wide">

                        <label class="label-form wide">عکس کارت</label>
                        <input type="file" name="pic_front_edit" class="input-form wide">

                        <label class="label-form wide">عکس پشت کارت</label>
                        <input type="file" name="pic_behind_edit" class="input-form wide">
                        <input type="hidden" name="Card_id" id="card_id_ajax">
                    </div>
                </div>

                <footer class="panel-footer">
                    <div class="center">
                        <input type='submit' name="EditCard" class="btn btn-blue" value="ویرایش">
                    </div>
                </footer>
            </form>


        </div>
    </div>
    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>حذف صفحه</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_card_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

@endsection

