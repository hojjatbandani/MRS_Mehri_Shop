@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2>مدیریت تبلیغات</h2>

                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ثبت تبلیغات</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">لیست تبلیغات</label>


                    <section class="tabcontent" id="content1">

                        <form>

                            <div class="row">
                                <div class="col-md-12">

                                    <label class="label-form wide">مکان نمایش</label>
                                    <select name="type_of_admin" class="input-form wide js-example-tags">
                                        <option value="level_1">پایین </option>
                                        <option value="level_2">بالا</option>
                                        <option value="journalist">چپ</option>
                                        <option value="journalist">راست</option>
                                    </select>

                                    <label class="label-form wide">عرض</label>
                                    <input type="text" class="input-form wide">

                                    <label class="label-form wide">طول</label>
                                    <input type="text" class="input-form wide">


                                    <label class="label-form wide">نکرار</label>
                                    <input type="password" class="input-form wide">


                                    <div id="msg_pass_id" class="msg_error"></div>

                                </div>
                            </div>

                            <footer class="panel-footer">
                                <div class="center">
                                    <input type="submit" class="btn btn-blue" value="ثبت">
                                </div>
                            </footer>
                        </form>
                    </section>

                    <section class="tabcontent" id="content2">

                        <div class="row">
                            <div class="col-md-12 h-pading">
                                <table class="table-defult">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام</th>
                                        <th>نوع</th>
                                        <th>رمز عبور</th>
                                        <th>نمایش دسترسی</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>رضا بقیعی</td>
                                        <td> اپراتور سطح اول</td>
                                        <td><a class='js-open-modal edit-icon' data-id=" " href='#' data-modal-id='change_pass' id='change_pass_id_ajax'> تغییر رمز</a></td>
                                        <td><a class='js-open-modal edit-icon' data-id=" " href='#' data-modal-id='popup' id='show_manager_by_ajax'><span
                                                        class='icon-eye'></span></a></td>
                                        <td><a id='load_modal_delete_cls' class='js-open-modal colse-icon' data-link="" data-modal-id='popup-close'><span
                                                        class='icon-cancel-1'></span>x</a></td>
                                    </tr>

                                    </tbody>
                                </table>
                                <div class="pagination center">
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->


    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>حذف صفحه</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_user_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

    <div id="change_pass" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>تغییر رمز عبور</h3>
        </header>
        <div class="modal-body">


            <input type="hidden" name="change_pass_id" id="change_pass_ajax">
            <footer class="center">
                <a href="#" class="js-modal-close btn btn-red">انصراف</a>
            </footer>
        </div>
    </div>
@endsection

