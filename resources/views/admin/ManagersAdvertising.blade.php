@extends('leyouts.admin')
@section('content')

    <!-- content -->
    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2>مدیریت تبلیغات</h2>

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>

                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ثبت تبلیغات</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">لیست تبلیغات</label>


                    <section class="tabcontent" id="content1">

                        <form action="{{route('Admin.Advertising_add')}}" enctype="multipart/form-data" method="post">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col-md-12">

                                    <label class="label-form wide">لینک تبلیغات</label>
                                    <input type="text" required="required" id="link_of_adv" name="link_of_adv"
                                           placeholder="لینک تبلیغات را وارد کنید" class="input-form wide">

                                    <label class="label-form wide">تصویر تبلیغات :</label>
                                    <input type="file" required="required" id="pic_of_adv_id" name="image_file"
                                           class="input-form wide">


                                    <div id="msg_pass_id" class="msg_error"></div>

                                </div>
                            </div>

                            <footer class="panel-footer">
                                <div class="center">
                                    <input type="submit" class="btn btn-blue" value="ثبت">
                                </div>
                            </footer>
                        </form>
                    </section>

                    <section class="tabcontent" id="content2">

                        <div class="row">
                            <div class="col-md-12 h-pading">
                                @if(count($res)>0)
                                    <table class="table-defult">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>لینک عکس تبلیغات</th>
                                            <th>نمایش عکس</th>
                                            <th>ویرایش</th>
                                            <th>حذف</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $x = 0; ?>
                                        @foreach($res as $row)
                                      <p style="display: none">{{$id =$row->Ads_id }}</p>
                                            <tr>
                                                <td>{{++$x}}</td>
                                                <td>{{$row->Ads_address}}</td>
                                                <td><a href="#"><img class="img-tumb"
                                                                     src="../admin/uploads/Advertising/{{$row->Ads_image}}"></a>
                                                </td>
                                                <td><a class='js-open-modal edit-icon' data-id="{{$row->Ads_id}}"
                                                       href='#' data-modal-id='popup'
                                                       id='show_advertising_by_ajax'><span
                                                                class='icon-edit'></span></a></td>
                                                <td><a id='load_modal_delete_cls' class='js-open-modal colse-icon'
                                                       data-link="{{route('Admin.Advertising_delete',$id)}}"
                                                       data-modal-id='popup-close'><span
                                                                class='icon-cancel-1'></span>x</a></td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                @else
                                    موردی برای نمایش  یافت نشد ...
                                @endif
                                <div class="pagination center">
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->


    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>اخطار</h3>
        </header>
        <div class="modal-body">
            <p> آیا از حذف  مطمئن هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_user_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>


    <div id="popup" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3> ویرایش </h3>
        </header>
        <div class="modal-body">
            <form action="{{route('Admin.Advertising_edt')}}" enctype="multipart/form-data" method="post">
                <div class="row">
                    {{csrf_field()}}
                    <div class="col-md-6 h-padding">
                        <label class="label-form ">لینک تبلیغ</label>
                        <input class="input-form wide" name="position" id="position_by_ajax" type="text" value=""
                               placeholder="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 h-padding">
                        <label class="label-form ">نمایش عکس :</label>
                        <img src="" class="img-tumb" id="Ads_image">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 h-padding">
                        <label class="label-form "> بارگذاری عکس جدید :</label>
                        <input type="file" name="image_file" id="new_pic_for_edit" class="input-form wide">
                    </div>
                    <input type="hidden" id="advertising_ajax_id" name="adv_ajax">
                </div>

                <footer class="center">
                    <button class="btn btn-orange">ثبت</button>
                    <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                </footer>
            </form>
        </div>
    </div>



@endsection

