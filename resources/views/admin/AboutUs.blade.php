@extends('leyouts.admin')
@section('content')

    <!-- content -->
    <div class="content" id="CONTENT">
        <div class="container ">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
                                </p>
                            @endif
                        @endforeach
                    </div>
                    @include('admin.partials.errors')



                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">درباره ما</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">تماس با ما</label>

                    <input class="tab-input" id="tab3" type="radio" name="tabs">
                    <label class="tab-label" for="tab3"> کادو شاپ چیه؟</label>
                    {{--ABOUT--}}
                    <section class="tabcontent" id="content1">
                        <header class="panel-header">
                            درباره ی ما
                        </header>
                                               <div class="panel-body">
                         <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='msg_error'></div>
                                        <?php echo $res['about_text'];?>
                                    </div>
                                </div>
                                 <footer class='panel-footer'>
                                    <div class='center'>
                                        <a
                                          class='js-open-modal btn btn-blue'
                                          href='#'
                                          data-modal-id='edit_about_modal'
                                          data-id='<?php echo $res['about_id']?>'
                                          id='edit_About_ajax'>ویرایش</a>
                                    </div>
                                </footer>
                        </div>
                    </section>
                    {{--CONTACT--}}
                    <section class="tabcontent" id="content2">

                            <header class="panel-header">
                                تماس با ما
                            </header>
                            <div class="panel-body">
                                <div class='row'>
                                    <div class='col-md-12'>
                                        <div class='msg_error'></div>
                                        <?php echo $con['contact_text'];?>
                                    </div>
                                </div>
                                <footer class='panel-footer'>
                                    <div class='center'>
                                        <a
                                                class='js-open-modal btn btn-blue'
                                                href='#'
                                                data-modal-id='edit_con_modal'
                                                data-id='<?php echo $con['contact_id']?>'
                                                id='edit_Contact_ajax'>ویرایش</a>
                                    </div>
                                </footer>
                            </div>
                    </section>
                    {{--What is Gift SHOP--}}
                    <section class="tabcontent" id="content3">

                        <header class="panel-header">
                            کادو شاپ چیه؟
                        </header>
                        <div class="panel-body">
                            <div class='row'>
                                <div class='col-md-12'>
                                    <div class='msg_error'></div>
                                    <?php echo $cado['Cadoshop_text'];?>
                                </div>
                            </div>
                            <footer class='panel-footer'>
                                <div class='center'>
                                    <a
                                            class='js-open-modal btn btn-blue'
                                            href='#'
                                            data-modal-id='edit_cado_modal'
                                            data-id='<?php echo $cado['Cadoshop_id']?>'
                                            id='edit_Cado_ajax'>ویرایش</a>
                                </div>
                            </footer>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
    {{--MODAL--}}
    {{--ABOUT--}}
    <div id="edit_about_modal" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش متن</h3>
        </header>
        <div class="modal-body">
            <form action="{{route('Admin.EditAbout')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="About_id" id="About_ajax_id">
                        <label class="label-form wide">متن مورد نظر :</label>
                        <input type="text" id="TextAboutEditUsID" name="TextAboutEditUs" class="input-form wide">
                    </div>
                </div>
                <footer class="panel-footer">
                    <div class="center">
                        <input type='submit' name="EditTextAbout" class="btn btn-blue" value="ویرایش">
                    </div>
                </footer>
            </form>
        </div>
    </div>
{{--CONTACT--}}
    <div id="edit_con_modal" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش متن</h3>
        </header>
        <div class="modal-body">
            <form action="{{route('Admin.EditContact')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="contact_id" id="contact_ajax_id">
                        <label class="label-form wide">متن مورد نظر :</label>
                        <input type="text" id="EditTextContactID" name="EditTextContactUs" class="input-form wide">
                    </div>
                </div>
                <footer class="panel-footer">
                    <div class="center">
                        <input type='submit' name="EditTextContact" class="btn btn-blue" value="ویرایش">
                    </div>
                </footer>
            </form>
        </div>
    </div>
{{--What is Gift SHOP--}}
    <div id="edit_cado_modal" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش متن</h3>
        </header>
        <div class="modal-body">
            <form action="{{route('Admin.EditCado')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="cado_id" id="cado_ajax_id">
                        <label class="label-form wide">متن مورد نظر :</label>
                        <input type="text" id="EditTextCadoID" name="EditTextCadoUs" class="input-form wide">
                    </div>
                </div>
                <footer class="panel-footer">
                    <div class="center">
                        <input type='submit' name="EditTextCado" class="btn btn-blue" value="ویرایش">
                    </div>
                </footer>
            </form>
        </div>
    </div>
@endsection
