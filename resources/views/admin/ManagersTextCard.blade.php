@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2>مدیریت تبلیغات</h2>

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>
                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ثبت جملات پیش فرض</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">لیست جملات پیش فرض</label>


                    <section class="tabcontent" id="content1">

                        <form action="{{route('Admin.TextCard')}}" method="post">
                            {{csrf_field()}}

                            <div class="row">
                                <div class="col-md-12">

                                    <label class="label-form wide">جمله پیش فرض</label>
                                    <input type="text" name="sentence" class="input-form wide">

                                    <div id="msg_pass_id" class="msg_error"></div>

                                </div>
                            </div>

                            <footer class="panel-footer">
                                <div class="center">
                                    <input type="submit" class="btn btn-blue" value="ثبت">
                                </div>
                            </footer>
                        </form>
                    </section>

                    <section class="tabcontent" id="content2">

                        <div class="row">
                            <div class="col-md-12 h-pading">
                                @if(count($sen)>0)
                                <table class="table-defult">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>متن جمله</th>
                                        <th>ویرایش</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x = 0; ?>

                                    @foreach($sen as $sentence)
                                        <p style="display: none">{{$id =$sentence->Sentences_id }}</p>

                                        <tr>
                                        <td>{{++$x}}</td>
                                        <td>{{$sentence->Sentences_title}} </td>
                                        <td><a class='js-open-modal edit-icon' data-id="{{$sentence->Sentences_id}}" href='#' data-modal-id='popup' id='edit_sentence_by_ajax'><span
                                                        class='icon-edit'></span></a></td>
                                        <td><a id='load_modal_delete_cls' class='js-open-modal colse-icon' data-link="{{route('Admin.TextCard_delete',$id)}}" data-modal-id='popup-close'><span
                                                        class='icon-cancel-1'></span>x</a></td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                @else
                                    موردی برای نمایش یافت نشد ...
                                    @endif
                                <div class="pagination center">
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->


    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>حذف صفحه</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_user_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

    <div id="change_pass" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>تغییر رمز عبور</h3>
        </header>
        <div class="modal-body">


            <input type="hidden" name="change_pass_id" id="change_pass_ajax">
            <footer class="center">
                <a href="#" class="js-modal-close btn btn-red">انصراف</a>
            </footer>
        </div>
    </div>

    <div id="popup" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3> ویرایش </h3>
        </header>
        <div class="modal-body">
            <form action="{{route('Admin.TextCard_edit_modal')}}"  method="post">
                <div class="row">
                    {{csrf_field()}}
                    <div class="col-md-6 h-padding">
                        <label class="label-form ">متن جمله پیش فرض</label>
                        <input class="input-form wide" name="sentence" id="sentence_by_ajax" type="text" value="" >
                    </div>
                </div>

                    <input type="hidden" id="sentence_ajax_id" name="sentence_ajax">

                <footer class="center">
                    <button class="btn btn-orange">ثبت</button>
                    <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

@endsection

