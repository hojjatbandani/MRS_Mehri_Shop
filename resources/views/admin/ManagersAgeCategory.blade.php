@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2>مدیریت گروه سنی</h2>

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>
                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ثبت  گروه سنی</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">لیست  گروه سنی</label>


                    <section class="tabcontent" id="content1">

                        <form action="{{route('Admin.AgeCategory_add')}}" method="post">

                            <div class="row">
                                <div class="col-md-12">
                                    {{csrf_field()}}
                                    <label class="label-form wide"> گروه سنی</label>
                                    <input type="text" required="required" class="input-form wide" name="age_group">
                                </div>
                            </div>
                            <footer class="panel-footer">
                                <div class="center">
                                    <input type="submit" class="btn btn-blue" value="ثبت">
                                </div>
                            </footer>
                        </form>
                    </section>
                    <section class="tabcontent" id="content2">
                        <div class="row">
                            <div class="col-md-12 h-pading">
                                @if(count($res)>0)
                                    <table class="table-defult">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>عنوان گروه سنی</th>
                                            <th>ویرایش</th>
                                            <th>حذف</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $x = 0; ?>
                                        @foreach($res as $row)
                                            <tr>
                                                <td>{{++$x}}</td>
                                                <td>{{$row->YearGroup_title}}</td>
                                                <td><a id='edit_age_of_list' class='js-open-modal colse-icon'
                                                       data-id="<?php echo $row->YearGroup_id;?>"
                                                       data-modal-id='editCat'><p class="icon-edit"></p></a></td>
                                                <td><a id='load_modal_delete_cls' class='js-open-modal colse-icon'
                                                       data-link="{{route('Admin.AgeCategory_delete',$row->YearGroup_id)}}" data-modal-id='popup-close'><span
                                                                class='icon-cancel-1'></span>x</a></td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                    @else
                                    موردی برای نمایش موجود نیست ...
                                @endif
                                <div class="pagination center">
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>

        </div>
    </div>
    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>حذف صفحه</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_user_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>
    <div id="editCat" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش</h3>
        </header>
        <div class="modal-body">

            <form action="{{route('Admin.AgeEdit')}}"  enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <input type="hidden" name="EditId" id="EditId_ajax">
                <label class="label-form wide">نام گروه سنی</label>
                <input type="text" name="EditTextAge" id="EditTextAgeId" class="input-form wide">
                <footer class="center">
                    <input type="submit" name="editSUB" class="btn btn-blue" value="ویرایش">
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>
@endsection

