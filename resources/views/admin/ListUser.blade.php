@extends('leyouts.admin')
@section('content')
    <!-- content -->
    <div class="content" id="CONTENT" xmlns="http://www.w3.org/1999/html">
        <div class="container row">
            <div class="col-md-12 table-style">
                <div class="panel">
                    <div class="panel-header">لیست کاربران</div>
                    <div class="flash-message">

                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>

                    @include('admin.partials.errors')
                    <div class="panel-body">
                        <?php if (count($list) > 0)
                        {?>
                        <table class="table-defult">
                            <thead class="dark-blue">
                            <tr>
                                <th> ردیف</th>
                                <th> نام</th>
                                <th> فامیل</th>
                                <th> ایمیل</th>
                                <th> موبایل</th>
                                <th> وضعیت</th>
                                <th> ویرایش</th>
                                <th> حذف</th>
                            </tr>
                            </thead>
                            <?php foreach ($list as $cun){
                            if ($cun['user_status'] == 'active') {
                                $status = 'فعال';
                                $data = 1;
                            } else {
                                $status = 'غیرفعال';
                                $data = 0;
                            }
                            ?>
                            <tbody>
                            <tr>
                                <td><?php echo $cun['user_id']?></td>
                                <td><?php echo $cun['user_name']?></td>
                                <td><?php echo $cun['user_family']?></td>
                                <td><?php echo $cun['user_email']?></td>
                                <td><?php echo $cun['user_phone']?></td>
                                <td><a class="ChangeStatus"
                                       href='{{route('Admin.CHS',$cun['user_id'])}}/{{$data}}'><?php echo $status;?></a>
                                </td>
                                <td><a class='js-open-modal edit-icon' href='' data-id='<?php echo $cun['user_id'];?>' data-modal-id='modal_edit_user'
                                       id='edit_user_of_list'><span class='icon-edit'></span></a></td>
                                <td><a data-modal-id='modal_delete_user'  class="js-open-modal" id='delete_user_of_list' href='' data-link="{{route('Admin.DTU',$id=$cun['user_id'])}}"><span class="icon-close"></span></a></td>
                            </tr>
                            </tbody>
                            <?php }
                            ?>
                        </table>
                        <?php
                        }else {
                            echo "کاربری وجود ندارد";
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content -->


    <div id="modal_edit_user" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش </h3>
        </header>
        <div class="modal-body">
            <div class="col-md-12">
                <div class="col-md-12">
                    <form action="{{route('Admin.ListUser.post')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="hidden_id_user" id="id_user_hidden_ajax"/>
                        <label class="label-form">نام</label>
                        <input type="text" name="name" id="name_id" class="input-form wide">
                        <label class="label-form">فامیل</label>
                        <input type="text" name="family" id="family_id" class="input-form wide">
                        <label class="label-form">ایمیل</label>
                        <input type="text" name="email" id="email_id" class="input-form wide">
                        <label class="label-form">موبایل</label>
                        <input type="text" name="phone" id="phone_id" class="input-form wide">
                        <label class="label-form">کلمه عبور</label>
                        <input type="password" name="password" id="password_id" autocomplete="off" class="input-form wide">
                </div>
            </div>
        </div>

        <footer class="panel-footer">
            <div class="center">
                <input type="submit" class="btn btn-blue" name="submit" value="ثبت">
            </div>
        </footer>
        </form>
    </div>

    <!-- The Modal -->
    <div id="modal_delete_user" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>

        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_user_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                </footer>
        </div>
    </div>

@endsection

