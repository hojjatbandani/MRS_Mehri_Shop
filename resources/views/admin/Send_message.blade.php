@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2>ارسال ایمیل و پیامک گروهی</h2>
                    <div class="flash-message">
                        @include('Web.partials.errors')
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>


                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ارسال ایمیل</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">ارسال پیامک</label>
                    <section class="tabcontent" id="content1">
                        <form action="{{route('Admin.Manage_message')}}" enctype="multipart/form-data" method="post">
                            {{csrf_field()}}

                            <div class="row select-all-bg">
                                <div class="col-md-4 col-md-offset-4 v-padding h-padding">
                                    <label class="label-form R-float">انتخاب همه</label>
                                    <!-- Rounded switch -->
                                    <label class="switch">
                                        <input type="checkbox" class="select_all_by_ajax" name="">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 h-pading">
                                    @if(count($user)>0)

                                    <table class="table-defult">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام کاربر</th>
                                            <th>ایمیل کاربر</th>
                                            <th>انتخاب</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $x = 1; ?>
                                        @foreach($user as $row)
                                        <tr>
                                            <td>{{$x++}}</td>
                                            <td>{{$row->user_name}} {{$row->user_family}}</td>
                                            <td>{{$row->user_email}}</td>
                                            <td>
                                                <label class="switch" style="float: none">
                                                    <input type="checkbox" id="" class="check_cmt" value="{{$row->user_email}}" name="target[]">
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                        کاربری  برای نمایش موجود نیست ...
                                        @endif
                                    <div class="pagination center">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <label class="label-form wide">موضوع</label>
                                <input type="text" name="email_subject" class="input-form wide">

                                <label class="label-form wide">متن ایمیل</label>
                                <textarea name="email_text" class="input-form wide"></textarea>
                            </div>


                            <footer class="panel-footer">
                                <div class="center">
                                    <input type="submit" class="btn btn-blue" value="ارسال">
                                </div>
                            </footer>
                        </form>
                    </section>

                    <section class="tabcontent" id="content2">

                        <form action="{{route('Admin.Manage_sms')}}" enctype="multipart/form-data" method="post">
                            {{csrf_field()}}
                            <div class="row select-all-bg">
                                <div class="col-md-4 col-md-offset-4 v-padding h-padding">
                                    <label class="label-form R-float">انتخاب همه</label>
                                    <!-- Rounded switch -->
                                    <label class="switch">
                                        <input type="checkbox" class="select_all_by_ajax" name="">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 h-pading">
                                    @if(count($user)>0)

                                        <table class="table-defult">
                                            <thead>
                                            <tr>
                                                <th>ردیف</th>
                                                <th>نام کاربر</th>
                                                <th>شماره همراه کاربر</th>
                                                <th>انتخاب</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $x = 1; ?>
                                            @foreach($user as $row)
                                                <tr>
                                                    <td>{{$x++}}</td>
                                                    <td>{{$row->user_name}} {{$row->user_family}}</td>
                                                    <td>{{$row->user_phone}}</td>
                                                    <td>
                                                        <label class="switch" style="float: none">
                                                            <input type="checkbox" id="" value="{{$row->user_phone}}" class="check_cmt" name="target[]">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </td>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        کاربری  برای نمایش موجود نیست ...
                                    @endif
                                    <div class="pagination center">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">

                                    <label class="label-form wide">متن پیامک</label>
                                    <textarea name="sms_text" class="input-form wide"></textarea>

                                </div>

                            </div>
                            <footer class="panel-footer">
                                <div class="center">
                                    <input type="submit" class="btn btn-blue" value="ارسال">
                                </div>
                            </footer>
                        </form>
                    </section>

                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->


    <div id="edit_card_modal" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش کارت</h3>
        </header>
        <div class="modal-body">
            <form action="{{route('Admin.EditCard')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">
                        <label class="label-form wide">مناسبت</label>
                        <select id="occasion_editID" name="occasion_edit" class="input-form wide js-example-tags">
                            <option id="oca_id_ajax" value=""></option>

                        </select>
                        <div style="display: none" id="season_editID">
                            <label class="label-form wide">فصل</label>
                            <select name="season_edit" class="input-form wide js-example-tags">
                                <option id="season_id_ajax" value=""></option>
                                <option value="1">بهار</option>
                                <option value="2">تابستان</option>
                                <option value="3">پاییز</option>
                                <option value="4">زمستان</option>
                            </select>
                        </div>
                        <label class="label-form wide">گروه سنی</label>
                        <select name="AgeCategory_edit" class="input-form wide js-example-tags">

                        </select>
                        <label class="label-form wide">دسته بندی</label>
                        <select name="category_edit" class="input-form wide js-example-tags">


                        </select>
                        <label class="label-form wide">جنسیت</label>
                        <select name="Sex_edit" class="input-form wide js-example-tags">
                            <option value="1">آقا</option>
                            <option value="0">خانم</option>
                        </select>

                        <label class="label-form wide">متن روی کارت</label>
                        <input id="text_id_ajax" type="text" name="TextCard_edit" class="input-form wide">

                        <label class="label-form wide">عکس کارت</label>
                        <input type="file" name="pic_front_edit" class="input-form wide">

                        <label class="label-form wide">عکس پشت کارت</label>
                        <input type="file" name="pic_behind_edit" class="input-form wide">
                        <input type="hidden" name="Card_id" id="card_id_ajax">
                    </div>
                </div>

                <footer class="panel-footer">
                    <div class="center">
                        <input type='submit' name="EditCard" class="btn btn-blue" value="ویرایش">
                    </div>
                </footer>
            </form>


        </div>
    </div>
    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>حذف صفحه</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_card_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

@endsection

