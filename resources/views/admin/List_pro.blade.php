@extends('leyouts.admin')
@section('content')

    <!-- content -->
    <div class="content" id="CONTENT" xmlns="http://www.w3.org/1999/html">
        <div class="container row">
            <div class="col-md-12 table-style">
                <table class="table-defult">
                    <thead class="dark-blue">
                    <tr>
                        <th>ردیف</th>
                        <th>نام</th>
                        <th>فامیل</th>
                        <th>نوع کاربر</th>
                        <th>ایمیل</th>
                        <th>موبایل</th>
                        <th>وضعیت</th>
                        <th>ویرایش</th>
                        <th>حذف</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr data-id=''>
                        <td>1</td>
                        <td>علی</td>
                        <td>محمدی</td>
                        <td>مدیر</td>
                        <td>test@gmail.com</td>
                        <td>09333335531</td>
                        <td><a class='edit_user_condition' href='#'>*</a></td>
                        <td><a class='js-open-modal edit-icon' href='' data-id='' data-modal-id='modal_edit_user'
                               id='edit_user_of_list'><span class='icon-pencil'></span>*</a></td>
                        <td><a class='delete_user_of_list' data-id='' href='#'><i class=\"icon-cancel\"></i>*</a></td>
                    </tr>

                    </tbody>
                </table>
                <div class="pagenation">
                </div>
            </div>
        </div>
    </div>
    <!-- content -->


    <div id="modal_edit_user" class="modal-box">
        <header>
            <a href="" class="js-modal-close close">×</a>
            <h3>ویرایش </h3>
        </header>
        <div class="modal-body">
            <div class="col-md-12">
                <div class="col-md-12">
                    <form action="" method="post">
                        <input type="hidden" name="hidden_id_user" id="id_user_hidden_ajax"/>
                        <label class="label-form">نام</label>
                        <input type="text" class="input-form wide">
                        <label class="label-form">فامیل</label>
                        <input type="text" class="input-form wide">
                        <label class="label-form">ایمیل</label>
                        <input type="text" class="input-form wide">
                        <label class="label-form">موبایل</label>
                        <input type="text" class="input-form wide">
                        <label class="label-form">کلمه عبور</label>
                        <input type="text" class="input-form wide">
                        <label class="label-form">نوع</label>
                        <input type="text" class="input-form wide">

                </div>
            </div>
        </div>

        <footer class="panel-footer">
            <div class="center">
                <input type="submit" class="btn btn-blue" name="submit" value="ثبت">
            </div>
        </footer>
        </form>
    </div>

    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3> محمد کرمی</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <button class="btn btn-orange">حذف شود</button>
                    <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

@endsection

