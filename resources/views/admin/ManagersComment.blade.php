@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT" xmlns="http://www.w3.org/1999/html">
        <div class="container row">
            <div class="col-md-12 table-style">
                <div class="panel">
                    <div class="panel-header">مدیریت نظرات</div>
                    <div class="flash-message">
                        @include('Web.partials.errors')
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>
                    <div class="panel-body">
                        <?php  if (count($res) > 0)
                        {?>
                        <table class="table-defult">
                            <thead class="dark-blue">
                            <tr>
                                <th>ردیف</th>
                                <th>نام و نام خانوادگی</th>
                                <th>نوع دسته بندی</th>
                                <th>نظر</th>
                                <th>وضعیت</th>
                                <th>حذف</th>
                            </tr>
                            </thead>
                            <?php
                            foreach ($res as $count){
                            if ($count['comment_status'] == 'active') {
                                $status = 'قابل نمایش';
                                $s = '1';
                            } else {
                                $status = 'غیر قابل نمایش';
                                $s = '0';
                            }
                            ?>
                            <tbody>
                            <tr>
                                <td><?php echo $count['comment_id']?></td>
                                <td><?php echo $count['user_name'] . ' ' . $count['user_family']?></td>
                                <td><?php echo $count['cat_name']?></td>
                                <td><?php echo $count['comment_name']?></td>
                                <td>
                                    <a href="<?php echo route('Admin.ChangeComment', $count['comment_id'])?>/<?php echo $s;?>"><?php echo $status?></a>
                                </td>
                                <td><a id="delete_comment_of_list" class='js-open-modal colse-icon'
                                       data-modal-id='popup-close'
                                       data-link="<?php echo route('Admin.DeleteComment', $id = $count['comment_id']);?>"
                                       href='#'><i class="icon-close"></i></a></td>
                            </tr>

                            </tbody>
                            <?php
                            }
                            ?>
                        </table>

                        <div class="pagenation">
                        </div>
                        <?php
                        }else {
                            echo 'نظری وجود ندارد';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="">
                <footer class="center">
                    <a href="" id="DeleteCommentAjax" class="btn btn-orange">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

@endsection

