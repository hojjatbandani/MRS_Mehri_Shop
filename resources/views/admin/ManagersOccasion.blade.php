@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2>مدیریت مناسبات</h2>
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>
                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ثبت مناسبت</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">لیست مناسبات</label>


                    <section class="tabcontent" id="content1">

                        <form action="{{route('Admin.Occasion')}}" method="post">

                            <div class="row">
                                <div class="col-md-12">
                                    {{csrf_field()}}

                                    <label class="label-form wide">نام مناسبت</label>
                                    <input type="text" required="required" name="occasion_name" class="input-form wide">


                                    <label class="label-form wide">زیر دسته</label>
                                    <select  name="occasion_load" class="input-form wide js-example-tags">
                                        <option value="0">انتخاب کنید</option>
                                        <option value="2">عید</option>
                                    </select>

                                </div>
                            </div>

                            <footer class="panel-footer">
                                <div class="center">
                                    <input type="submit" class="btn btn-blue" value="ثبت">
                                </div>
                            </footer>
                        </form>
                    </section>

                    <section class="tabcontent" id="content2">

                        <div class="row">
                            <div class="col-md-12 h-pading">
                                @if(count($occasion)>0)
                                    <p class="center"><a class='js-open-modal colse-icon '
                                                         data-modal-id='myModal_setting' href="#">برای تغییر اولویت کلیک
                                            کنید</a></p>
                                    <table class="table-defult">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>نام مناسبت</th>
                                            <th>اولویت</th>
                                            <th>حذف</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <div style="display: none">{{$x = 0}}</div>

                                        @foreach($occasion as $row)
                                            <tr>
                                                <td>{{++$x}}</td>
                                                <td>{{$row->occasion_text}}</td>
                                                <td>{{$row->occasion_priority}}</td>
                                                <td><a id='load_modal_delete_cls' class='js-open-modal colse-icon'
                                                       data-link="{{route('Admin.Occasion_delete',$row->occasion_id)}}"
                                                       data-modal-id='popup-close'><span
                                                                class='icon-cancel-1'></span>x</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    موردی یافت نشد
                                @endif
                                <div class="pagination center">
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->


    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>حذف صفحه</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_user_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

    <div id="myModal_setting" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش اولویت</h3>
        </header>
        <div class="modal-body">
            <div class="row special">
                <div class="col-md-10 col-md-offset-1">
                    <div class="alert alert-info alert-dismissable fade in">
                        تغییرات به صورت خودکار انجام میشود.
                    </div>
                </div>
            </div>

            <!--                                        <p class="center">-->
            <!--                                            <a href="#" data-toggle="modal" data-target="#myModal_add">-->
            <!--                                                <i class="fa fa-plus"></i> افزودن درگاه-->
            <!--                                            </a>-->
            <!--                                        </p>-->

            <div class="table-responsive" id="table_bank_list_1">
                <table class='table'>
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>نام دسته</th>
                        <th>اولویت بندی</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($occasion as $row)

                        <tr>
                            <td data-title='ردیف'>{{++$x}}</td>
                            <td data-title='نام'>{{$row->occasion_text}}</td>
                            <td data-title='اولویت بندی'>
                                <input type='number' class='input-form wide col_id_by_ajax' value="{{$row->occasion_priority}}"
                                        data-id="{{$row->occasion_id}}">
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
                <footer class="panel-footer">
                    <div class="center">
                        <a  class="btn btn-blue"  href="">ثبت</a>
                    </div>
                </footer>
            </div>
        </div>
    </div>

@endsection

