@extends('leyouts.admin')
@section('content')

    <!-- content -->
    <div class="content" id="CONTENT" xmlns="http://www.w3.org/1999/html">
        <div class="container row">
            <div class="col-md-12 table-style">
                <div class="panel">
                    <div class="panel-header">لیست تراکنش ها</div>
                    @include('admin.partials.errors')
                    <div class="panel-body">
                        <?php if (count($list) > 0)
                        {?>
                        <table class="table-defult">
                            <thead class="dark-blue">
                            <tr>
                                <th> ردیف</th>
                                <th> نام و نام خانوادگی</th>
                                <th> متن سفارشی کارت</th>
                                <th>نوع ارسال </th>
                                <th> قیمت سفارش</th>
                                <th> مبلغ تراکنش</th>
                                <th> نوع تراکنش</th>
                                <th> تاریخ تراکنش</th>
                            </tr>
                            </thead>
                            <?php foreach ($list as $cun){
                            if ($cun['authorycode_refid']!=null) {
                                $type = 'پرداخت شده';
                                $data = 1;
                            } else {
                                $type = 'پرداخت نشده';
                                $data = 0;
                            }
                                ?>
                            <tbody>
                            <tr>
                                <td><?php echo $cun['transaction_id']?></td>
                                <td><?php echo $cun['user_name'].' '.$cun['user_family']?></td>
                                <td><?php echo $cun['Orders_cardtext']?></td>
                                <td><?php echo $cun['Orders_send_type']?></td>
                                <td><?php echo $cun['Orders_price_card']?></td>
                                <td><?php echo $cun['transaction_amount']?></td>
                                <td><?php echo $type;?></td>
                                <td><?php echo $cun['transantion_date']?></td>
                            </tr>
                            </tbody>
                            <?php }
                            ?>
                        </table>
                        <?php
                        }else {
                            echo "تراکنشی وجود ندارد";
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <button class="btn btn-orange">حذف شود</button>
                    <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

@endsection

