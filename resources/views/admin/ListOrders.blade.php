@extends('leyouts.admin')
@section('content')

    <!-- content -->
    <div class="content" id="CONTENT" xmlns="http://www.w3.org/1999/html">
        <div class="container row">
            <div class="col-md-12 table-style">
                <div class="panel">
                    <div class="panel-header">لیست سفارشات</div>
                    <div class="flash-message">
                        @include('Web.partials.errors')
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>

                    @include('admin.partials.errors')
                    <div class="panel-body">
                        <?php  if (count($res) > 0)
                        {?>
                            <table class="table-defult">
                                <thead class="dark-blue">
                            <tr>
                                <th>ردیف</th>
                                <th>دسته بندی</th>
                                <th>متن روی کارت</th>
                                <th>شماره همراه دریافت کننده</th>
                                <th>آدرس دریافت کننده</th>
                                <th>نوع ارسال</th>
                                <th>حذف</th>
                            </tr>
                            </thead>
                            <?php
                            foreach ($res as $count) {
                                $type = '';
                                if ($count['Orders_send_type'] == '1') {
                                    $type = 'ایمیل';
                                }
                                if ($count['Orders_send_type'] == '2') {
                                    $type = 'پست پیشتاز';
                                }
                                echo "
                           <tbody>
                          <tr>
                            <td>$count[Orders_id]</td>
                            <td>$count[cat_name]</td>
                            <td>$count[Orders_cardtext]</td>
                            <td>$count[Orders_receiver_phone]</td>
                            <td>$count[Orders_receiver_address]</td>
                            <td>$type</td>
                             <td><a  id='delete_order_of_list' class='js-open-modal colse-icon'
                              data-modal-id='popup-close'
                              data-link=" . route('Admin.DeleteOrders', $id = $count['Orders_id']) . "
                              href='#'><i class='icon-close'></i></a></td>
                          </tr>

                    </tbody>
                      </table>
                ";
                            }
                            }else {
                                echo 'سفارشی وجود ندارد';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content -->

        <!-- The Modal -->
        <div id="popup-close" class="modal-box">
            <header>
                <a href="#" class="js-modal-close close">×</a>
            </header>
            <div class="modal-body">
                <p> آیا مطمین هستید ؟ </p>
                <form action="#">
                    <footer class="center">
                        <a id="DeleteOrderAjax" class="btn btn-blue">حذف شود</a>
                        <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                    </footer>
                </form>
            </div>
        </div>

@endsection

