@extends('leyouts.admin')
@section('content')

    <!-- content -->
    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2>ثبت و نمایش مدیران میانی</h2>

                    <div id="msg_pass_id" class="msg_success"></div>

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div>
                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ثبت مدیران میانی</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">لیست مدیران میانی</label>


                    <section class="tabcontent" id="content1">

                        <form action="{{route('Admin.MiddleManagers')}}" method="post">

                        <div class="row">
                            <div class="col-md-12">
                                {{csrf_field()}}

                                <label class="label-form wide">نام کاربر</label>
                                <input type="text" required="required" name="username" class="input-form wide">

                                <label class="label-form wide">فامیل کاربر</label>
                                <input type="text" name="family" class="input-form wide" required="required">


                                <label class="label-form wide">ایمیل کاربر</label>
                                <input type="email" name="user_email" class="input-form wide" required="required">


                                <label class="label-form wide">رمز عبور</label>
                                <input type="password" name="password" class="input-form wide new_pass" required="required">


                                <div class="msg_error"></div>
                                <label class="label-form wide">تکرار رمز عبور</label>
                                <input type="password" class="input-form wide reply_pass_ajax" >

                                <div id="msg_pass_id" class="msg_error"></div>
                                <div class="row">
                                    <div class="row select-all-bg">
                                        <div class="col-md-4 col-md-offset-4 v-padding h-padding">
                                            <label class="label-form R-float">انتخاب همه</label>
                                            <!-- Rounded switch -->
                                            <label class="switch">
                                                <input type="checkbox" id="select_all_by_ajax" name="">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">صفحه داشبورد</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="dashboard_page">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">لیست کاربران</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="list_user">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">لیست سفارشات</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="orders_list">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">مدیران میانی</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="modir_miani">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float"> مدیریت دسته ها </label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="manage_cat">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float"> مدیریت کارت ها </label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="manage_card">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float"> مدیریت مناسبت ها</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="manage_occasion">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float"> مدیریت گروه های سنی</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox"  class="check_cmt" required="required"  name="manage_age">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">مدیریت جملات پیش فرض</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="manage_Sentence">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">مدیریت نظرات</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="manage_comment">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">مدیریت تبلیغات</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt"required="required" name="manage_adv">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">مدیریت تراکنش ها</label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox"  class="check_cmt" required="required"  name="manage_transaction">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                    <div class="col-md-4 v-padding h-padding">
                                        <label class="label-form R-float">مدیریت صفحات </label>
                                        <!-- Rounded switch -->
                                        <label class="switch">
                                            <input type="checkbox" class="check_cmt" required="required" name="manage_pages">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div >
                            </div>
                        </div>

                        <footer class="panel-footer">
                            <div class="center">
                                <input type="submit" value="ثبت" class="btn btn-blue">
                            </div>
                        </footer>
                    </form>
                    </section>

                    <section class="tabcontent" id="content2">

                        <div class="row">
                            <div class="col-md-12 h-pading">
                                <?php if (count($load)>0){ ?>
                                <table class="table-defult">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام</th>
                                        <th>رمز عبور</th>
                                        <th>ویرایش دسترسی</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                            $x =0;
                                    foreach ($load as $user){

                                        ?>
                                    <?php $id = $user->Admin_id?>
                                <tr>
                                    <td><?php echo ++$x;?></td>
                                    <td><?php echo $user->Admin_username.' '.$user->Admin_family ;?></td>
                                    <td><a class='js-open-modal edit-icon' data-id="<?php echo $user->Admin_id ?>"  href='#' data-modal-id='change_pass' id='change_pass_id_ajax'> تغییر رمز</a></td>
                                    <td><a class='js-open-modal edit-icon' data-token='{{csrf_field()}}' data-id="<?php echo $user->Admin_id ?>" href='#' data-modal-id='popup' id='show_manager_by_ajax'><span
                                                    class='icon-eye'></span></a></td>
                                    <td><a id='load_modal_delete_cls' class='js-open-modal colse-icon' data-link="{{route('Admin.delete_manager',$id)}}" data-modal-id='popup-close'><span
                                                    class='icon-cancel-1'></span>x</a></td>
                                </tr>
                                    <?php } ?>

                                    </tbody>
                                </table>
                                    <?php }else{
                                    echo 'موردی یافت نشد';
                                    } ?>
                                <div class="pagination center">
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->
    <div id="popup" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3> نمایش دسترسی </h3>
        </header>
        <div class="modal-body">
            <form action="{{route('Admin.MiddleManagers_edit')}}" method="post">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">

                        {{csrf_field()}}

                        <input type="hidden" name="hidden_id" id="hidden_id_ajax">
                        <div class="row v-padding">
                            <label class="label-form R-float">صفحه داشبورد</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="dashboard_page_ajax" name="dashboard_page">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="row v-padding">
                            <label class="label-form R-float">لیست کاربران</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="list_user_ajax" name="list_user">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="row v-padding">
                            <label class="label-form R-float">لیست سفارشات</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="orders_list_ajax" name="orders_list">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="row v-padding">
                            <label class="label-form R-float">مدیران میانی</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="modir_miani_ajax" name="modir_miani">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="row v-padding">
                            <label class="label-form R-float"> مدیریت دسته ها </label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="manage_cat_ajax" name="manage_cat">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="row v-padding">
                            <label class="label-form R-float"> مدیریت کارت ها </label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="manage_card_ajax" name="manage_card">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="row v-padding">
                            <label class="label-form R-float"> مدیریت مناسبت ها</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="manage_occasion_ajax" name="manage_occasion">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="row v-padding">
                            <label class="label-form R-float"> مدیریت گروه های سنی</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="manage_age_ajax" name="manage_age">
                                <span class="slider round"></span>
                            </label>
                        </div>

                        <div class="row v-padding">
                            <label class="label-form R-float">مدیریت جملات پیش فرض</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="manage_Sentence_ajax" name="manage_Sentence">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="row v-padding">
                            <label class="label-form R-float">مدیریت نظرات</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="manage_comment_ajax" name="manage_comment">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="row v-padding">
                            <label class="label-form R-float">مدیریت تبلیغات</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox" id="manage_adv_ajax" name="manage_adv">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="row v-padding">
                            <label class="label-form R-float">مدیریت تراکنش ها</label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox"  id="manage_transaction_ajax" name="manage_transaction">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="row v-padding">
                            <label class="label-form R-float">مدیریت صفحات </label>
                            <!-- Rounded switch -->
                            <label class="switch">
                                <input type="checkbox" class="check_cmt" id="manage_pages_ajax" name="manage_pages">
                                <span class="slider round"></span>
                            </label>
                        </div>

                    </div>
                </div>
                <footer class="panel-footer">
                    <div class="center">
                        <input type="submit" value="ثبت" class="btn btn-blue">
                        <a href="#"  class="js-modal-close btn btn-red">انصراف</a>
                    </div>
                </footer>
            </form>
        </div>
    </div>

    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>حذف صفحه</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <a href="" class="btn btn-blue" id="delete_user_ajax">حذف شود</a>
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

    <div id="change_pass" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>تغییر رمز عبور</h3>
        </header>
        <div class="modal-body">


            <label class="label-form wide">رمز عبور</label>
            <input type="password" name="password" class="input-form wide new_pass_ajax">


            <div id="msg_pass_id" class="msg_error"></div>
            <label class="label-form wide">تکرار رمز عبور</label>
            <input type="password" class="input-form wide reply_pass_ajax_id">

            <input type="hidden" name="change_pass_id" id="change_pass_ajax">
            <footer class="center">
                <a href="#" id="change_pass_ajax_form" class="js-modal-close btn btn-blue">ثبت</a>
                <a href="#"  class="js-modal-close btn btn-red">انصراف</a>
            </footer>
        </div>
    </div>



@endsection

