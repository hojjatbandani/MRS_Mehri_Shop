@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    @include('Web.partials.errors')
                    <h2>مدیریت دسته ها</h2>
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}
                                </p>
                            @endif
                        @endforeach
                    </div>

                    <input class="tab-input" id="tab1" type="radio" name="tabs" checked>
                    <label class="tab-label" for="tab1">ثبت دسته</label>

                    <input class="tab-input" id="tab2" type="radio" name="tabs">
                    <label class="tab-label" for="tab2">لیست دسته ها</label>


                    <section class="tabcontent" id="content1">

                        <form action="{{route('Admin.Category')}}" enctype="multipart/form-data" method="post">

                            <div class="row">
                                <div class="col-md-12">
                                    {{csrf_field()}}

                                    <label class="label-form wide">نام دسته</label>
                                    <input type="text" required="required" name="cat_name" class="input-form wide">


                                    <label class="label-form wide"> دسته </label>
                                    <select name="cat" class="input-form wide parent_cat_ajax">
                                        <option value="0">انتخاب کنید</option>
                                        <?php echo $cat;?>
                                    </select>

                                    <div class="child_category_ajax">

                                        <label class="label-form wide"> زیر دسته </label>
                                        <select name="cat_child" id="child_of_parent_cat" class="input-form wide">

                                        </select>
                                    </div>


                                    <div class="show_or_hide_by_ajax">
                                        <label class="label-form wide">تصویر دسته</label>
                                        <input type="file" name="image_file"
                                               class="input-form wide">
                                        <div id="msg_pass_id" class="msg_error"></div>
                                    </div>

                                </div>
                            </div>

                            <footer class="panel-footer">
                                <div class="center">
                                    <input type="submit" class="btn btn-blue" value="ثبت">
                                </div>
                            </footer>
                        </form>
                    </section>

                    <section class="tabcontent" id="content2">

                        <div class="row">
                            <div class="col-md-12 h-pading">

                                <?php if (count($category) > 0){ ?>
                                <table class="table-defult">
                                    <thead>
                                    <tr>
                                        <th>ردیف</th>
                                        <th>نام</th>
                                        <th>عکس</th>
                                        <th>ویرایش</th>
                                        <th>حذف</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $x = 0;
                                    foreach ($category as $cat1){
                                    $id = $cat1->cat_id;
                                    ?>
                                    <tr>
                                        <td><?php echo ++$x;?></td>
                                        <td><?php echo $cat1->cat_name;?></td>
                                        <td><img style='width:100px;border:1px solid #ccc;border-radius:5px;' src='<?php echo route('home')."/uploads/category/". $cat1->image?>' alt='عکس یافت نشد'></td>
                                        <td><a id='edit_category_of_list' class='js-open-modal colse-icon'
                                               data-id="<?php echo $cat1->cat_id;?>"
                                               data-modal-id='editCat'><p class="icon-edit"></p></a></td>
                                        <td><a id='load_modal_delete_cls' class='js-open-modal colse-icon'
                                               data-link="{{route('Admin.Category_delete',$id)}}"
                                               data-modal-id='popup-close'><span
                                                        class='icon-cancel-1'></span>x</a></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <?php }else {
                                    echo "موردی برای نمایش یافت نشد";
                                }?>
                                <div class="pagination center">
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>حذف صفحه</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <footer class="center">
                <a href="" class="btn btn-blue" id="delete_user_ajax">حذف شود</a>
                <a href="#" class="js-modal-close btn btn-red">انصراف</a>
            </footer>
        </div>
    </div>

    <div id="editCat" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3>ویرایش</h3>
        </header>
        <div class="modal-body">

            <form action="{{route('Admin.CategoryEdit')}}"  enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <input type="hidden" name="EditId" id="EditId_ajax">
                <label class="label-form wide">نام دسته</label>
                <input type="text" name="EditTextCat" id="EditTextCatId" class="input-form wide">
                <label class="label-form wide">تصویر دسته</label>
                <input type="file" name="image_file_edit"
                       class="input-form wide">
                <td><img style='width:100px;border:1px solid #ccc;border-radius:5px;' id="edit_img" src='' alt='عکس یافت نشد' /></td>
                <footer class="center">
                    <input type="submit" name="editCategorySUB" class="btn btn-blue" value="ویرایش">
                    <a href="#" class="js-modal-close btn btn-red">انصراف</a>
                </footer>
            </form>
        </div>
    </div>
@endsection

