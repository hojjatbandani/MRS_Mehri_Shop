@extends('leyouts.admin')
@section('content')

    <div class="content" id="CONTENT">
        <div class="container ">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel submitAd-panel">
                        <header class="panel-header">
                            ثبت و مدیریت محصولات
                        </header>
                        <div class="panel-body">
                            <form action="#">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="label-form">نام محصول</label>
                                        <input class="input-form wide" type="text" placeholder="">

                                        <label class="label-form wide">تاریخ</label>
                                        <input class="input-form wide" type="date" placeholder="">

                                        <label class="label-form wide">قیمت محصول</label>
                                        <input class="input-form wide" type="text" placeholder="">

                                        <label class="label-form wide">توضیحات</label>
                                        <input class="input-form wide" type="text" placeholder="">

                                        <label class="label-form wide">آپلود فایل</label>
                                        <input class="input-form wide" type="file" placeholder="">

                                        <label class="label-form wide">آپلود تصویر</label>
                                        <input class="input-form wide" type="file" placeholder="">

                                    </div>

                                </div>

                                <footer class="panel-footer">
                                    <div class="center">
                                        <button class="btn btn-blue">ثبت</button>
                                    </div>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- The Modal -->
    <div id="popup" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3> محمد کرمی</h3>
        </header>
        <div class="modal-body">
            <form action="#">
                <div class="row">
                    <div class="col-md-6 h-padding">
                        <label class="label-form ">نام</label>
                        <input class="input-form wide" type="text" value="" placeholder="">
                    </div>
                    <div class="col-md-6 h-padding">
                        <label class="label-form ">نام خانوادگی</label>
                        <input class="input-form wide" type="text" value="" placeholder="">
                    </div>
                    <div class="col-md-6 h-padding">
                        <label class="label-form ">ایمیل</label>
                        <input class="input-form wide" type="text" value="" placeholder="">
                    </div>
                    <div class="col-md-6 h-padding">
                        <label class="label-form ">همراه</label>
                        <input class="input-form wide" type="text" value="" placeholder="">
                    </div>
                </div>

                <footer class="center">
                    <button class="btn btn-orange">ثبت</button>
                    <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

    <!-- The Modal -->
    <div id="popup-close" class="modal-box">
        <header>
            <a href="#" class="js-modal-close close">×</a>
            <h3> محمد کرمی</h3>
        </header>
        <div class="modal-body">
            <p> آیا مطمین هستید ؟ </p>
            <form action="#">
                <footer class="center">
                    <button class="btn btn-orange">حذف شود</button>
                    <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
                </footer>
            </form>
        </div>
    </div>

@endsection

