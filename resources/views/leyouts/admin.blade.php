<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>مدیریت</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{route('home')}}/admin/fonts/iransans/css/fontiran.css">
    <link rel="stylesheet" type="text/css" href="{{route('home')}}/admin/css/fontello.css">
    <link rel="stylesheet" type="text/css" href="{{route('home')}}/admin/css/style.css">
</head>


<body class="admin">

<!-- header -->
<header class="topHeader bg-top-header">

    <div id="openSidebar" class="sidebar-btn">
        <button class="bg-orange">&#9776;</button>
    </div>
    <div id="closeSidebar" class="sidebar-btn">
        <button class="bg-orange">&times;</button>
    </div>

    <nav class="navbar menu">
        <ul class="active">
            <li class="current-item"><a href="<?php echo route('Admin.Dashboard');?>" class="">صفحه داشبورد</a></li>
            <li><a href="#" class="">نمایش سایت</a></li>
            <li><a href="#" class="dropbtn">بیشتر</a></li>
        </ul>

        <a class="toggle-nav" href="<?php echo route('home')?>">&#9776;</a>

        <div class="logo">
            <a href="#"><img src="{{route('home')}}/user/img/logo.png" alt=""></a>
        </div>
    </nav>
</header>

<!-- sidebar -->
<div class="sidebar" id="SIDEBAR">
    <ul>
        <li>
            <a href='<?php echo route('Admin.Dashboard');?>'>
                <span class="icon-038-speedometer-1"></span>
                <span class="icon-house"></span>
                <span class="SidebarTxt">صفحه داشبورد</span>
            </a>
        </li>
        <li>
            <a href='<?php echo route('Admin.ListUser');?>'>
                <span class="icon-034-group-1 dark-gray"></span>
                <span class="icon-users"></span>
                <span class="SidebarTxt">لیست کاربران</span>
            </a>
        </li>
        <li>
            <a href='<?php echo route('Admin.Orders');?>'>
                <span class="icon-028-car"></span>
                <span class="icon-users"></span>
                <span class="SidebarTxt">لیست سفارشات</span>
            </a>
        </li>
        <li>
            <a href='<?php echo route('Admin.MiddleManagers');?>'>
                <span class="icon-027-billboard"></span>
                <span class="icon-layers"></span>
                <span class="SidebarTxt">مدیران میانی</span>
            </a>
        </li>
        <li>
            <a href='<?php echo route('Admin.Category');?>'>
                <span class="icon-024-content dark-gray"></span>
                <span class="icon-document"></span>
                <span class="SidebarTxt"> مدیریت دسته ها</span>
            </a>
        </li>
        <li>
            <a href='<?php echo route('Admin.Cards');?>'>
                <span class="icon-011-dollar-symbol"></span>
                <span class="icon-layers"></span>
                <span class="SidebarTxt"> مدیریت کارت ها </span>
            </a>
        </li>
        <li>
            <a href='<?php echo route('Admin.Occasion');?>'>
                <span class="icon-017-group-4"></span>
                <span class="icon-layers"></span>
                <span class="SidebarTxt">مدیریت مناسبت ها</span>
            </a>
        </li>
        <li>
            <a href='<?php echo route('Admin.AgeCategory');?>'>
                <span class="icon-014-money-bag"></span>
                <span class="icon-document"></span>
                <span class="SidebarTxt"> مدیریت گروه های سنی</span>
            </a>
        </li>
        <li class='active'>
            <a href='<?php echo route('Admin.ManageSEND');?>'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>

                <span class="SidebarTxt">مدیریت کد تخفیف </span>
            </a>
        </li>
        <li class='active'>
            <a href='<?php echo route('Admin.Manage_message');?>'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>

                <span class="SidebarTxt">ارسال ایمیل و پیامک گروهی</span>
            </a>
        </li>
        <li class='active'>
            <a href='<?php echo route('Admin.ManageSEND');?>'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>

                <span class="SidebarTxt">مدیریت ارسال پستی </span>
            </a>
        </li>
        <li class='active'>
            <a href='<?php echo route('Admin.TextCard');?>'>
                <span class="icon-023-content-1"></span>
                <span class="icon-document"></span>
                <span class="SidebarTxt">مدیریت جملات پیش فرض</span>
            </a>
        </li>
        <li class='active'>
            <a href='<?php echo route('Admin.Comment');?>'>
                <span class="icon-023-content-1"></span>
                <span class="icon-compose"></span>

                <span class="SidebarTxt">مدیریت نظرات</span>
            </a>
        </li>
        <li class='active'>
            <a href='<?php echo route('Admin.Advertising');?>'>
                <span class="icon-023-content-1"></span>
                <span class="icon-compose"></span>

                <span class="SidebarTxt">مدیریت تبلیغات</span>
            </a>
        </li>
        <li class='active'>
            <a href='<?php echo route('Admin.Transaction');?>'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>

                <span class="SidebarTxt">مدیریت تراکنش ها </span>
            </a>
        </li>
        <li class='active'>
            <a href='<?php echo route('Admin.AboutUs');?>'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>

                <span class="SidebarTxt">مدیریت صفحات </span>
            </a>
        </li>
        <li>
            <a href='<?php echo route('home')?>'>
                <span class="icon-001-power-button-off dark-gray"></span>
                <span class="SidebarTxt">خروج</span>
            </a>
        </li>
    </ul>
</div>

<!-- content -->
@yield('content')

<!-- The Modal -->

<!-- The Modal -->
<div id="popup-close" class="modal-box">
    <header>
        <a href="#" class="js-modal-close close">×</a>
        <h3> محمد کرمی</h3>
    </header>
    <div class="modal-body">
        <p> آیا مطمین هستید ؟ </p>
        <form action="#">
            <footer class="center">
                <button class="btn btn-orange">حذف شود</button>
                <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
            </footer>
        </form>
    </div>
</div>


<!-- ======== js files ============ -->

<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="{{route('home')}}/admin/js/main.js"></script>
</body>
</html>