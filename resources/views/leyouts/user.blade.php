<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>اگهی خودرو - شرایط و قوانین</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="fonts/iransans/css/fontiran.css">
    <link rel="stylesheet" type="text/css" href="css/fontello.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>


<body class="admin">

<!-- header -->
<header class="topHeader bg-top-header">

    <div id="openSidebar" class="sidebar-btn">
        <button class="bg-orange">&#9776;</button>
    </div>
    <div id="closeSidebar" class="sidebar-btn">
        <button class="bg-orange">&times;</button>
    </div>

    <nav class="navbar menu">
        <ul class="active">
            <li class="current-item"><a href="#" class="">صفحه داشبورد</a></li>
            <li><a href="#" class="">نمایش سایت</a></li>
            <li><a href="#" class="dropbtn">بیشتر</a></li>
        </ul>

        <a class="toggle-nav" href="#">&#9776;</a>

        <div class="logo">
            <a href="#"><img src="user/img/logo.png" alt=""></a>
        </div>
    </nav>
</header>

<!-- sidebar -->
<div class="sidebar" id="SIDEBAR">
    <ul>
        <li>
            <a href='#'>
                <span class="icon-038-speedometer-1"></span>
                <span class="icon-house"></span>
                <span class="SidebarTxt">صفحه داشبورد</span>
            </a>
        </li>
        <li>
            <a href='#'>
                <span class="icon-034-group-1 dark-gray"></span>
                <span class="icon-users"></span>
                <span class="SidebarTxt">لیست کاربران</span>
            </a>
        </li>
        <li>
            <a href='#'>
                <span class="icon-028-car"></span>
                <span class="icon-users"></span>
                <span class="SidebarTxt"> مدیران میانی</span>
            </a>
        </li>
        <li>
            <a href='#'>
                <span class="icon-027-billboard"></span>
                <span class="icon-layers"></span>
                <span class="SidebarTxt">مدیریت محصولات</span>
            </a>
        </li>
        <li>
            <a href='#'>
                <span class="icon-024-content dark-gray"></span>
                <span class="icon-document"></span>
                <span class="SidebarTxt"> مدیریت نظرات</span>
            </a>
        </li>
        <li>
            <a href='#'>
                <span class="icon-011-dollar-symbol"></span>
                <span class="icon-layers"></span>
                <span class="SidebarTxt"> مدیریت سفارشات</span>
            </a>
        </li>
        <li>
            <a href='#'>
                <span class="icon-017-group-4"></span>
                <span class="icon-layers"></span>
                <span class="SidebarTxt">مدیریت اسلایدر</span>
            </a>
        </li>
        <li>
            <a href='#'>
                <span class="icon-014-money-bag"></span>
                <span class="icon-document"></span>
                <span class="SidebarTxt"> مدیریت کارت تخفیف</span>
            </a>
        </li>

        <li class='active'>
            <a href='#'>
                <span class="icon-023-content-1"></span>
                <span class="icon-document"></span>
                <span class="SidebarTxt">مدیریت دسته ها و زیر دسته ها </span>
            </a>
        </li>
        <li class='active'>
            <a href='#'>
                <span class="icon-023-content-1"></span>
                <span class="icon-compose"></span>

                <span class="SidebarTxt">مدیریت جملات دلخواه </span>
            </a>
        </li>
        <li class='active'>
            <a href='#'>
                <span class="icon-023-content-1"></span>
                <span class="icon-compose"></span>

                <span class="SidebarTxt">افزودن اعیاد</span>
            </a>
        </li>
        <li class='active'>
            <a href='#'>
                <span class="icon-023-content-1"></span>
                <span class="icon-compose"></span>

                <span class="SidebarTxt">مدیریت درباره ی ما </span>
            </a>
        </li>
        <li class='active'>
            <a href='#'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>

                <span class="SidebarTxt">مدیریت تراکنش ها </span>
            </a>
        </li>
        <li class='active'>
            <a href='#'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>

                <span class="SidebarTxt">مدیریت درگاه بانک </span>
            </a>
        </li>
        <li class='active'>
            <a href='#'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>

                <span class="SidebarTxt">مدیریت تبلیغات </span>
            </a>
        </li>
        <li class='active'>
            <a href='#'>
                <span class="icon-023-content-1"></span>
                <span class="icon-file-1"></span>
                <span class="SidebarTxt">مدیریت چاپ الگوهای انتحاب شده </span>
            </a>
        </li>
        <li>
            <a href='#'>
                <span class="icon-001-power-button-off dark-gray"></span>
                <span class="SidebarTxt">خروج</span>
            </a>
        </li>
    </ul>
</div>

<!-- content -->
@yield('content')

<!-- The Modal -->
<div id="popup" class="modal-box">
    <header>
        <a href="#" class="js-modal-close close">×</a>
        <h3> محمد کرمی</h3>
    </header>
    <div class="modal-body">
        <form action="#">
            <div class="row">
                <div class="col-md-6 h-padding">
                    <label class="label-form ">نام</label>
                    <input class="input-form wide" type="text" value="" placeholder="">
                </div>
                <div class="col-md-6 h-padding">
                    <label class="label-form ">نام خانوادگی</label>
                    <input class="input-form wide" type="text" value="" placeholder="">
                </div>
                <div class="col-md-6 h-padding">
                    <label class="label-form ">ایمیل</label>
                    <input class="input-form wide" type="text" value="" placeholder="">
                </div>
                <div class="col-md-6 h-padding">
                    <label class="label-form ">همراه</label>
                    <input class="input-form wide" type="text" value="" placeholder="">
                </div>
            </div>

            <footer class="center">
                <button class="btn btn-orange">ثبت</button>
                <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
            </footer>
        </form>
    </div>
</div>

<!-- The Modal -->
<div id="popup-close" class="modal-box">
    <header>
        <a href="#" class="js-modal-close close">×</a>
        <h3> محمد کرمی</h3>
    </header>
    <div class="modal-body">
        <p> آیا مطمین هستید ؟ </p>
        <form action="#">
            <footer class="center">
                <button class="btn btn-orange">حذف شود</button>
                <a href="#" class="js-modal-close btn btn-orange">انصراف</a>
            </footer>
        </form>
    </div>
</div>


<!-- ======== js files ============ -->

<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="js/main.js"></script>
</body>
</html>