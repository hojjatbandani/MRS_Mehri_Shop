<!DOCTYPE html>
<html>
<head>
    <title>shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo route('home')?>/user/fonts/iransans/css/fontiran.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo route('home')?>/user/lib/mighty/src/css/mightyslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo route('home')?>/user/css/animate.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinModern.css">
    <link rel="stylesheet" type="text/css" href="<?php echo route('home')?>/user/css/style.css">
</head>



<header class="topHeader" id="home">
    <div class="nav_top">
        <div class="wrapper">
            <ul>
                <li>
                    <a href="{{ Route('home.Login')}}">
                        <span class="fa fa-user"></span>
                        ورود
                    </a>
                </li>
                <li>
                    <a href="{{ Route('home.Register')}}">
                        <span class="fa fa-lock"></span>
                        ثبت نام
                    </a>
                </li>

                <li>
						<span style="cursor:pointer" onclick="openNav()"><i class="fa fa-search"></i>
							جستجو
						</span>
                </li>
                <!-- search -->
                <div id="myNav" class="overlayfull">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <div class="overlayfull-content">
                        <input class="overlayfull-search" type="text" placeholder="کارت مورد نظر را جستجو کنید ...">
                    </div>
                </div>

                {{--<li class="date">--}}
                    {{--<a href="{{ Route('home')}}/Buy_basket" onclick="openNav()">--}}
                        {{--<span class="fa fa-shopping-basket "></span>--}}
                        {{--سبد خرید--}}
                    {{--</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
    <div class="middle_nav index">
        <nav>
            <div class="wrapper menu">
                <a href="javascript:void(0);" class="res_menu">&#9776;</a>
                <ul>
                    <li> <a target="_blank" href="{{ Route('home')}}">صفحه اصلی <i class="fa fa-home"></i> </a> <span class="border"></span> </li>
                    <li> <a target="_blank"  href="{{ Route('home')}}/whatis">کادونت چیه؟ <i class="fa fa-gift"></i> </a> <span class="border"></span> </li>
                    <li> <a target="_blank"  href="{{ Route('home')}}/howtowork">چگونه کار میکند؟<i class="fa fa-question-circle"></i></a> <span class="border"></span> </li>
                    <li> <a target="_blank"  href="{{ Route('home')}}/services">خدمات ما<i class="fa fa-archive "></i></a> <span class="border"></span> </li>
                    <li> <a target="_blank"  href="{{ Route('home')}}/stepone">شروع خرید<i class="fa fa-shopping-bag"></i></a> <span class="border"></span> </li>
                    <li> <a target="_blank"  href="{{ Route('occasion_description')}}">محصولات ما<i class="fa fa-list"></i></a> <span class="border"></span> </li>
                    <li> <a target="_blank"  href="{{ Route('home')}}/about_us">درباره ما<i class="fa fa-bullhorn"></i></a> <span class="border"></span> </li>
                    <li> <a target="_blank"  href="{{ Route('home')}}/contact_us">تماس با ما<i class="fa fa-phone"></i></a> <span class="border"></span> </li>

                    <li class="nav-links logo">
                        <a href="#">
                            <img src="<?php echo route('home')?>/user/img/logo.png" alt="logo" />
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

</header>

<section class="bg-bottom-page-section">
    <span id="bg-bottom-page"></span>
    <div class="row bg-bottom-page">
        <div class="bg">
            <div class="bg-color">
                <div class="wrapper">

                </div>
            </div>

        </div>
    </div>
</section>


@yield('content')



<footer id="contactus" class="footer-page">
    <div class="row main-footer">
        <div class="wrapper">
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
                <h2>لینک های مفید</h2>
                <ul class="rpad">
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
                <h2>لینک های مفید</h2>
                <ul class="rpad">
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
                <h2>لینک های مفید</h2>
                <ul class="rpad">
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 zpad">
                        <img src="<?php echo route('home')?>/img/logo.png" >
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 zpad">
                        <img src="<?php echo route('home')?>/img/enamad.png" >
                    </div>
                </div>

                <div class="row footer-icons center">
                    <ul class="ul-inline">
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook-square" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-telegram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row center bottom-footer">
        <p>تمامی حقوق متعلق به سایت میباشد</p>
    </div>
</footer>


<!-- JS files -->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<!-- Uses the built in easing capabilities added In jQuery 1.1 to offer multiple easing options -->
<script type="text/javascript" src="<?php echo route('home')?>/user/lib/mighty/assets/js/jquery.easing-1.3.pack.js"></script>
<!-- Mobile touch events for jQuery -->
<script type="text/javascript" src="<?php echo route('home')?>/user/lib/mighty/assets/js/jquery.mobile.just-touch.js"></script>

<script type="text/javascript" src="<?php echo route('home')?>/user/lib/mighty/src/js/tweenlite.js"></script>
<!-- Main slider JS script file -->
<script type="text/javascript" src="<?php echo route('home')?>/user/lib/mighty/src/js/mightyslider.min.js"></script>
<script type="text/javascript" src="<?php echo route('home')?>/user/lib/wow/dist/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/js/ion.rangeSlider.js"></script>

<script src="<?php echo route('home')?>/user/js/main.js"></script>
</body>
</html>