<!DOCTYPE html>
<html>
<head>
	<title>shop</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="fonts/iransans/css/fontiran.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="lib/mighty/src/css/mightyslider.css"/>
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="homePage">
	<header class="topHeader">

		<div class="nav_top">
			<div class="wrapper">
				<ul>
					<li>
						<a href="#">
							<span class="fa fa-user"></span>
							ورود
						</a>
					</li>
					<li>
						<a href="#">
							<span class="fa fa-lock"></span>
							ثبت نام
						</a>
					</li>

					<li>
						<span style="cursor:pointer" onclick="openNav()"><i class="fa fa-search"></i>
							جستجو
						</span>
					</li>
					<!-- search -->
					<div id="myNav" class="overlayfull">
					  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
					  <div class="overlayfull-content">
					  	<input class="overlayfull-search" type="text" placeholder="کارت مورد نظر را جستجو کنید ...">
					  </div>
					</div>

					<li class="date">
						<a href="#" onclick="openNav()">
							<span class="fa fa-shopping-basket "></span>
							سبد خرید
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="middle_nav index">
			<nav>
				<div class="wrapper menu">
					<a href="javascript:void(0);" class="res_menu">&#9776;</a>
					<ul>
				        <li> <a href="#home">صفحه اصلی <i class="fa fa-home"></i> </a> <span class="border"></span> </li>
				        <li> <a href="#whatis">کادوشاپ چیه؟ <i class="fa fa-gift"></i> </a> <span class="border"></span> </li>
				        <li> <a href="#HowToWork">چگونه کار میکند؟<i class="fa fa-question-circle"></i></a> <span class="border"></span> </li>
				        <li> <a href="#services">خدمات ما<i class="fa fa-archive "></i></a> <span class="border"></span> </li>
				        <li> <a href="#selecthearts">شروع خرید<i class="fa fa-shopping-bag"></i></a> <span class="border"></span> </li>
				        <li> <a href="#products">محصولات ما<i class="fa fa-list"></i></a> <span class="border"></span> </li>
				        <li> <a href="#aboutus">درباره ما<i class="fa fa-bullhorn"></i></a> <span class="border"></span> </li>
				        <li> <a href="#contactus">تماس با ما<i class="fa fa-phone"></i></a> <span class="border"></span> </li>

				        <li class="nav-links logo"> 
				        	<a href="#">
				        		<img src="img/logo.png" alt="logo" />
				        	</a>
				        </li>
			    	</ul>
				</div>
			</nav>

			<div class="breadcrump_holder">
				<a class="breadcrump-arrow-right" href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله قبل</a>
				<a class="breadcrump-arrow-left" href="#">مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
				
		        <ul class="breadcrumb">
		            <li class="active">
		            	<i class="fa fa-search" aria-hidden="true"></i>
		            	<a href="#">انتخاب مناسبت</a>
		            </li>
		            <li class="active">
					  	<i class="fa fa-credit-card" aria-hidden="true"></i>
					  	<a href="#">انتخاب کارت هدیه</a>
				  	</li>
				  	<li class="actived">
					  	<i class="fa fa-picture-o" aria-hidden="true"></i>
					  	سفارشی سازی
				  	</li>
			  		<li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
		        </ul>
		    </div>

		</div>
	</header>

	<div class="content customizetwo">
		<section class="row wrapper mighty-slider-section ">

		<div class="col-md-3 col-sm-4 col-xs-12 col-xxs-12 zpad">
			<div class="panel">
				<div class="panel-body">
					<p>تبلیغات</p>
				</div>
			</div>

			<div class="panel">
				<div class="panel-body">
					<p>تبلیغات</p>
				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<p>تبلیغات</p>
				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<p>تبلیغات</p>
				</div>
			</div>

		</div>

		<div class="col-md-9 col-sm-8 col-xs-12 col-xxs-12 zpad">
			<div class="panel">
				<div class="panel-header">
					<h2>سفارشی سازی</h2>
				</div>
				<div class="panel-body">
					<div class="img-holder row">
						<img class="img-responsive" src="img/kart-2.png">
					</div>

					<div class="row under-line">
						<div class="col-md-4 col-sm-4 col-xs-12 col-xxs-12 zpad">
							<label class="label-form">متن دلخواه</label>
							<input class="input-form wide" type="text" name="" />
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 col-xxs-12 zpad upload-img2">
							<span class="inline">
								<input type="radio" name="select-customize-image">
								<label class="label-form">تصویر پیشفرض</label>
							</span>

							<span class="inline">
								<input type="radio" name="select-customize-image">
								<label class="label-form">آپلود تصویر دلخواه</label>
							</span>
						
							<input class="input-form wide" type="file" name="">
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 col-xxs-12 zpad">
							<label class="label-form wide">تعداد کارت</label>
							<input class="input-form input-num" type="text" name="" />
							<span class="light-green">تخفیف 1000 تومان</span>
						</div>
					</div>

					<div class="row under-line">
						<p class="zpad">نحوه ارسال</p>

						<div class="kadr-blue-white row">
							<i class="fa fa-edge icon-kadr" aria-hidden="true"></i>
							<span class="inline post-text-kadr">
								<input type="radio" name="select-customize-image" class="tablinks" onclick="box1(event, 'tab1')">
								<label class=""> ارسال به ایمیل </label>
								
							</span>
							<p class="description-span">
								( در این صورت میتوانید به صورت الکترونیکی تصویر کارت را در ایمیل خود دریافت و سپس به دوست خود از طریق ایمیل، تلگرام و ... بدهید و یا از طرف ما به دوست تان ایمیل شود )
							</p>
						</div>

						<div class="kadr-blue-white row">
							<i class="fa fa-truck icon-kadr" aria-hidden="true"></i>
							<span class="inline post-text-kadr">
									<input  type="radio" name="select-customize-image" class="tablinks" onclick="box1(event, 'tab2')">
								<label class="">باربری <span> ( زمان ارسال یک تا دو روزه) </span></label>
							</span>
							<span class="price-post-kadr">هزینه ارسال <span>۸,۰۰۰</span> تومان </span>
						</div>

						<div class="kadr-blue-white row">
							<i class="fa fa-plane icon-kadr" aria-hidden="true"></i>
							<span class="inline post-text-kadr">
								<input  type="radio" name="select-customize-image" class="tablinks" onclick="box1(event, 'tab3')">
								<label class="">پست پیشتاز <span> ( زمان ارسال یک تا دو روزه) </span></label>
							</span>
							<span class="price-post-kadr">هزینه ارسال <span>۸,۰۰۰</span> تومان </span>
						</div>
					</div>

					<div class="row">
						<div id="tab1" class="tabcontent clearfix">
							<div class="col-md-3 col-sm-6 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">آدرس ایمیل فرستنده کادو</label>
	                    		<input class="input-form wide" type="email" name="">
	                    	</div>

	                    	<div class="col-md-3 col-sm-6 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">آدرس ایمیل گیرنده کادو</label>
	                    		<input class="input-form wide" type="email" name="">
	                    	</div>

	                    	<div class="col-md-3 col-sm-6 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">زمان ارسال ایمیل</label>
	                    		<input class="input-form wide" type="email" name="">
	                    	</div>

	                    	<div class="col-md-3 col-sm-6 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">موضوع ایمیل</label>
	                    		<input class="input-form wide" type="email" name="">
	                    	</div>

	                    	<div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">متن ایمیل</label>
	                    		<textarea class="textarea-form wide" rows="8"></textarea>
	                    	</div>
						</div>

	                    <div id="tab2" class="tabcontent clearfix">
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">استان</label>
	                    		<select class="input-form wide">
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    		</select>
	                    	</div>
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">شهر</label>
	                    		<select class="input-form wide">
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    		</select>
	                    	</div>
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">منطقه</label>
	                    		<select class="input-form wide">
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    		</select>
	                    	</div>

	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">شماره تلفن همراه گیرنده</label>
	                    		<input class="input-form wide" type="text" name="">
	                    	</div>
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">شماره ثابت گیرنده</label>
	                    		<input class="input-form wide" type="text" name="">
	                    	</div>
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">شماره تلفن همراه فرستنده</label>
	                    		<input class="input-form wide" type="text" name="">
	                    	</div>

	                    	<div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">آدرس گیرنده</label>
	                    		<textarea class="textarea-form wide" rows="6"></textarea>
	                    	</div>

	                    	

	                    	<div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">توضیح اضافی در مورد ارسال در صورت نیاز</label>
	                    		<textarea class="textarea-form wide" rows="6"></textarea>
	                    	</div>
	                    </div>

	                    <div id="tab3" class="tabcontent clearfix">
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">استان</label>
	                    		<select class="input-form wide">
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    		</select>
	                    	</div>
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">شهر</label>
	                    		<select class="input-form wide">
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    		</select>
	                    	</div>
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">منطقه</label>
	                    		<select class="input-form wide">
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    			<option>انتخاب</option>
	                    		</select>
	                    	</div>

	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">شماره تلفن همراه گیرنده</label>
	                    		<input class="input-form wide" type="text" name="">
	                    	</div>
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">شماره ثابت گیرنده</label>
	                    		<input class="input-form wide" type="text" name="">
	                    	</div>
	                    	<div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
	                    		<label class="label-form">شماره تلفن همراه فرستنده</label>
	                    		<input class="input-form wide" type="text" name="">
	                    	</div>

	                    	<div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">آدرس گیرنده</label>
	                    		<textarea class="textarea-form wide" rows="6"></textarea>
	                    	</div>

	                    	

	                    	<div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
	                    		<label class="label-form">توضیح اضافی در مورد ارسال در صورت نیاز</label>
	                    		<textarea class="textarea-form wide" rows="6"></textarea>
	                    	</div>
	                    </div>
					</div>

				</div>
			</div>
		</div>
			
			
		</section>
	</div>


	<!-- footer -->
	<section class="bg-bottom-page-section">
	<span id="bg-bottom-page"></span>
		<div class="row bg-bottom-page">
			<div class="bg">
				<div class="bg-color">
					<div class="wrapper">

					</div>
				</div>
				
			</div>
		</div>
	</section>

	<footer id="contactus" class="footer-page">

		<div class="row main-footer">
			<div class="wrapper">
				<div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
					<h2>لینک های مفید</h2>
					<ul class="rpad">
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
					<h2>لینک های مفید</h2>
					<ul class="rpad">
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
					<h2>لینک های مفید</h2>
					<ul class="rpad">
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 zpad">
							<img src="img/logo.png" >
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 zpad">
							<img src="img/enamad.png" >
						</div>
					</div>
					
					<div class="row footer-icons center">
						<ul class="ul-inline">
							<li>
								<a href="#">
									<i class="fa fa-facebook-square" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-google-plus-square" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-telegram" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-instagram" aria-hidden="true"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="row center bottom-footer">
			<p>تمامی حقوق متعلق به سایت میباشد</p>
		</div>
	</footer>

	<!-- JS files -->
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

		<!-- Uses the built in easing capabilities added In jQuery 1.1 to offer multiple easing options -->
	    <script type="text/javascript" src="lib/mighty/assets/js/jquery.easing-1.3.pack.js"></script>
	    <!-- Mobile touch events for jQuery -->
	    <script type="text/javascript" src="lib/mighty/assets/js/jquery.mobile.just-touch.js"></script>

	    <script type="text/javascript" src="lib/mighty/src/js/tweenlite.js"></script>
	    <!-- Main slider JS script file -->
	    <script type="text/javascript" src="lib/mighty/src/js/mightyslider.min.js"></script>

		<script src="js/main.js"></script>
</body>
</html>