@extends('leyouts.user')
@section('content')
	<div class="content customize">
		<section class="row wrapper mighty-slider-section ">

			<!-- PARENT -->
			<div id="example" class="mightyslider_modern_skin black ltr-dir">
				<!-- FRAME -->
				<div class="frame" data-mightyslider="
			        width: 1585,
			        height: 500
			    ">
					<!-- SLIDEELEMENT -->
					<div class="slide_element">

						<!-- SLIDES -->
						<div class="slide prev_2" data-mightyslider="
			                cover: 'user/img/11.jpg',
			                thumbnail: 'user/img/11.jpg'
			            "></div>
						<div class="slide prev_1" data-mightyslider="
			                cover: 'user/img/11.jpg',
			                thumbnail: 'user/img/11.jpg'
			            "></div>
						<div class="slide active" data-mightyslider="
			                cover: 'user/img/11.jpg',
			                thumbnail: 'user/img/11.jpg'
			            ">
							<!-- LAYER -->
							<div class="mSCaption">
								You can use direct video url<br />for full-sized videos & covers
							</div>
						</div>
						<div class="slide next_1" data-mightyslider="
			                cover: 'user/img/11.jpg',
			                thumbnail: 'user/img/11.jpg'
			            "></div>
						<div class="slide next_2" data-mightyslider="
			                cover: 'user/img/11.jpg',
			                thumbnail: 'user/img/11.jpg'
			            "></div>
						<div class="slide" data-mightyslider="
			                cover: 'user/img/11.jpg',
			                thumbnail: 'user/img/11.jpg'
			            "></div>
						<!-- END OF SLIDES -->
					</div>
					<!-- END OF SLIDEELEMENT -->
					<!-- ARROW BUTTONS -->
					<a class="mSButtons mSPrev"></a>
					<a class="mSButtons mSNext"></a>
				</div>
				<!-- END OF FRAME -->
				<!-- SLIDER TIMER -->
				<canvas id="progress" width="160" height="160"></canvas>
				<!-- END OF SLIDER TIMER -->
				<!-- THUMBNAILS -->
				<div id="thumbnails">
					<div>
						<ul></ul>
					</div>
				</div>
				<!-- END OF THUMBNAILS -->
			</div>
			<!-- END OF PARENT -->

			<p class=" row center bottom-btn-holder">
				<a href="#" class="btn-2">همه موارد و جزئیات بیشتر</a>
			</p>
		</section>
	</div>

@endsection
