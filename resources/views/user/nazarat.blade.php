<!DOCTYPE html>
<html>
<head>
	<title>shop</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="fonts/iransans/css/fontiran.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="lib/mighty/src/css/mightyslider.css"/>
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="homePage">
	<header class="topHeader">

		<div class="nav_top">
			<div class="wrapper">
				<ul>
					<li>
						<a href="#">
							<span class="fa fa-user"></span>
							ورود
						</a>
					</li>
					<li>
						<a href="#">
							<span class="fa fa-lock"></span>
							ثبت نام
						</a>
					</li>

					<li>
						<span style="cursor:pointer" onclick="openNav()"><i class="fa fa-search"></i>
							جستجو
						</span>
					</li>
					<!-- search -->
					<div id="myNav" class="overlayfull">
					  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
					  <div class="overlayfull-content">
					  	<input class="overlayfull-search" type="text" placeholder="کارت مورد نظر را جستجو کنید ...">
					  </div>
					</div>

					<li class="date">
						<a href="#" onclick="openNav()">
							<span class="fa fa-shopping-basket "></span>
							سبد خرید
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="middle_nav index">
			<nav>
				<div class="wrapper menu">
					<a href="javascript:void(0);" class="res_menu">&#9776;</a>
					<ul>
				        <li> <a href="#home">صفحه اصلی <i class="fa fa-home"></i> </a> <span class="border"></span> </li>
				        <li> <a href="#whatis">کادوشاپ چیه؟ <i class="fa fa-gift"></i> </a> <span class="border"></span> </li>
				        <li> <a href="#HowToWork">چگونه کار میکند؟<i class="fa fa-question-circle"></i></a> <span class="border"></span> </li>
				        <li> <a href="#services">خدمات ما<i class="fa fa-archive "></i></a> <span class="border"></span> </li>
				        <li> <a href="#selecthearts">شروع خرید<i class="fa fa-shopping-bag"></i></a> <span class="border"></span> </li>
				        <li> <a href="#products">محصولات ما<i class="fa fa-list"></i></a> <span class="border"></span> </li>
				        <li> <a href="#aboutus">درباره ما<i class="fa fa-bullhorn"></i></a> <span class="border"></span> </li>
				        <li> <a href="#contactus">تماس با ما<i class="fa fa-phone"></i></a> <span class="border"></span> </li>

				        <li class="nav-links logo"> 
				        	<a href="#">
				        		<img src="img/logo.png" alt="logo" />
				        	</a>
				        </li>
			    	</ul>
				</div>
			</nav>

			<div class="breadcrump_holder">
				<a class="breadcrump-arrow-right" href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله قبل</a>
				<a class="breadcrump-arrow-left" href="#">مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
				
		        <ul class="breadcrumb">
		            <li class="active">
		            	<i class="fa fa-search" aria-hidden="true"></i>
		            	<a href="#">انتخاب مناسبت</a>
		            </li>
		            <li class="actived">
					  	<i class="fa fa-credit-card" aria-hidden="true"></i>
					  	انتخاب کارت هدیه (جزئیات و نظرات)
				  	</li>
				  	<li class="">
					  	<i class="fa fa-picture-o" aria-hidden="true"></i>
					  	سفارشی سازی
				  	</li>
			  		<li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
		        </ul>
		    </div>

		</div>
	</header>

	<div class="content">
		<section class="row wrapper">
			<div class="panel">
				<div class="panel-header">
					<h2>جعبه کادو صورتی گلدار</h2>
				</div>
				<!-- panel-header -->
				<div class="panel-body">
					<div class="kado_photo">
						<div class="row">

							<div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 zpad center">
								<div class="kado_imgiv">
									<img class="img-responsive" src="img/kado.jpg" alt="phot" />
									<img class="img-responsive" src="img/kado.jpg" alt="phot" />
									<img class="img-responsive" src="img/kado.jpg" alt="phot" />
								</div><!-- kado_imgiv -->
							</div><!-- kado_img -->
							<div class="col-md-9 colsm-8 col-xs-6 col-xxs-12 right content">
								<h2>
									<i class="fa fa-dot-circle-o" aria-hidden="true"></i>
									جعبه کادو صورتی گلدار
								</h2>
								<p>
									لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
									<br />
									<a href="#">لینک</a>
									<br />
									لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
								</p>
							</div><!-- content -->

						</div><!-- kado_photoiv -->
					</div><!-- kado_photo -->

					<div class="diviv"></div>
					<div class="row">
						
						<div class="comment">
							<div class="commentiv">
								<button class="comment_fav">
									<span>0</span>
									<span class="fa fa-heart-o"></span>
								</button>
								<button class="email_to_frend">
									<span>ایمیل به یک دوست</span>
									<span class="fa fa-envelope-o"></span>
								</button>
								<button class="to_fasebook">
									<span>همرسانی در فیسبوک</span>
									<span class="fa fa-facebook"></span>
								</button>
							</div>
							<!-- commentiv -->

							<div class="add_comment">
								<h3>نظر خودتان را بنویسید</h3>
								<input class="input-form" type="text" name=""><br />
								<div class="paper">
								    <div class="paper-content">
								        <textarea placeholder="متن نظر یا بررسی خود را بنویسد"></textarea>
								    </div><!-- paper-content -->
								</div><!-- paper -->
								<p>
									<span class="fa fa-star-o"></span>
									<span class="fa fa-star-o"></span>
									<span class="fa fa-star-o"></span>
									<span class="fa fa-star-o"></span>
									<span class="fa fa-star-o"></span>
								</p>
								<div class="send_comment left">
									<button class="btn_comment_send">
										<span>ارسال نظر</span>
										<span class="fa fa-paper-plane-o"></span>
									</button>
								</div>
							</div><!-- add_comment -->
							<div class="diviv"></div><!-- diviv -->
							<div class="comment_list">
								<h3>لیست نظرات</h3>
								<div class="comment_list_user">

									<div class="comment_user">
										<h4>
											<span class="img_user"><img src="img/no_profile.jpg"></span>
											
											<div class="user_name">
												<span>علی عسکری خواه</span><br />
												<span class="fa fa-star-o"></span>
												<span class="fa fa-star-o"></span>
												<span class="fa fa-star-o"></span>
												<span class="fa fa-star-o"></span>
												<span class="fa fa-star-o"></span>
											</div>
										</h4>
										<div class="comment_user">
											لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
										</div><!-- comment_user -->
									</div>

									<div class="comment_user">
										<h4>
											<span class="img_user"><img src="img/no_profile.jpg"></span>
											
											<div class="user_name">
												<span>علی عسکری خواه</span><br />
												<span class="fa fa-star-o"></span>
												<span class="fa fa-star-o"></span>
												<span class="fa fa-star-o"></span>
												<span class="fa fa-star-o"></span>
												<span class="fa fa-star-o"></span>
											</div>
										</h4>
										<div class="comment_user">
											لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
										</div><!-- comment_user -->
									</div>

								</div><!-- comment_list_user -->
							</div><!-- comment_list -->

						</div><!-- comment -->

					</div><!-- row -->


				</div><!-- panel-body -->
			</div><!-- panel -->
		</section>
	</div>

	<!-- footer -->
	<section class="bg-bottom-page-section">
	<span id="bg-bottom-page"></span>
		<div class="row bg-bottom-page">
			<div class="bg">
				<div class="bg-color">
					<div class="wrapper">

					</div>
				</div>
				
			</div>
		</div>
	</section>

	<footer id="contactus" class="footer-page">

		<div class="row main-footer">
			<div class="wrapper">
				<div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
					<h2>لینک های مفید</h2>
					<ul class="rpad">
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
					<h2>لینک های مفید</h2>
					<ul class="rpad">
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
					<h2>لینک های مفید</h2>
					<ul class="rpad">
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
						<li>
							<a href="#">لینک های مفید</a>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 zpad">
							<img src="img/logo.png" >
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 zpad">
							<img src="img/enamad.png" >
						</div>
					</div>
					
					<div class="row footer-icons center">
						<ul class="ul-inline">
							<li>
								<a href="#">
									<i class="fa fa-facebook-square" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-google-plus-square" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-telegram" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa fa-instagram" aria-hidden="true"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="row center bottom-footer">
			<p>تمامی حقوق متعلق به سایت میباشد</p>
		</div>
	</footer>

	<!-- JS files -->
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

		<!-- Uses the built in easing capabilities added In jQuery 1.1 to offer multiple easing options -->
	    <script type="text/javascript" src="lib/mighty/assets/js/jquery.easing-1.3.pack.js"></script>
	    <!-- Mobile touch events for jQuery -->
	    <script type="text/javascript" src="lib/mighty/assets/js/jquery.mobile.just-touch.js"></script>

	    <script type="text/javascript" src="lib/mighty/src/js/tweenlite.js"></script>
	    <!-- Main slider JS script file -->
	    <script type="text/javascript" src="lib/mighty/src/js/mightyslider.min.js"></script>

		<script src="js/main.js"></script>
</body>
</html>