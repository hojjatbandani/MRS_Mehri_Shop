@extends('leyouts.web')
@section('content')
    <div class="content">
        <div class="row googleMapDiv">
            <div class="wrapper">
                <div class="col-md-6 col-sm-6 col-xs-8 col-xxs-12 contactus zpad">
                    <div class="panel center">
                        <div class="panel-body">
                            <div class="contactus-header">
                                <h2>تماس با ما</h2>
                                <p>
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم
                                </p>
                            </div>
                            <div class="rpad contactus-main">
                                <address>
                                    تهران، بزرگراه شهید بابایی، روبروی دانشگاه امام حسین (ع)، خروجی حکیمیه، خیابان شهید صدوقی
                                </address>

                                <div class="row contact-main">
                                    <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-4 zpad">
                                        <p>ایمیل</p>
                                        <p>info@site.com</p>
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-4 zpad">
                                        <p>تلفن</p>
                                        <p>021-888888</p>
                                    </div>

                                    <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-4 zpad">
                                        <p>فکس</p>
                                        <p>021-888888</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="map" style="width:100%;height:400px;"></div>
        </div>
    </div>

@endsection
