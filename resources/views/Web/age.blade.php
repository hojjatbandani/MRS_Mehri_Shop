@extends('leyouts.web')
@section('content')

    <div class="breadcrump_holder">
        <a class="breadcrump-arrow-right" href='{{route('select_products_season')}}'><i class="fa fa-angle-right" aria-hidden="true"></i>مرحله
            قبل</a>
        <a class="breadcrump-arrow-left" href='{{route('select_products')}}'>مرحله بعد<i class="fa fa-angle-left" aria-hidden="true"></i></a>

        <ul class="breadcrumb">
            <li class="actived">
                <i class="fa fa-search" aria-hidden="true"></i>
                <a>انتخاب مناسبت(گروه سنی)</a>
            </li>
            <li>
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                <a>انتخاب کارت هدیه</a>
            </li>
            <li>
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                سفارشی سازی
            </li>
            <li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
        </ul>
    </div>

    <div class="content agecontent">
        <section class="row wrapper	gift-card">
            <div class="panel">
                <div class="panel-header">
                    <h2>انتخاب گروه سنی </h2>
                </div>
                <div class="panel-body">

                    <div class="row gift-card-grid">
                        <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12  zpad grid-Equal">
                            <div class="m-img-age"></div>
                            <div class="overlay">
                                <div class="text">
                                    <p>گروه سنی آقایان</p>
                                    <ul>
                                        <?php
                                        if (count($age) > 0){

                                        foreach($age as $row){
                                        $title = $row->YearGroup_title;
                                        $gender = 'man';
                                        ?>
                                        <li>
                                            <a href="{{route('home.select_age',$title)}}/{{$gender}}"><?php echo $title;?></a>
                                        </li>

                                        <?php }
                                        }else {
                                        }?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12  zpad grid-Equal">
                            <div class="w-img-age"></div>
                            <div class="overlay">
                                <div class="text">
                                    <p>گروه سنی خانم ها</p>
                                    <ul>
                                        <?php
                                        if (count($age) > 0){

                                        foreach($age as $row){
                                        $title = $row->YearGroup_title;
                                        $gender = 'woman';
                                        ?>
                                        <li>
                                            <a href="{{route('home.select_age',$title)}}/{{$gender}}"><?php echo $title;?></a>
                                        </li>

                                        <?php }
                                        }else {
                                        }?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>
    </div>
    <section class="bg-bottom-page-section">
        <span id="bg-bottom-page"></span>
        <div class="row bg-bottom-page">
            <div class="bg">
                <div class="bg-color">
                    <div class="wrapper">

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
