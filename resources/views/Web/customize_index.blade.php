@extends('leyouts.web')
@section('content')


<div class="content customize">

	<div class="top-header">
		<a href="<?php echo Route('stepone')?>">
			<h2>برای شروع خرید اینجا کلیک کنید</h2>
		</a>
	</div>

	<section class="row wrapper mighty-slider-section ">

		<div class="description-kadr">
			<div class="kadr-blue_white">
				<p>
					لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.

				</p>
			</div>
		</div>

		<!-- PARENT -->
		<div id="example" class="mightyslider_modern_skin black ltr-dir">
			<!-- FRAME -->
			<div class="frame" data-mightyslider="
			        width: 1585,
			        height: 500
			    ">
				<!-- SLIDEELEMENT -->
				<div class="slide_element">

					<!-- SLIDES -->
					<div class="slide prev_2" data-mightyslider="
			                cover: '<?php echo route('home')?>/user/img/11.jpg',
			                thumbnail: '<?php echo route('home')?>/user/img/11.jpg'
			            "></div>
					<div class="slide prev_1" data-mightyslider="
			                cover: '<?php echo route('home')?>/user/img/11.jpg',
			                thumbnail: '<?php echo route('home')?>/user/img/11.jpg'
			            "></div>
					<div class="slide active" data-mightyslider="
			                cover: '<?php echo route('home')?>/user/img/11.jpg',
			                thumbnail: '<?php echo route('home')?>/user/img/11.jpg'
			            ">
						<!-- LAYER -->
						<div class="mSCaption">
							You can use direct video url<br />for full-sized videos & covers
						</div>
					</div>
					<div class="slide next_1" data-mightyslider="
			                cover: '<?php echo route('home')?>/user/img/11.jpg',
			                thumbnail: '<?php echo route('home')?>/user/img/11.jpg'
			            "></div>
					<div class="slide next_2" data-mightyslider="
			                cover: '<?php echo route('home')?>/user/img/11.jpg',
			                thumbnail: '<?php echo route('home')?>/user/img/11.jpg'
			            "></div>
					<div class="slide" data-mightyslider="
			                cover: '<?php echo route('home')?>/user/img/11.jpg',
			                thumbnail: '<?php echo route('home')?>/user/img/11.jpg'
			            "></div>
					<!-- END OF SLIDES -->
				</div>
				<!-- END OF SLIDEELEMENT -->
				<!-- ARROW BUTTONS -->
				<a class="mSButtons mSPrev"></a>
				<a class="mSButtons mSNext"></a>
			</div>
			<!-- END OF FRAME -->
			<!-- SLIDER TIMER -->
			<canvas id="progress" width="160" height="160"></canvas>
			<!-- END OF SLIDER TIMER -->
			<!-- THUMBNAILS -->
			<div id="thumbnails">
				<div>
					<ul></ul>
				</div>
			</div>
			<!-- END OF THUMBNAILS -->
		</div>
		<!-- END OF PARENT -->

		<p class=" row center bottom-btn-holder">
			<a href="#" class="btn-2">همه موارد و جزئیات بیشتر</a>
		</p>
	</section>
</div>


<!-- footer -->
<section class="bg-bottom-page-section">
	<span id="bg-bottom-page"></span>
	<div class="row bg-bottom-page">
		<div class="bg">
			<div class="bg-color">
				<div class="wrapper">

				</div>
			</div>

		</div>
	</div>
</section>

@endsection
