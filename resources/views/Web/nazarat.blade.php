@extends('leyouts.web')
@section('content')
    <div class="breadcrump_holder">
        <a class="breadcrump-arrow-right" href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله قبل</a>
        <a class="breadcrump-arrow-left" href="#">مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>

        <ul class="breadcrumb">
            <li class="active">
                <i class="fa fa-search" aria-hidden="true"></i>
                <a href="#">انتخاب مناسبت</a>
            </li>
            <li class="actived">
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                انتخاب کارت هدیه (جزئیات و نظرات)
            </li>
            <li class="">
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                سفارشی سازی
            </li>
            <li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
        </ul>
    </div>

    </div>
    </header>

    <div class="content">
        <section class="row wrapper">
            <div class="panel">
                <div class="panel-header">
                    <h2>جعبه کادو صورتی گلدار</h2>
                </div>
                <!-- panel-header -->
                <div class="panel-body">
                    <div class="kado_photo">
                        <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 lfloat zpad center">
                                <div class="kado_imgiv">
                                    <img class="img-responsive" src="<?php echo route('home')?>/uploads/category/<?php echo $res['image']?>"  />
                                </div><!-- kado_imgiv -->
                            </div><!-- kado_img -->
                            <div class="col-md-9 col-sm-8 col-xs-6 col-xxs-12 lfloat right content">
                                <h2>
                                    <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                    جعبه کادو صورتی گلدار
                                </h2>

                            </div><!-- content -->
                        </div><!-- kado_photoiv -->
                    </div><!-- kado_photo -->
                    <div class="diviv"></div>
                    <div class="row">
                        <div class="comment">
                            <div class="commentiv row">
                                <a href="#" class="btn-1">
                                    <span>0</span>
                                    <span class="fa fa-heart-o"></span>
                                </a>
                                <a href="#" class="btn-1">
                                    <span>ایمیل به یک دوست</span>
                                    <span class="fa fa-envelope-o"></span>
                                </a>
                                <a href="#" class="btn-1">
                                    <span>همرسانی در فیسبوک</span>
                                    <span class="fa fa-facebook"></span>
                                </a>
                            </div>
                            <!-- commentiv -->
                            <div class="add_comment">
                                <h3>نظر خودتان را بنویسید</h3>
                                <input class="input-form" type="text" name=""><br />
                                <div class="paper">
                                    <div class="paper-content">
                                        <textarea placeholder="متن نظر یا بررسی خود را بنویسد"></textarea>
                                    </div><!-- paper-content -->
                                </div><!-- paper -->
                                <p>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>
                                </p>
                                <div class="send_comment left">
                                    <a href="#" class="btn-2">
                                        <span>ارسال نظر</span>
                                        <span class="fa fa-paper-plane-o"></span>
                                    </a>
                                </div>
                            </div><!-- add_comment -->
                            <div class="diviv"></div><!-- diviv -->
                            <div class="comment_list">
                                <h3>لیست نظرات</h3>
                                <div class="comment_list_user">
                                    <div class="comment_user">
                                        <h4>
                                            <span class="img_user"><img src="img/no_profile.jpg"></span>
                                            <div class="user_name">
                                                <span>علی عسکری خواه</span><br />
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                            </div>
                                        </h4>
                                        <div class="comment_user">
                                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                                        </div><!-- comment_user -->
                                    </div>
                                    <div class="comment_user">
                                        <h4>
                                            <span class="img_user"><img src="img/no_profile.jpg"></span>
                                            <div class="user_name">
                                                <span>علی عسکری خواه</span><br />
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                            </div>
                                        </h4>
                                        <div class="comment_user">
                                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                                        </div><!-- comment_user -->
                                    </div>
                                </div><!-- comment_list_user -->
                            </div><!-- comment_list -->
                        </div><!-- comment -->
                    </div><!-- row -->
                </div><!-- panel-body -->
            </div><!-- panel -->
        </section>
    </div>
@endsection