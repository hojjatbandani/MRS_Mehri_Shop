@extends('leyouts.web')
@section('content')

	<div class="breadcrump_holder">
		<a class="breadcrump-arrow-right" href='{{route('select_products')}}'><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله
			قبل</a>
		<a class="breadcrump-arrow-left" href={{route('Customize_2')}}>مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>

		<ul class="breadcrumb">
			<li class="active">
				<i class="fa fa-search" aria-hidden="true"></i>
				<a href="{{route('select_products')}}">انتخاب مناسبت</a>
			</li>
			<li class="active">
				<i class="fa fa-credit-card" aria-hidden="true"></i>
				<a href='{{route('custom')}}'>انتخاب کارت هدیه</a>
			</li>
			<li class="actived">
				<i class="fa fa-picture-o" aria-hidden="true"></i>
				سفارشی سازی
			</li>
			<li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
		</ul>
	</div>

	</div>
	<div class="content customize">
		<div class="row wrapper products wow zoomIn">
			<h2 class="center">محصولات</h2>

			<!-- PARENT -->
			<div id="example" class="mightyslider_modern_skin black ltr-dir">
				<!-- FRAME -->
				<div class="frame" data-mightyslider="
				        width: 1250,
				        height: 350
				    ">
					<!-- SLIDEELEMENT -->
					<div class="slide_element">

						<!-- SLIDES -->
						<div class="slide prev_2" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01m.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01m.png',
				                link: {
				                    url: '<?php echo route('Customize_2');?>',

				                }
				            "></div>
						<div class="slide prev_1" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01e.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01e.png',
				                link: {
				                    url: '<?php echo route('Customize_2');?>',

				                }
				            "></div>
						<div class="slide active" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01t.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01t.png',
				                link: {
				                    url: '<?php echo route('Customize_2');?>',

				                }
				            ">
							<!-- LAYER -->
							<div class="mSCaption">
								You can use direct video url<br />for full-sized videos & covers
							</div>
						</div>
						<div class="slide next_1" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01n.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01n.png',
				                link: {
				                    url: '<?php echo route('Customize_2');?>',

				                }
				            "></div>
						<div class="slide next_2" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01v.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01v.png',
				                link: {
				                    url: '<?php echo route('Customize_2');?>',

				                }
				            "></div>
						<div class="slide" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01p.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01p.png',
				                link: {
				                    url: '<?php echo route('Customize_2');?>',

				                }
				            "></div>
						<!-- END OF SLIDES -->
					</div>
					<!-- END OF SLIDEELEMENT -->
					<!-- ARROW BUTTONS -->
					<a class="mSButtons mSPrev"></a>
					<a class="mSButtons mSNext"></a>
				</div>
				<!-- END OF FRAME -->
				<!-- SLIDER TIMER -->
				<canvas id="progress" width="160" height="160"></canvas>
				<!-- END OF SLIDER TIMER -->
				<!-- THUMBNAILS -->
				<div id="thumbnails">
					<div>
						<ul></ul>
					</div>
				</div>
				<!-- END OF THUMBNAILS -->
			</div>
			<!-- END OF PARENT -->


			<p class=" row center bottom-btn-holder" style="margin-top:15px;">
				<a href="<?php echo route('all_card')?>" class="btn-2">مشاهده همه طرح های کارت هدیه</a>
			</p>
		</div>	</div>

@endsection
