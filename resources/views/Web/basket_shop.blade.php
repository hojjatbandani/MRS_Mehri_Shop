@extends('leyouts.web')
@section('content')

	<div class="breadcrump_holder">
		<a class="breadcrump-arrow-right" href='{{route('Customize_2')}}'><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله قبل</a>

		<ul class="breadcrumb">
			<li class="active">
				<i class="fa fa-search" aria-hidden="true"></i>
				<a href="#">انتخاب مناسبت</a>
			</li>
			<li class="active">
				<i class="fa fa-credit-card" aria-hidden="true"></i>
				<a href="#">انتخاب کارت هدیه</a>
			</li>
			<li class="active">
				<i class="fa fa-picture-o" aria-hidden="true"></i>
				سفارشی سازی
			</li>
			<li class="actived"><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
		</ul>
	</div>

	<div class="content basketshopcontent">

		<section class="row wrapper mighty-slider-section ">

			<div class="panel">
				<div class="panel-header">
					<h2>سبد خرید</h2>
				</div>
				<div class="panel-body">
					<div class="row row-table zpad">
						<table class="table-style">
							<thead>
							<tr>
								<th>مشخصات کارت هدیه</th>
								<th>تعداد</th>
								<th>قیمت واحد</th>
								<th> قیمت کل</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									<img class="img-tumb" src="img/11.jpg">
									<span>کارت هدیه شنا</span>
								</td>
								<td>

									<select id="how_much" class="input-form wide" style="width:100%;">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</td>
								<td><span class="main_price">
										<?php

											echo $amount_card;

										?>
									</span>تومان</td>
								<td><span class="price_all">
										<?php

                                        	echo $amount_card;

                                        ?>
									</span>تومان</td>
							</tr>
							</tbody>
						</table>
					</div>

					<div class="row under-line">
						<div class="col-md-6 col-sm-6 zpad">
							<div class="off-shop">
								<input type="text" placeholder="کد تخفیف" id="code_off" class="input-form" />
								<a href="#" class="btn-2" id="off_maker">اعمال تخفیف</a>
							</div>
						</div>

						<div class="col-md-6 col-sm-6 zpad">
							<div class="sum-shop">
								<?php
									if($amount_card!=0){
										echo "<p  class='row'>جمع کل خرید شما <span class='lfloat price_all'><b>".$amount_card."</b>تومان</span></p>";
									}
									if($city_price!=0){
                                        echo "<p  class='row'>هزینه ارسال <span class='lfloat city_price'><b>".$city_price."</b>تومان</span></p>";
									}
									if($Kado_Price!=0){
                                        echo "<p  class='row'>هزینه کادو پیچی <span class='lfloat kado_price'><b>".$Kado_Price."</b>تومان</span></p>";
									}
                                ?>

								<p class="row">تخفیف<span class="lfloat takhfif_perecent"><b>0</b>درصد</span></p>
								<p class="row">مبلغ قابل پرداخت <span class="lfloat price_all_by_takhfif"><b>0</b>تومان</span> </p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="btn_basket">
							<a href={{route('Paid')}} class="btn-2">پرداخت و تحویل</a>
						</div><!-- into_basketiv -->
					</div>
				</div>
			</div>




		</section>

	</div>
@endsection
