<!DOCTYPE html>
<html>
<head>
    <title>shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="user/fonts/iransans/css/fontiran.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="user/css/animate.css">
    <link rel="stylesheet" type="text/css" href="user/css/style.css">
</head>
<body class="homePage">

<div class="content SignUpContent">
    <section class="row">
        <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 signUp-rightSide">

            <div class="signUp-side-box">
                <div class="login-logo">
                    <a href="<?php echo route('home')?>"></a>
                </div>
                <div class="signUp-side-text">
                    <p>سلام دوست عزیز :)</p>
                    <p>
                        ممنونم که سایت ما رو انتخاب کردید.
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-sm-8 col-xs-6 col-xxs-12 signUp-box-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-xs-12 signUp-box xpad">
                    <h2 class="signUpTitr">اطلاعات شخصی</h2>
                    @include('Web.partials.errors')
                    {{isset($error_check)?$error_check:''}}
                    <p id="message"></p>
                    <p class="signUp-subHeaderTitr">لطفا اطلاعات شخصی خودتون رو بصورت دقیق پر کنید </p>
                    <form class="login-form" action="{{ Route('home.Register') }}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                <label class="label-form wide">نام</label>
                                <input type="text" required="required" class="input-form wide" name="name"
                                       value="{{old('name')}}">
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                <label class="label-form wide">نام خانوادگی</label>
                                <input type="text" required="required" class="input-form wide" name="family"
                                       value="{{old('family')}}">
                            </div>
                        </div>
                        <div class="row xpad">
                            <label class="label-form wide">ایمیل</label>
                            <input type="email" required="required" class="input-form wide" name="email"
                                   value="{{old('email')}}">
                        </div>

                        <div class="row xpad">
                            <label class="label-form wide">موبایل</label>
                            <input type="number" required="required" class="input-form wide" name="phone_number"
                                   value="{{old('phone_number')}}" id="phone_id"/>
                        </div>

                        <div class="row xpad">
                            <label class="label-form wide">کلمه عبور</label>
                            <input type="password" required="required" class="input-form wide" name="password"
                                   id="password_id">
                        </div>
                        <div class="row xpad">
                            <label class="label-form wide">تکرار کلمه عبور</label>
                            <input type="password" required="required" class="input-form wide" name="password"
                                   id="password_re_id">
                        </div>

                        <div class="row xpad">
                            <div class="col-md-8 col-sm-8 col-xs-8 col-xxs-8 go-to-login">
                                از قبل حساب کاربری دارید؟
                                <a href="{{Route('home.Login')}}">ورود به حساب کاربری</a>
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-4">
                                <input class="btn-plus lfloat" type="submit" name="sub_reg" value="ثبت نام"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
</div>


<!-- JS files -->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<!-- Uses the built in easing capabilities added In jQuery 1.1 to offer multiple easing options -->
<script type="text/javascript" src="user/lib/mighty/assets/js/jquery.easing-1.3.pack.js"></script>
<!-- Mobile touch events for jQuery -->
<script type="text/javascript" src="user/lib/mighty/assets/js/jquery.mobile.just-touch.js"></script>

<script type="text/javascript" src="user/lib/mighty/src/js/tweenlite.js"></script>
<!-- Main slider JS script file -->
<script type="text/javascript" src="user/lib/mighty/src/js/mightyslider.min.js"></script>

<script src="user/js/main.js"></script>
</body>
</html>