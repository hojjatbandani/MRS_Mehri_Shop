@extends('leyouts.web')
@section('content')

    <div class="breadcrump_holder">
        <a class="breadcrump-arrow-right" href='{{route('home')}}'><i class="fa fa-angle-right" aria-hidden="true"></i>خانه
            </a>
        <a class="breadcrump-arrow-left" href='{{route('home.select_season')}}'>مرحله بعد<i class="fa fa-angle-left" aria-hidden="true"></i></a>

        <ul class="breadcrumb">
            <li class="actived">
                <i class="fa fa-search" aria-hidden="true"></i>
                <a>انتخاب مناسبت(ماه تولد)</a>
            </li>
            <li>
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                <a>انتخاب کارت هدیه</a>
            </li>
            <li>
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                سفارشی سازی
            </li>
            <li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
        </ul>
    </div>
    <div class="content">
        <section class="row wrapper	gift-card">
            <!-- <div class="panel">
                <div class="panel-header">
                    <h2>انتخاب ماه</h2>
                </div>
                <div class="panel-body"> -->

            <div class="row gift-card-grid">
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12  zpad se-grid-Equal">
                    <div class="se-img1"></div>
                    <div class="overlay">
                        <div class="text">
                            <p>بهار</p>
                            <ul>
                                <li>
                                    <a href="{{route('home.select_season',1)}}">فروردین</a>
                                </li>
                                <li>
                                    <a href="{{route('home.select_season',2)}}">اردیبهشت</a>
                                </li>
                                <li>
                                    <a href="{{route('home.select_season',3)}}">خرداد</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12  zpad se-grid-Equal">
                    <div class="se-img2"></div>
                    <div class="overlay">
                        <div class="text">
                            <p>تابستان</p>
                            <ul>
                                <li>
                                    <a href="{{route('home.select_season',4)}}">تیر</a>
                                </li>
                                <li>
                                    <a href="{{route('home.select_season',5)}}">مرداد</a>
                                </li>
                                <li>
                                    <a href="{{route('home.select_season',6)}}">شهریور</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12  zpad se-grid-Equal">
                    <div class="se-img3"></div>
                    <div class="overlay">
                        <div class="text">
                            <p>پاییز</p>
                            <ul>
                                <li>
                                    <a href="{{route('home.select_season',7)}}">مهر</a>
                                </li>
                                <li>
                                    <a href="{{route('home.select_season',8)}}">آبان</a>
                                </li>
                                <li>
                                    <a href="{{route('home.select_season',9)}}">آذر</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12  zpad se-grid-Equal">
                    <div class="se-img4"></div>
                    <div class="overlay">
                        <div class="text">
                            <p>زمستان</p>
                            <ul>
                                <li>
                                    <a href="{{route('home.select_season',10)}}">دی</a>
                                </li>
                                <li>
                                    <a href="{{route('home.select_season',11)}}">بهمن</a>
                                </li>
                                <li>
                                    <a href="{{route('home.select_season',12)}}">اسفند</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- </div>
        </div> -->
        </section>

    </div>
@endsection
