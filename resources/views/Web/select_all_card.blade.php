@extends('leyouts.web')
@section('content')


	<div class="breadcrump_holder">
		<a class="breadcrump-arrow-right" href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله قبل</a>
		<a class="breadcrump-arrow-left" href="#">مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
		<ul class="breadcrumb">
			<li class="active">
				<i class="fa fa-search" aria-hidden="true"></i>
				<a href="#">انتخاب مناسبت</a>
			</li>
			<li class="active">
				<i class="fa fa-credit-card" aria-hidden="true"></i>
				انتخاب کارت هدیه
			</li>
			<li class="actived">
				<i class="fa fa-picture-o" aria-hidden="true"></i>
				سفارشی سازی (انتخاب طرح کارت)
			</li>
			<li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
		</ul>
	</div>

	</div>
	</header>

	<div class="content productcontent">

		<section class="row wrapper mighty-slider-section ">
			<div class="row">
				<div class="col-md-3 xpad">
					<div class="panel">
						<div class="panel-header">
							فیلتر بر اساس
						</div>
						<div class="panel-body">
							<p>دسته بندی محصولات</p>
							<ul>
								<li>
									<input form="ch1" type="checkbox" name="">
									<label for="ch1">نوزاد و کودک (0)</label>
								</li>
								<li>
									<input form="ch1" type="checkbox" name="">
									<label for="ch1">فکری و کمک آموزشی (0)</label>
								</li>
							</ul>
							<p>نام تجاری</p>
							<ul>
								<li>
									<input form="ch1" type="checkbox" name="">
									<label for="ch1">نوزاد و کودک (0)</label>
								</li>
								<li>
									<input form="ch1" type="checkbox" name="">
									<label for="ch1">فکری و کمک آموزشی (0)</label>
								</li>
							</ul>
						</div>
						<!-- panel-body -->
					</div><!-- panel -->
				</div><!-- col-right -->

				<div class="col-md-9 xpad">

					<div class="panel">
						<div class="panel-header">
							<h2>انتخاب طرح کارت</h2>
						</div><!-- panel-header -->

						<div class="panel-body">
							<div class="kado_head">
								<div class="row">
									<div class="side_right col-md-6 col-sm-6 col-xs-6 col-xxs-12">
										<select class="input-form">
											<optgroup>
												نمایش 15
											</optgroup>
											<option>
												نمایش 30
											</option>
											<option>
												نمایش 50
											</option>
											<option>
												نمایش 100
											</option>
										</select>
										<select class="input-form">
											<option>
												به ترتیب جدید ترین
											</option>
										</select>
									</div><!-- sid_right -->
									<div class="side_left col-md-6 col-sm-6 col-xs-6 col-xxs-12 left">
										<ul>
											<li><a href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li><a href="#">5</a></li>
											<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
										</ul>
									</div><!-- side_left -->
								</div><!-- row -->
							</div><!-- kado_head -->

							<div class="diviv"></div>
							<div class="kado_photo">
								<div class="kado_photoiv">

									<div class="row">

										<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 zpad center">
											<div class="kado_imgiv">
												<img class="img-responsive" src="<?php echo route('home')?>/user/img/card/01e.png">
												<div class="kado_discription">
													<p>
														کارت هدیه
													</p>
													<p>
														<a href="<?php echo route('occasion');?>" class="btn-2">

															<span class="bor-span-right">انتخاب کارت هدیه</span>
														</a>
													</p>
												</div>
											</div><!-- kado_imgiv -->
										</div><!-- kado_img -->

										<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 zpad center">
											<div class="kado_imgiv">
												<img class="img-responsive" src="<?php echo route('home')?>/user/img/card/01m.png">
												<div class="kado_discription">
													<p>
														کارت هدیه
													</p>
													<p>
														<a href="<?php echo route('occasion');?>" class="btn-2">

															<span class="bor-span-right">انتخاب کارت هدیه</span>
														</a>
													</p>
												</div>
											</div><!-- kado_imgiv -->
										</div><!-- kado_img -->

										<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 zpad center">
											<div class="kado_imgiv">
												<img class="img-responsive" src="<?php echo route('home')?>/user/img/card/01t.png">
												<div class="kado_discription">
													<p>
														کارت هدیه
													</p>
													<p>
														<a href="<?php echo route('occasion');?>" class="btn-2">

															<span class="bor-span-right">انتخاب کارت هدیه</span>
														</a>
													</p>
												</div>
											</div><!-- kado_imgiv -->
										</div><!-- kado_img -->

										<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 zpad center">
											<div class="kado_imgiv">
												<img class="img-responsive" src="<?php echo route('home')?>/user/img/card/01v.png">
												<div class="kado_discription">
													<p>
														کارت هدیه
													</p>
													<p>
														<a href="<?php echo route('occasion');?>" class="btn-2">

															<span class="bor-span-right">انتخاب کارت هدیه</span>
														</a>
													</p>
												</div>
											</div><!-- kado_imgiv -->
										</div><!-- kado_img -->

										<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 zpad center">
											<div class="kado_imgiv">
												<img class="img-responsive" src="<?php echo route('home')?>/user/img/card/01p.png">
												<div class="kado_discription">
													<p>
														کارت هدیه
													</p>
													<p>
														<a href="<?php echo route('occasion');?>" class="btn-2">

															<span class="bor-span-right">انتخاب کارت هدیه</span>
														</a>
													</p>
												</div>
											</div><!-- kado_imgiv -->
										</div><!-- kado_img -->

										<div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12 zpad center">
											<div class="kado_imgiv">
												<img class="img-responsive" src="<?php echo route('home')?>/user/img/card/01n.png">
												<div class="kado_discription">
													<p>
														کارت هدیه
													</p>
													<p>
														<a href="<?php echo route('occasion');?>" class="btn-2">

															<span class="bor-span-right">انتخاب کارت هدیه</span>
														</a>
													</p>
												</div>
											</div><!-- kado_imgiv -->
										</div><!-- kado_img -->
									</div><!-- row -->

								</div><!-- kado_photoiv -->
							</div><!-- kado_photo -->
						</div><!-- panel-body -->
					</div><!-- panel -->

				</div><!-- col-left -->

			</div><!-- row -->
		</section>
	</div>

	<!-- footer -->
	<section class="bg-bottom-page-section">
		<span id="bg-bottom-page"></span>
		<div class="row bg-bottom-page">
			<div class="bg">
				<div class="bg-color">
					<div class="wrapper">

					</div>
				</div>

			</div>
		</div>
	</section>

@endsection