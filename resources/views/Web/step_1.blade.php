@extends('leyouts.web')
@section('content')

	<div class="breadcrump_holder">
		<a class="breadcrump-arrow-right" href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله قبل</a>
		<a class="breadcrump-arrow-left" href="#">مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>

		<ul class="breadcrumb">
			<li class="actived">
				<i class="fa fa-search" aria-hidden="true"></i>
				انتخاب مناسبت
			</li>
			<li class="">
				<i class="fa fa-credit-card" aria-hidden="true"></i>
				انتخاب کارت هدیه
			</li>
			<li class="">
				<i class="fa fa-picture-o" aria-hidden="true"></i>
				سفارشی سازی
			</li>
			<li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
		</ul>
	</div>

	</div>
	</header>

	<!-- <a class="wow bounceInDown" data-wow-duration="1.5s" data-wow-delay="0.5s" href="#">تولد</a> -->

	<div class="content">
		<section class="step-1">
			<ul>
				<li class="hearts heart-1 wow bounceInDown" data-wow-duration="2s" data-wow-delay="0s">
					<a href="<?php echo route('select_products')?>">
						<span class="heart-text">تولد</span>
					</a>
				</li>

				<li class="hearts heart-2 wow bounceInDown" data-wow-duration="2s" data-wow-delay="0.3s">
					<a href="<?php echo  route('select_products')?>">
						<span class="heart-text">ولنتاین</span>
					</a>
				</li>

				<li class="hearts heart-3 wow bounceInDown" data-wow-duration="2.5s" data-wow-delay="0.4s">
					<a href="<?php echo route('select_products')?>">
						<span class="heart-text">ازدواج</span>
					</a>
				</li>

				<li class="hearts heart-4 wow bounceInDown" data-wow-duration="2.5s" data-wow-delay="0.4s">
					<a href="<?php echo route('select_products')?>">
						<span class="heart-text">روز مادر</span>
					</a>
				</li>


				<li class="hearts heart-5 wow bounceInDown" data-wow-duration="2.2s" data-wow-delay="0.5s">
					<a href="<?php echo route('select_products')?>">
						<span class="heart-text">روز پدر</span>
					</a>
				</li>

				<li class="hearts heart-6 wow bounceInDown" data-wow-duration="2.5s" data-wow-delay="0.7s">
					<a href="<?php echo route('select_products')?>">
						<span class="heart-text">روز معلم</span>
					</a>
				</li>

				<li class="hearts heart-7 wow bounceInDown" data-wow-duration="2.7s" data-wow-delay="0.6s">
					<a href="<?php echo  route('select_products')?>">
						<span class="heart-text">عیدی</span>
					</a>
				</li>

				<li class="hearts heart-8 wow bounceInDown" data-wow-duration="2.5s" data-wow-delay="0.6s">
					<a href="<?php echo route('select_products')?>">
						<span class="heart-text">بدون مناسبت</span>
					</a>
				</li>
			</ul>
		</section>
	</div>

@endsection