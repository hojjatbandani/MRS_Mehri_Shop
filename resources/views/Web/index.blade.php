<!DOCTYPE html>
<html>
<head>
    <title>shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo route('home')?>/user/fonts/iransans/css/fontiran.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo route('home')?>/user/lib/mighty/src/css/mightyslider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo route('home')?>/user/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo route('home')?>/user/css/style.css">
</head>
<body class="homePage">

<header class="topHeader" id="home">
    <div class="nav_top">
        <div class="wrapper">
            <ul>
                <li>
                    <a href="{{ Route('home')}}/Login">
                        <span class="fa fa-user"></span>
                        ورود
                    </a>
                </li>
                <li>
                    <a href="{{ Route('home')}}/Register">
                        <span class="fa fa-lock"></span>
                        ثبت نام
                    </a>
                </li>
                <li>
						<span style="cursor:pointer" onclick="openNav()"><i class="fa fa-search"></i>
							جستجو
						</span>
                </li>
                <!-- search -->
                {{--<form action="{{ Route('home')}}/home_search" method="post">--}}
                    <div id="myNav" class="overlayfull">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <div class="overlayfull-content">
                            <input class="overlayfull-search" type="text" placeholder="کارت مورد نظر را جستجو کنید ...">
                        </div>
                    </div>
                {{--</form>--}}

                <li class="date">
                    <a href="#" onclick="openNav()">
                        <span class="fa fa-shopping-basket "></span>
                        سبد خرید
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="middle_nav index">
        <nav>
            <div class="wrapper menu">
                <a href="javascript:void(0);" class="res_menu">&#9776;</a>
                <ul>
                    <li><a href="#home">صفحه اصلی <i class="fa fa-home"></i> </a> <span class="border"></span></li>
                    <li><a href="#whatis">کادونت چیه؟  <i class="fa fa-gift"></i> </a> <span class="border"></span></li>
                    <li><a href="#HowToWork">چگونه کار میکند؟<i class="fa fa-question-circle"></i></a> <span
                                class="border"></span></li>
                    <li><a href="#services">خدمات ما<i class="fa fa-archive "></i></a> <span class="border"></span></li>
                    <li><a href="#selecthearts">شروع خرید<i class="fa fa-shopping-bag"></i></a> <span
                                class="border"></span></li>
                    <li><a href="#products">محصولات ما<i class="fa fa-list"></i></a> <span class="border"></span></li>
                    <li><a href="#aboutus">درباره ما<i class="fa fa-bullhorn"></i></a> <span class="border"></span></li>
                    <li><a target="_blank" href="<?php echo route('ContactUs')?>">تماس با ما<i class="fa fa-phone"></i></a> <span class="border"></span></li>

                    <li class="nav-links logo">
                        <a href="#">
                            <img src="user/img/logo.png" alt="logo"/>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

</header>


<div class="content homeIndex">

    <!-- slider -->
    <div class="divSlider row">
        <div class="one owl-carousel owl-theme">
            <div class="item">
						<span class="slide-item slide1">
							<div class="owl-slide-txt">
								<h1>کادونت</h1>
								<p>خوش آمدید</p>
								<a href="<?php echo route('stepone')?>" class="owl-slide-btn btn-2"> شروع خرید </a>
							</div>
						</span>
            </div>
            <div class="item">
						<span class="slide-item slide2">
							<div class="owl-slide-txt">
								<h1>کادونت</h1>
								<p>خوش آمدید</p>
								<a href="<?php echo route('stepone')?>" class="owl-slide-btn btn-2"> شروع خرید </a>
							</div>
						</span>
            </div>
            <div class="item">
						<span class="slide-item slide3">
							<div class="owl-slide-txt">
								<h1>کادونت</h1>
								<p>خوش آمدید</p>
								<a href="<?php echo route('stepone')?>" class="owl-slide-btn btn-2"> شروع خرید </a>
							</div>
						</span>
            </div>
        </div>
    </div>


    <section>
        <span id="whatis"></span>
        <div class="row whatis">
            <div class="bg">
                <div class="wrapper">
                    <div class="row inner-about-row">

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <span id="HowToWork"></span>
        <div class="row wrapper HowToWork">
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-6 zpad center wow zoomIn">
                <!-- <i class="fa fa-search" aria-hidden="true"></i> -->
                <span class="fasearch"></span>
                <h2>انتخاب مناسبت</h2>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-6 zpad center wow zoomIn">
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                <h2>انتخاب کارت هدیه</h2>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-6 zpad center wow zoomIn">
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                <h2>سفارشی سازی</h2>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-6 zpad center wow zoomIn">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                <h2>پرداخت اینترنتی و تحویل</h2>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
            </div>
        </div>
    </section>

    <section>
        <span id="services"></span>
        <div class="row services">
            <div class="bg">
                <div class="bg-color">
                    <div class="wrapper">
                        <h2 class="center wow fadeInDown"> خدمات </h2>
                        <p class="wow fadeInDown">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در
                            شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها
                            شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی
                            ایجاد کرد.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="" class="row wrapper select-gift-card">
        <span id="startbuy"></span>
        <h2 class="center">انتخاب کارت هدیه</h2>
        <div class="row gift-card-grid">
            <?php echo $cat;?>
        </div>
        <p class=" row center bottom-btn-holder">
            <a href="{{route('all_products')}}" class="btn-2">همه موارد و جزئیات بیشتر</a>
        </p>
    </section>


    <section class="step-1">
        <span id="selecthearts"></span>
        <ul>
            <?php
            $time = "0";
            if (count($occasion) > 0) {
            $x = "2s";
            foreach ($occasion as $oca) {  ?>
            <li class='hearts heart-1 wow bounceInDown'  data-wow-duration ="<?php echo $x ;?>" data-wow-delay = "<?php echo $time; ?>s" >
                <a href ="<?php echo Route('select_products',$oca['occasion_parent_id'])?>">
                    <span class='heart-text' ><?php echo $oca['occasion_text'];?></span>
                </a >
            </li >

            <?php
            $time = $time + 0.25;
            }
            }?>
        </ul>
    </section>


    <section>
        <span id="products"></span>
        <div class="row wrapper products wow zoomIn">
            <h2 class="center">محصولات</h2>

            <!-- PARENT -->
            <div id="example" class="mightyslider_modern_skin black ltr-dir">
                <!-- FRAME -->
                <div class="frame" data-mightyslider="
				        width: 1250,
				        height: 350
				    ">
                    <!-- SLIDEELEMENT -->
                    <div class="slide_element">

                        <!-- SLIDES -->
                        <div class="slide prev_2" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01m.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01m.png',
				                link: {
				                    url: '<?php echo route('occasion_description');?>',

				                }
				            "></div>
                        <div class="slide prev_1" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01e.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01e.png',
				                link: {
				                    url: '<?php echo route('occasion_description');?>',

				                }
				            "></div>
                        <div class="slide active" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01t.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01t.png',
				                link: {
				                    url: '<?php echo route('occasion_description');?>',

				                }
				            ">
                            <!-- LAYER -->
                            <div class="mSCaption">
                                You can use direct video url<br />for full-sized videos & covers
                            </div>
                        </div>
                        <div class="slide next_1" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01n.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01n.png',
				                link: {
				                    url: '<?php echo route('occasion_description');?>',

				                }
				            "></div>
                        <div class="slide next_2" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01v.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01v.png',
				                link: {
				                    url: '<?php echo route('occasion_description');?>',

				                }
				            "></div>
                        <div class="slide" data-mightyslider="
				                cover: '<?php echo route('home')?>/user/img/card/01p.png',
				                thumbnail: '<?php echo route('home')?>/user/img/card/01p.png',
				                link: {
				                    url: '<?php echo route('occasion_description');?>',

				                }
				            "></div>
                        <!-- END OF SLIDES -->
                    </div>
                    <!-- END OF SLIDEELEMENT -->
                    <!-- ARROW BUTTONS -->
                    <a class="mSButtons mSPrev"></a>
                    <a class="mSButtons mSNext"></a>
                </div>
                <!-- END OF FRAME -->
                <!-- SLIDER TIMER -->
                <canvas id="progress" width="160" height="160"></canvas>
                <!-- END OF SLIDER TIMER -->
                <!-- THUMBNAILS -->
                <div id="thumbnails">
                    <div>
                        <ul></ul>
                    </div>
                </div>
                <!-- END OF THUMBNAILS -->
            </div>
            <!-- END OF PARENT -->


            <p class=" row center bottom-btn-holder" style="margin-top:15px;">
                <a href="<?php echo route('all_card')?>" class="btn-2">مشاهده همه طرح های کارت هدیه</a>
            </p>
        </div>
    </section>

    <section>
        <span id="aboutus"></span>
        <div class="row aboutus">
            <div class="bg">
                <div class="bg-color">
                    @if(count($about)>0)
                        @foreach ($about as $object)
                            {{ $object->about_text }}
                        @endforeach
                    @endif
                </div>

            </div>
        </div>
    </section>

</div>

<footer id="contactus" class="footer-page">

    <div class="row main-footer">
        <div class="wrapper">
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
                <h2>لینک های مفید</h2>
                <ul class="rpad">
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
                <h2>لینک های مفید</h2>
                <ul class="rpad">
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
                <h2>لینک های مفید</h2>
                <ul class="rpad">
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                    <li>
                        <a href="#">لینک های مفید</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 col-xxs-12 zpad">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 zpad">
                        <img src="img/logo.png">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 zpad">
                        <img src="img/enamad.png">
                    </div>
                </div>

                <div class="row footer-icons center">
                    <ul class="ul-inline">
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook-square" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-telegram" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row center bottom-footer">
        <p>تمامی حقوق متعلق به سایت میباشد</p>
    </div>
</footer>


<!-- JS files -->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<!-- Uses the built in easing capabilities added In jQuery 1.1 to offer multiple easing options -->
<script type="text/javascript" src="<?php echo route('home')?>/user/lib/mighty/assets/js/jquery.easing-1.3.pack.js"></script>
<!-- Mobile touch events for jQuery -->
<script type="text/javascript" src="<?php echo route('home')?>/user/lib/mighty/assets/js/jquery.mobile.just-touch.js"></script>

<script type="text/javascript" src="<?php echo route('home')?>/user/lib/mighty/src/js/tweenlite.js"></script>
<!-- Main slider JS script file -->
<script type="text/javascript" src="<?php echo route('home')?>/user/lib/mighty/src/js/mightyslider.min.js"></script>
<script type="text/javascript" src="<?php echo route('home')?>/user/lib/wow/dist/wow.min.js"></script>

<script src="<?php echo route('home')?>/user/js/main.js"></script>
</body>
</html>