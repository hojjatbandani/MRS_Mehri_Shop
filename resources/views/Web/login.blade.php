<!DOCTYPE html>
<html>
<head>
    <title>shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="user/fonts/iransans/css/fontiran.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="user/css/animate.css">
    <link rel="stylesheet" type="text/css" href="user/css/style.css">
</head>
<body class="homePage">

<div class="content logincontent">
    <section class="row">
        <div class="col-md-4 col-sm-6 col-xs-8 col-xxs-12 login-box">
            <div class="login-main-form">
                @include('Web.partials.errors')
                {{isset($error_check)?$error_check:''}}
                <p id="message"></p>
                <div class="login-logo">
                    <a href="{{route('home')}}"></a>
                </div>
                <form class="login-form" action="{{route('Login')}}" method="post" >
                    {!! csrf_field() !!}
                    <p>
                        <label class="label-form wide">نام کاربری</label>
                        <input type="email" id="email_login" name="email" required="required" class="input-form wide">

                        <label class="label-form wide">رمز عبور</label>
                        <input type="password" id="password_login" name="password" required="required" class="input-form wide">
                    </p>
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8 col-xxs-8">
                            <p class="remember">
                                <input type="checkbox" name="remember">مرا بخاطر بسپار
                            </p>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-4">
                            <input class="btn-plus lfloat" type="submit" name="sub_log" value="ورود"/>
                        </div>
                    </div>

                    <a class="forgot-password" href="#">رمز عبورتان را فراموش کرده اید؟</a>

                    <i class="fa fa-heart bottom-center-heart" aria-hidden="true"></i>
                </form>

                <div class="row new-accont-btn">
                    <a class="btn-1" href="{{route('home.Register')}}">ایجاد حساب کاربری</a>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-sm-6 col-xs-4 col-xxs-12 signIn-left-bg">
            <div class="signIn-text">
                <h2 class="signInTitr">ورود به ناحیه کاربری</h2>
                <p class="signIn-subHeaderTitr">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است
                </p>
            </div>
        </div>
    </section>
</div>


<!-- JS files -->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<!-- Uses the built in easing capabilities added In jQuery 1.1 to offer multiple easing options -->
<script type="text/javascript" src="user/lib/mighty/assets/js/jquery.easing-1.3.pack.js"></script>
<!-- Mobile touch events for jQuery -->
<script type="text/javascript" src="user/lib/mighty/assets/js/jquery.mobile.just-touch.js"></script>

<script type="text/javascript" src="user/lib/mighty/src/js/tweenlite.js"></script>
<!-- Main slider JS script file -->
<script type="text/javascript" src="user/lib/mighty/src/js/mightyslider.min.js"></script>

<script src="user/js/main.js"></script>
</body>
</html>