@extends('leyouts.web')
@section('content')

    <div class="breadcrump_holder">
        <a class="breadcrump-arrow-right" href='{{route('custom')}}'><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله
            قبل</a>
        <a class="breadcrump-arrow-left" href='{{route('Web.Kado')}}'>مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>

        <ul class="breadcrumb">
            <li class="active">
                <i class="fa fa-search" aria-hidden="true"></i>
                <a href="#">انتخاب مناسبت</a>
            </li>
            <li class="active">
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                <a href="#">انتخاب کارت هدیه</a>
            </li>
            <li class="actived">
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                سفارشی سازی
            </li>
            <li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
        </ul>
    </div>

    <div class="content customizetwo">
        <section class="row wrapper">

            <div class="col-md-3 col-sm-4 col-xs-12 col-xxs-12 zpad">
                <div class="panel">
                    <div class="panel-body">
                        <p>تبلیغات</p>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">
                        <p>تبلیغات</p>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <p>تبلیغات</p>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <p>تبلیغات</p>
                    </div>
                </div>

            </div>

            <div class="col-md-9 col-sm-8 col-xs-12 col-xxs-12 zpad">

                <div class="panel">
                    <div class="panel-header">
                        <h2>سفارشی سازی</h2>
                    </div>
                    <div class="panel-body">
                        <div class="img-holder row">
                            <img class="img-responsive" src="user/img/kart-2.png">
                        </div>

                        <div class="row under-line">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 zpad">
                                <div class="col-md-2">
                                    <label class="label-form wide">مبلغ هدیه</label>
                                </div>
                                <div class="col-md-10">
                                    <input class="input-form wide load_hedye_by_ajax"  type="text" placeholder="0 تومان"/>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 zpad">
                                <div class="col-md-2">
                                    <label class="label-form">متن دلخواه</label>
                                </div>
                                <div class="col-md-10">
                                    <input class="input-form wide" id="caption_onimage" type="text" placeholder="متن دلخواه خود را بنویسید"/>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 zpad">
                                <div class="col-md-2">
							<span class="label-form">
								<label class="label-form no-lpad">تصویر پیشفرض</label>
							</span>
                                </div>

                                <div class="col-md-10">
                                <!-- Rounded switch -->
                                {{--<label class="switch">--}}
                                    {{--<input type="checkbox">--}}
                                    {{--<span class="slider round"></span>--}}
                                {{--</label>--}}

                                <div class="input-form wide">
                                        <input type="file" accept="image/*" onchange="loadFile(event)" class="input-dsgn">
                                        <input type="button" onclick="getFile.simulate()" value="آپلودتصویردلخواه" accept="image/*" onchange="loadFile(event)">
                                         <label id="selected">تصویر دلخواه خود را آپلود کنید</label>
                                </div>
                                </div>
                            </div>



                            <div style="text-align:center">
                                <a class="btn-1 preview-btn" href="#" id="myBtn" style="margin-top:0;margin-bottom:10px;">
                                    <span>پیشنمایش</span>
                                </a>
                            </div>

                        </div>

                        <div class="row under-line">
                            <p class="zpad">نحوه ارسال</p>

                            <div class="kadr-blue-white row">
                                <i class="fa fa-edge icon-kadr" aria-hidden="true"></i>
                                <span class="inline post-text-kadr">
								<input type="radio" name="select-customize-image" class="tablinks"
                                       onclick="box1(event, 'tab1')">
								<label class=""> ارسال به ایمیل </label>

							</span>
                                <p class="description-span">
                                    ( در این صورت میتوانید به صورت الکترونیکی تصویر کارت را در ایمیل خود دریافت و سپس به
                                    دوست خود از طریق ایمیل، تلگرام و ... بدهید و یا از طرف ما به دوست تان ایمیل شود )
                                </p>
                            </div>

                            <div class="kadr-blue-white row">
                                <i class="fa fa-motorcycle icon-kadr" aria-hidden="true"></i>
                                <span class="inline post-text-kadr">
									<input type="radio" name="select-customize-image" class="tablinks"
                                           onclick="box1(event, 'tab2')">
								<label class="">ارسال از طریق پیک موتوری  <span> ( زمان ارسال یک تا چهار ساعت ) </span></label>
							</span>
                                <span class="price-post-kadr">هزینه ارسال <span
                                            class="num-fa">4,000</span> تومان </span>
                            </div>

                            <div class="kadr-blue-white row">
                                <i class="fa fa-plane icon-kadr" aria-hidden="true"></i>
                                <span class="inline post-text-kadr">
								<input type="radio" name="select-customize-image" class="tablinks"
                                       onclick="box1(event, 'tab3')">
								<label class="">پست پیشتاز <span> ( زمان ارسال یک تا دو روزه ) </span></label>
							</span>
                                <span class="price-post-kadr">هزینه ارسال <span
                                            class="num-fa">9,000</span> تومان </span>
                            </div>
                        </div>

                        <div class="row">
                            <div id="tab1" class="tabcontent clearfix">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">آدرس ایمیل فرستنده کادو</label>
                                    <input class="input-form wide" type="email" name="">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">آدرس ایمیل گیرنده کادو</label>
                                    <input class="input-form wide" type="email" name="">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">زمان ارسال ایمیل</label>
                                    <input class="input-form wide" type="email" name="">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">موضوع ایمیل</label>
                                    <input class="input-form wide" type="email" name="">
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">متن ایمیل</label>
                                    <textarea class="textarea-form wide" rows="8"></textarea>
                                </div>

                                <div class="center">
                                    <a class="btn-2 last_step"  href='{{route('Basket_shop')}}'>مشاهده جزییات پرداخت</a>
                                </div>
                            </div>

                            <div id="tab2" class="tabcontent clearfix">
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">استان</label>
                                    <select class="input-form wide ostan">
                                        <?php echo $res; ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">شهر</label>
                                    <select class="input-form wide city">

                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">منطقه</label>
                                    <select class="input-form wide">
                                    </select>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">شماره تلفن همراه گیرنده</label>
                                    <input class="input-form wide" type="text" name="">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">شماره ثابت گیرنده</label>
                                    <input class="input-form wide" type="text" name="">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">کد پستی</label>
                                    <input class="input-form wide" type="text" name="">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">آدرس گیرنده</label>
                                    <textarea class="textarea-form wide" rows="6"></textarea>
                                </div>


                                <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">توضیح اضافی در مورد ارسال در صورت نیاز</label>
                                    <textarea class="textarea-form wide" rows="6"></textarea>
                                </div>

                                <div class="center">
                                    <a class="btn-2 last_step" href={{route('Web.Kado')}}>مشاهده جزییات پرداخت</a>
                                </div>
                            </div>

                            <div id="tab3" class="tabcontent clearfix">
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">استان</label>
                                    <select class="input-form wide ostan">
                                        <?php echo $res; ?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">شهر</label>
                                    <select class="input-form wide city">
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">منطقه</label>
                                    <select class="input-form wide">
                                    </select>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">شماره تلفن همراه گیرنده</label>
                                    <input class="input-form wide" type="text" name="">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">شماره ثابت گیرنده</label>
                                    <input class="input-form wide" type="text" name="">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12 xpad">
                                    <label class="label-form">کد پستی</label>
                                    <input class="input-form wide" type="text" name="">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">آدرس گیرنده</label>
                                    <textarea class="textarea-form wide" rows="6"></textarea>
                                </div>


                                <div class="col-md-6 col-sm-6 col-xs-12 col-xxs-12 xpad">
                                    <label class="label-form">توضیح اضافی در مورد ارسال در صورت نیاز</label>
                                    <textarea class="textarea-form wide" rows="6"></textarea>
                                </div>

                                <div class="center">
                                    <a class="btn-2 last_step" href={{route('Web.Kado')}}>مشاهده جزییات پرداخت</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </section>
    </div>


    <!-- The Modal -->
    <div id="myModal" class="modal">
        <span class="close">×</span>
        <div class="modal-content">
            <img id="output" class="modal-content-img" src="user/img/kart-2.png">

            <p id="caption">عنوان دلخواه</p>
        </div>
    </div>


@endsection

