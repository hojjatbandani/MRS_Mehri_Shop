@extends('leyouts.web')
@section('content')
<div class="breadcrump_holder">
	<a class="breadcrump-arrow-right" href='{{route('Customize_2')}}'><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله قبل</a>
	<a class="breadcrump-arrow-left" href='{{route('Basket_shop')}}'>مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>

	<ul class="breadcrumb">
		<li class="active">
			<i class="fa fa-search" aria-hidden="true"></i>
			<a href="#">انتخاب مناسبت</a>
		</li>
		<li class="active">
			<i class="fa fa-credit-card" aria-hidden="true"></i>
			<a href="#">انتخاب کارت هدیه</a>
		</li>
		<li class="actived">
			<i class="fa fa-picture-o" aria-hidden="true"></i>
			سفارشی سازی (کادوپیچی)
		</li>
		<li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
	</ul>
</div>

<div class="content kadocontent">

	<section class="row wrapper mighty-slider-section ">
		<div class="panel">
			<div class="panel-header tab-header">
				<div class="tab">
					<button class="tablinks" onclick="box1(event, 'tab1')" id="defaultOpen"> پاکت ساده هدیه </button>
					<button class="tablinks" onclick="box1(event, 'tab2')">جعبه هدیه </button>
				</div>
			</div>

			<div class="panel-body">
				<div id="tab1" class="tabcontent">
					<div class="kado_photo">
						<div class="kado_photoiv">

							<div class="row">

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->
							</div><!-- row -->

						</div><!-- kado_photoiv -->
					</div><!-- kado_photo -->
				</div>

				<div id="tab2" class="tabcontent">
					<div class="kado_photo">
						<div class="kado_photoiv">

							<div class="row">

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->

								<div class="col-md-4 col-sm-6 col-xs-12 col-xxs-12 zpad center">
									<div class="kado_imgiv">
										<img class="img-responsive" src="user/img/kado.jpg">
										<div class="kado_discription">
											<p>
												جعبه کادو گلدار صورتی
											</p>
											<p>
												<a href='{{route('Basket_shop')}}' class="btn-2">
													<i class="fa fa-shopping-cart"></i>
													<span class="bor-span-right kado_price"><span>39000</span> تومان</span>
												</a>
											</p>
										</div>
									</div><!-- kado_imgiv -->
								</div><!-- kado_img -->
							</div><!-- row -->

						</div><!-- kado_photoiv -->
					</div><!-- kado_photo -->
				</div>
			</div>
		</div>

	</section>

</div>
@endsection
