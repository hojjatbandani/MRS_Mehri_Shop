@extends('leyouts.web')
@section('content')
    <div class="breadcrump_holder">
        <a class="breadcrump-arrow-right" href='{{route('home')}}'><i class="fa fa-angle-right" aria-hidden="true"></i>خانه
        </a>
        <a class="breadcrump-arrow-left" href='{{route('home.select_season')}}'>مرحله بعد<i class="fa fa-angle-left" aria-hidden="true"></i></a>

        <ul class="breadcrumb">
            <li class="actived">
                <i class="fa fa-search" aria-hidden="true"></i>
                <a>انتخاب مناسبت(عید)</a>
            </li>
            <li>
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                <a>انتخاب کارت هدیه</a>
            </li>
            <li>
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                سفارشی سازی
            </li>
            <li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
        </ul>
    </div>

    <div class="content">
        <section class="row wrapper	gift-card">
            <div class="panel">
                <div class="panel-header">
                    <h2>انتخاب عید</h2>
                </div>
                <div class="panel-body">


                    <div class="row gift-card-grid">
                        <?php if (count($eyd) > 0) {
                        foreach ($eyd as $row) {
                        ?>
                        <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12  zpad eid-grid-Equal">
                            <div class="eid-img eid-img1"><p class="text"><?php echo $row->eyd_name;?></p></div>
                            <div class="overlay-fade">
                                <div class="text">
                                    <div class="text">
                                        <a class="btn-3" href={{route('home.select_eyd',$row->eyd_name)}}>انتخاب</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }}else {
                        }?>
                        <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12  zpad eid-grid-Equal">
                            <div class="eid-img eid-img4"><p class="text">عید دیگر</p></div>
                            <div class="overlay-fade">
                                <div class="text">
                                    <div class="text">
                                        <a class="btn-3" href="#">انتخاب</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <section class="bg-bottom-page-section">
        <span id="bg-bottom-page"></span>
        <div class="row bg-bottom-page">
            <div class="bg">
                <div class="bg-color">
                    <div class="wrapper">

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
