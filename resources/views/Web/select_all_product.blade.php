@extends('leyouts.web')
@section('content')
    <div class="breadcrump_holder">
        <a class="breadcrump-arrow-right" href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>مرحله
            قبل</a>
        <a class="breadcrump-arrow-left" href="#">مرحله بعد<i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>

        <ul class="breadcrumb">
            <li class="active">
                <i class="fa fa-search" aria-hidden="true"></i>
                <a href="#">انتخاب مناسبت</a>
            </li>
            <li class="actived">
                <i class="fa fa-credit-card" aria-hidden="true"></i>
                انتخاب کارت هدیه
            </li>
            <li class="">
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                سفارشی سازی
            </li>
            <li><i class="fa fa-shopping-cart" aria-hidden="true"></i>پرداخت اینترنتی و تحویل</li>
        </ul>
    </div>


    <div class="content">

        <section class="row all-gift-card wrapper">
            <div class="panel">
                <div class="panel-header row center">
                    <h2 class="div-range-after">رنج قیمت : </h2>
                    <!-- Page contents -->
                    <div class="div-range-holder">
                        <div>
                            <input type="text" id="range" value="" name="range"/>
                        </div>
                    </div>
                    <div class="div-range-after">
                        <a class="btn-1" href="#">نمایش</a>
                    </div>
                </div>
                <div class="panel-body allgift">


                            <?php echo $res; ?>


                </div>
            </div>
        </section>
    </div>
@endsection

