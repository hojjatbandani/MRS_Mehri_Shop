<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Admins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Admins', function (Blueprint $table) {
            $table->increments('Admin_id');
            $table->string('Admin_family',200);
            $table->string('Admin_email',200);
            $table->string('Admin_password',200);
            $table->string('Admin_username',200);
            $table->enum('Admin_dashboard',array('off','on'));
            $table->enum('Admin_listUser',array('off','on'));
            $table->enum('Admin_orders',array('off','on'));
            $table->enum('Admin_middle_managers',array('off','on'));
            $table->enum('Admin_category',array('off','on'));
            $table->enum('Admin_cards',array('off','on'));
            $table->enum('Admin_occasion',array('off','on'));
            $table->enum('Admin_age',array('off','on'));
            $table->enum('Admin_textCard',array('off','on'));
            $table->enum('Admin_comment',array('off','on'));
            $table->enum('Admin_advertising',array('off','on'));
            $table->enum('Admin_transaction',array('off','on'));
            $table->enum('Admin_page_settings',array('off','on'));
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Admins');
    }
}
