<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Transactions', function (Blueprint $table) {
            $table->increments('transaction_id');
            $table->integer('transaction_amount');
            $table->integer('user_id');
            $table->integer('transantion_type');
            $table->timestamp('transantion_date');
            $table->string('transaction_authorycode',200);
            $table->string('authorycode_refid',50);
            $table->integer('order_id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Transactions');
    }
}
