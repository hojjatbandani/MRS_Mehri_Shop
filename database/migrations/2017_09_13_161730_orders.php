<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Orders', function (Blueprint $table) {
            $table->increments('Orders_id');
            $table->integer('cat_id');
            $table->integer('card_id');
            $table->string('Orders_cardtext',200);
            $table->string('Orders_card_custom_image',200);
            $table->integer('Orders_num_card');
            $table->integer('Orders_price_card');
            $table->integer('Orders_send_type');
            $table->text('Orders_receiver_address');
            $table->string('Orders_receiver_mobile',20);
            $table->string('Orders_receiver_phone',20);
            $table->text('Orders_more_desc');
            $table->string('Orders_receiver_email',100);
            $table->timestamp('Orders_receiver_date_send_email');
            $table->text('Orders_receiver_email_desc');
            $table->string('Orders_receiver_email_title',100);
            $table->string('Orders_card_code',100);
            $table->string('Orders_Tracking_code',100);
            $table->integer('GiftType_id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Orders');
    }
}
