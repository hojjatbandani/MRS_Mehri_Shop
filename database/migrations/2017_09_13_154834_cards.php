<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('card_id');
            $table->string('card_front_image',200);
            $table->string('card_behind_image',200);
            $table->integer('card_category');
            $table->integer('card_occasion');
            $table->integer('card_age_category');
            $table->string('card_title',200);
            $table->integer('card_season');
            $table->integer('card_sexuality');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
