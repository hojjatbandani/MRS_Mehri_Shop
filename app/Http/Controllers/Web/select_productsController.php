<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Web\category_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class select_productsController extends Controller
{
    function index($id = 0)
    {
        if ($id == 1){
            return view('Web.session');

        }elseif ($id == 2){
            $eyd =  category_md::load_eid();
            return view('Web.eid',compact('eyd'));

        }else{
            $cat = category_md::load_category();
            return view('Web.select_product',compact('cat'));
        }
//        Session::set('id_occasion', $id_oca);

    }

    function season()
    {
        return view('Web.session');
    }
}
