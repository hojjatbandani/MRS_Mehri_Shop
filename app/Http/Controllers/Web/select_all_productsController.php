<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Web\category_md;
use App\Models\Web\Products_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class select_all_productsController extends Controller
{
    function index()
    {
        $res=Products_md::Load_pro();
        return view('Web.select_all_product',compact('res'));
    }

    function Basket_shop()
    {
        return view('Web.basket_shop');
    }

    function Login()
    {
        return view('Web.basket_shop');
    }

    function Register()
    {
        return view('Web.basket_shop');
    }
}
