<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Web\Home_md;
use App\Models\Web\category_md;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class OccasionController extends Controller
{
    function index()
    {
        $occasion = Home_md::load_occasion();
        return view('Web.step_1', compact('occasion'));
    }

    function occasion_about()
    {
        $occasion = category_md::Load_category_empty();
        return view('Web.select_ product_index', compact('occasion'));
    }

    function occasion_description()
    {
        $occasion = Home_md::load_occasion();
        return view('Web.customize_index', compact('occasion'));
    }

}
