<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Web\Cards_md;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Basket_shopController extends Controller
{
    function index(Request $request)
    {
        $amount_card = 0;
        if($request->session()->has('savecharge'))
        {
            $amount_card = $request->session()->get('savecharge');
        }
        $city_price=0;
        if($request->session()->has('city_price'))
        {
            $city_price = $request->session()->get('city_price');
        }
        $Kado_Price=0;
        if($request->session()->has('Kado_Price'))
        {
            $Kado_Price = $request->session()->get('Kado_Price');
        }
        return view('Web.basket_shop',compact('amount_card','city_price','Kado_Price'));
    }

    function Login()
    {
        return view('Web.basket_shop');
    }

    function Register()
    {
        return view('Web.basket_shop');
    }
}
