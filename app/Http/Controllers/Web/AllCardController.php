<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Web\category_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AllCardController extends Controller
{
    function index()
    {
        return view('Web.select_all_card');
    }

    function Basket_shop()
    {
        return view('Web.basket_shop');
    }

    function Login()
    {
        return view('Web.basket_shop');
    }

    function Register()
    {
        return view('Web.basket_shop');
    }
}
