<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Models\Web\Register_md;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    function index()
    {
        return view('Web.Register');
    }

    function Register()
    { $this->validate(request(),[
        'password'=>'min:6'
    ],[
        'password.min'=>'کلمه عبور باید حداقل 6 حرف باشد'
    ]);
        $data = [
            'user_name' => request()->input('name'),
            'user_family' => request()->input('family'),
            'user_email' => request()->input('email'),
            'user_phone' => request()->input('phone_number'),
            'user_password' =>md5(request()->input('password'))
        ];
        $res = Register_md::insert_user($data);
        if ($res == 'true') {
            return redirect('Login');
        } else if ($res == 'false'){
            return view('Web.Register');
        }else{
            return view('Web.Register')->with(['error_check'=>'این ایمیل استفاده شده لطفا دوباره امتحان کنید']);
        }
    }
}
