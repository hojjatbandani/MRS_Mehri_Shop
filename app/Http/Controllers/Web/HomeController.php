<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Web\Home_md;
use Illuminate\Http\Request;
use Session;
use App\Models\Web\category_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class HomeController extends Controller
{
    function index()
    {
        $about = Home_md::about_us();
        $occasion = Home_md::load_occasion();
        $cat = category_md::load_category_in();
        return view('Web.index', compact('about', 'occasion', 'cat'));
    }

    function services()
    {
        $about = Home_md::about_us();
        $occasion = Home_md::load_occasion();
        $cat = category_md::load_category_in();
        return view('Web.services', compact('about'));
    }

    function howtowork()
    {
        $about = Home_md::about_us();
        $occasion = Home_md::load_occasion();
        $cat = category_md::load_category_in();
        return view('Web.howtowork', compact('about'));
    }

    function whatis()
    {
        $about = Home_md::about_us();
        $occasion = Home_md::load_occasion();
        $cat = category_md::load_category_in();
        return view('Web.whatis', compact('about'));
    }

    function stepone()
    {
        $about = Home_md::about_us();
        $occasion = Home_md::load_occasion();
        $cat = category_md::load_category_in();
        return view('Web.step_one', compact('about', 'occasion', 'cat'));
    }



    function load_city($id){
        $str = Home_md::load_city($id);
        return $str;
    }

    function codeoff($id){
        $str = Home_md::load_code($id);
        return $str;
    }

    function savecity($id){
        Home_md::savecity($id);
    }

    function saveKado($id){
        Home_md::saveKado($id);
    }

    function savecharge($id){
        Home_md::savecharge($id);
    }

    function select_season(Request $request, $id = 0)
    {
        if ($id == 1) {
            $request->session()->put('month', 'فروردین');
            $request->session()->put('season', 'بهار');
        } elseif ($id == 2) {
            $request->session()->put('month', 'اردیبهشت');
            $request->session()->put('season', 'بهار');
        } elseif ($id == 3) {
            $request->session()->put('month', 'خرداد');
            $request->session()->put('season', 'بهار');
        } elseif ($id == 4) {
            $request->session()->put('month', 'تیر');
            $request->session()->put('season', 'تابستان');
        } elseif ($id == 5) {
            $request->session()->put('month', 'مرداد');
            $request->session()->put('season', 'تابستان');
        } elseif ($id == 6) {
            $request->session()->put('month', 'شهریور');
            $request->session()->put('season', 'تابستان');
        } elseif ($id == 7) {
            $request->session()->put('month', 'مهر');
            $request->session()->put('season', 'پاییز');
        } elseif ($id == 8) {
            $request->session()->put('month', 'آبان');
            $request->session()->put('season', 'پاییز');
        } elseif ($id == 9) {
            $request->session()->put('month', 'آذر');
            $request->session()->put('season', 'پاییز');
        } elseif ($id == 10) {
            $request->session()->put('month', 'دی');
            $request->session()->put('season', 'زمستان');
        } elseif ($id == 11) {
            $request->session()->put('month', 'بهمن');
            $request->session()->put('season', 'زمستان');

        } elseif ($id == 12) {
            $request->session()->put('month', 'اسفند');
            $request->session()->put('season', 'زمستان');
        }
        $age = Home_md::load_age();
        return view('Web.age', compact('age'));
    }

    function select_age(Request $request, $title, $id)
    {
        $request->session()->put('age', $title);
        $request->session()->put('gender', $id);

        $cat = category_md::load_category();
        return view('Web.select_product', compact('cat'));

    }

    function select_eyd(Request $request, $title)
    {
        $request->session()->put('eyd_title', $title);

        $age = Home_md::load_age();
        return view('Web.age', compact('age'));

    }


}
