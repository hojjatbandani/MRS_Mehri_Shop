<?php

namespace App\Http\Controllers\Web;

use Illuminate\Routing\Route;
use App\Models\Web\Login_md;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    function index()
    {
        return view('Web.login');
    }

    function login()
    {
        $this->validate(request(), [
            'password' => 'min:6'
        ], [
            'password.min' => 'کلمه عبور باید حداقل 6 حرف باشد'
        ]);
        $data = [
            'user_email' => request()->input('email'),
            'user_password' =>md5(request()->input('password')),
        ];
        $remember=request()->input('remember');
        if ($remember == true) {

            if ($res == true) {
                return redirect('/');
            } else {
                return view('Web.login')->with(['error_check' => 'نام کاربری یا کلمه عبور اشتباه است لطفا دوباره امتحان کنید']);
            }
        } else {
            $res = Login_md::login($data);
            if ($res == true) {
                return redirect('/');
            } else {
                return view('Web.login')->with(['error_check' => 'نام کاربری یا کلمه عبور اشتباه است لطفا دوباره امتحان کنید']);
            }
        }
        $res = Login_md::login($data);
        if (count($res) > 0) {
            return redirect('/');
        } else {
            return view('Web.login');
        }

    }
}
