<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\List_orders_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ListOrdersController extends Controller
{
    function index()
    {
        $res = List_orders_md::load_orders();
        return view('admin.ListOrders', compact('res'));
    }

    function DeleteOrders(Request $request,$id)
    {
        $res = List_orders_md::DeleteOrders($id);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت حذف شد!');
            return redirect(route('Admin.Orders'));
        } else {
            $request->session()->flash('alert-success', ' حذف نشد!');
            return redirect(route('Admin.Orders'));
        }
    }
}
