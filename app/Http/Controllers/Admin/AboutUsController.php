<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use  App\Models\Admin\AboutUs_md;
use App\Models\Admin\CadoShop_md;
use  App\Models\Admin\ContactUs_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AboutUsController extends Controller
{
    function index()
    {
        $res = AboutUs_md::loadAbout();
        $con = ContactUs_md::loadContact();
        $cado = CadoShop_md::loadCadoShop();
        return view('admin.AboutUs', compact('res', 'con', 'cado'));
    }

    function EditAbout(Request $request)
    {
        $this->validate($request, [
            'TextAboutEditUs' => 'required'
        ], [
            'TextAboutEditUs.required' => 'لطفا متن را وارد کنید'
        ]);
        $id = request()->input('About_id');
        $text = request()->input('TextAboutEditUs');
        $res = AboutUs_md::EditAbout($id, $text);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت ویرایش شد!');
            return redirect(route('Admin.AboutUs'));
        } else {
            $request->session()->flash('alert-success', ' ویرایش نشد!');
            return redirect(route('Admin.AboutUs'));
        }
    }

    function DateAbout($id)
    {
        $res = AboutUs_md::GetData($id);
        echo json_encode($res);
    }
}
