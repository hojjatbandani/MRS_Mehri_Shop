<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\SendPrice_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\Request;

class ManagersSendPriceController extends Controller
{
    function index()
    {
        $code = SendPrice_md::load_code_off();
        return view('admin.ManagersSendPrice', compact('code'));
    }

    function edit_code_off($id)
    {
        $code = SendPrice_md::edit_code_off($id);
        echo json_encode($code);

    }

    function add_code_off(Request $request)
    {
        SendPrice_md::add_code_off($request);
        $request->session()->flash('alert-success', 'ثبت با موفقیت انجام شد!');
        return redirect(route('Admin.ManageSEND'));
    }
    function delete_code_off(Request $request,$id)
    {
        SendPrice_md::del_code_off($id);
        $request->session()->flash('alert-success', 'حذف با موفقیت انجام شد!');
        return redirect(route('Admin.ManageSEND'));
    }
    function code_off_edit(Request $request)
    {
        SendPrice_md::code_off_edit($request);
        $request->session()->flash('alert-success', 'ویرایش با موفقیت انجام شد!');
        return redirect(route('Admin.ManageSEND'));
    }

}
