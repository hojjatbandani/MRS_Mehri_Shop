<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\occasion_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ManagersOccasionController extends Controller
{
    function index()
    {
        $occasion = occasion_md::load_occasion();
        return view('admin.ManagersOccasion', compact('occasion'));
    }

    function Add_occasion(Request $request)
    {
        if ($_POST['occasion_load'] == 0) {
            $data = array(
                'occasion_text' => request()->input('occasion_name'),
                'occasion_priority' => 0,
                'occasion_parent_id' => 0
            );

        } else {
            $data = array(
                'occasion_text' => request()->input('occasion_name'),
                'occasion_priority' => 0,
                'occasion_parent_id' => $_POST['occasion_load']
            );

        }

        occasion_md::add_occasion($data);
        $request->session()->flash('alert-success', 'با موفقیت ثبت شد!');
        return redirect(route('Admin.Occasion'));

    }

    function delete_occasion(Request $request, $id)
    {
        occasion_md::delete_oca($id);
        $request->session()->flash('alert-success', 'با موفقیت حذف شد!');
        return redirect(route('Admin.Occasion'));
    }

    function load_parity($id, $priority)
    {
        $res = occasion_md::load_parity($id, $priority);
        echo json_encode($res);
    }

    function load_info()
    {
        $res = occasion_md::load_info();
        echo json_encode($res);
    }
}
