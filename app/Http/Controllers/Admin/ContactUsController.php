<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\ContactUs_md;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ContactUsController extends Controller
{
    function EditContact(Request $request)
    {
        $this->validate($request, [
            'EditTextContactUs' => 'required'
        ], [
            'EditTextContactUs.required' => 'لطفا متن را وارد کنید'
        ]);
        $id = request()->input('contact_id');
        $text = request()->input('EditTextContactUs');
        $res = ContactUs_md::EditContact($id, $text);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت ویرایش شد!');
            return redirect(route('Admin.AboutUs'));
        } else {
            $request->session()->flash('alert-success', ' ویرایش نشد!');
            return redirect(route('Admin.AboutUs'));
        }
    }
    function DateContact($id)
    {
        $res=ContactUs_md::GetData($id);
        echo json_encode($res);
    }
}
