<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Admin\List_user_md;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ListUserController extends Controller
{

    function index()
    {
        $list = List_user_md::load_users();
        return view('admin.ListUser', ['list' => $list]);
    }

    function DataUserAjax($id)
    {
        $res = List_user_md::DataUser($id);
        echo json_encode($res);
    }

    function UpData(Request $request)
    {
        $user_id = request()->input('hidden_id_user');
        $pass=request()->input('password');
        if (isset($pass)){
            $this->validate($request, [
                'name'=>'required',
                'family'=>'required',
                'email'=>'required',
                'phone'=>'required',
                'password'=>'min:6'
            ],[
                'name.required'=>'ورودی نباید خالی باشد',
                'family.required'=>'ورودی نباید خالی باشد',
                'email.required'=>'ورودی نباید خالی باشد',
                'phone.required'=>'ورودی نباید خالی باشد',
                'password.min'=>'کلمه عبور باید حداقل 6 حرف باشد',
            ]);
            $data = [
                'user_name' => request()->input('name'),
                'user_family' => request()->input('family'),
                'user_email' => request()->input('email'),
                'user_phone' => request()->input('phone'),
                'user_password' => md5(request()->input('password'))
            ];
        }else{
            $this->validate($request, [
                'name'=>'required',
                'family'=>'required',
                'email'=>'required',
                'phone'=>'required',

            ],[
                'name.required'=>'ورودی نباید خالی باشد',
                'family.required'=>'ورودی نباید خالی باشد',
                'email.required'=>'ورودی نباید خالی باشد',
                'phone.required'=>'ورودی نباید خالی باشد',

            ]);
            $data = [
                'user_name' => request()->input('name'),
                'user_family' => request()->input('family'),
                'user_email' => request()->input('email'),
                'user_phone' => request()->input('phone'),
            ];
          }
        $res = List_user_md::UPDataUser($data, $user_id);
        if ($res == true) {
            $request->session()->flash('alert-success', 'ویرایش انجام شد!');
            return redirect(route('Admin.ListUser'));
        } else {
            $request->session()->flash('alert-success', ' ویرایش انجام نشد!');
            return redirect(route('Admin.ListUser'));
        }
    }

    function DeleteUser(Request $request,$id)
    {
        $res = List_user_md::delete_user($id);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت حذف شد!');
            return redirect(route('Admin.ListUser'));
        } else {
            $request->session()->flash('alert-success', ' حذف نشد!');
            return redirect(route('Admin.ListUser'));
        }
    }

    function ChangeStatus($user_id, $status)
    {
        $res = List_user_md::change_status($user_id, $status);
        if ($res == true) {
            return redirect(route('Admin.ListUser'));
        } else {
            return redirect(route('Admin.ListUser'));
        }
    }
}
