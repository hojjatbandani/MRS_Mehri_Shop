<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Advertising_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\Request;

class ManagersAdvertisingController extends Controller
{
    function index()
    {
        $res = Advertising_md::load_Adv();
        return view('admin.ManagersAdvertising', compact('res'));
    }

    function edit_adv(Request $request)
    {
        $id = $request->adv_ajax;
        if ($request->image_file > 0) {
            $this->validate($request, [
                'image_file' => 'image|mimes:jpeg,png,jpg',
            ], [
                'image_file.image' => 'فایل ارسال شده باید عکس باشد',
                'image_file.mimes' => 'فرمت فایل ارسال شده باید jpeg,png,jpg باشد ',
            ]);
            $imageName = time() . '.' . $request->image_file->getClientOriginalExtension();
            $request->image_file->move(public_path('admin/uploads/Advertising'), $imageName);
            $data = [
                'Ads_address' => $request->position,
                'Ads_image' => $imageName,
            ];
        } else {
            $data = [
                'Ads_address' => $request->position
            ];
        }
        $res = Advertising_md::edit_adv($id, $data);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت ویرایش شد!');
            return redirect(route('Admin.Advertising'));
        } else {
            $request->session()->flash('alert-success', ' ویرایش نشد!');
            return redirect(route('Admin.Advertising'));
        }

    }

    function Advertising_delete(Request $request, $id = 0)
    {
        Advertising_md::delete_adv($id);
        $request->session()->flash('alert-success', 'حذف با موفقیت انجام شد!');
        return redirect(route('Admin.Advertising'));
    }

    function load_advertising(Request $request, $id = 0)
    {
        $res = Advertising_md::load_advertising($id);
        echo json_encode($res);
    }

    function add_adv(Request $request)
    {

        $this->validate($request, [
            'image_file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:6024',
        ]);
        $imageName = time() . '.' . $request->image_file->getClientOriginalExtension();
        $request->image_file->move(public_path('admin/uploads/Advertising'), $imageName);
        $data = array(
            'Ads_address' => $request['link_of_adv'],
            'Ads_position' => 1,
            'Ads_image' => $imageName,
        );
        $res = Advertising_md::add_adv($data);
        $request->session()->flash('alert-success', 'ثبت با موفقیت انجام شد!');
        return redirect(route('Admin.Advertising'));

    }
}
