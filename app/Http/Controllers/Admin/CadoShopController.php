<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\CadoShop_md;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CadoShopController extends Controller
{
    function EditCadoShop(Request $request)
    {
        $this->validate($request, [
            'EditTextCadoUs' => 'required'
        ], [
            'EditTextCadoUs.required' => 'لطفا متن را وارد کنید'
        ]);
        $id = request()->input('cado_id');
        $text = request()->input('EditTextCadoUs');
        $res = CadoShop_md::EditCadoShop($id, $text);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت ویرایش شد!');
            return redirect(route('Admin.AboutUs'));
        } else {
            $request->session()->flash('alert-success', ' ویرایش نشد!');
            return redirect(route('Admin.AboutUs'));
        }
    }
    function DataCadoShop($id)
    {
        $res=CadoShop_md::GetData($id);
        echo json_encode($res);
    }
}
