<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\category_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ManagersCategoryController extends Controller
{
    function index()
    {
        $cat = category_md::load_cat();
        $category = category_md::load_category();
        return view('admin.ManagersCategory', compact('cat', 'category'));
    }
    function CategoryData($id)
    {
        $res = category_md::CategoryData($id);
        echo json_encode($res);
    }
    function AgeCategory_priority(Request $request)
    {
        $res = category_md::CategoryData($id);
        echo json_encode($res);
    }
    function EditCat(Request $request){
        $this->validate($request, [
            'EditTextCat' => 'required'
        ], [
            'EditTextCat.required' => 'لطفا نام دسته را وارد کنید'
        ]);
        $id = request()->input('EditId');
        if ($request->image_file_edit > 0) {
            $this->validate($request, [
                'image_file_edit' => 'image|mimes:jpeg,png,jpg',
            ], [
                'image_file_edit.image' => 'فایل ارسال شده باید عکس باشد',
                'image_file_edit.mimes' => 'فرمت فایل ارسال شده باید jpeg,png,jpg باشد ',
            ]);
            $PictName = time() . '.' . $request->image_file_edit->getClientOriginalExtension();
            $request->image_file_edit->move(public_path('uploads/category/'), $PictName);

        $data =[
            'cat_name'=>request()->input('EditTextCat'),
            'image'=>$PictName,
        ];
        }else{
            $data=[
                'cat_name'=>request()->input('EditTextCat')
            ];
        }
        $res = category_md::EditCat($id, $data);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت ویرایش شد!');
            return redirect(route('Admin.Category'));
        } else {
            $request->session()->flash('alert-success', ' ویرایش نشد!');
            return redirect(route('Admin.Category'));
        }
    }
    function Add_cat(Request $request)
    {

        if ($_POST['cat'] == 0) {
            $this->validate($request, [
                'image_file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            ]);
            $imageName = time() . '.' . $request->image_file->getClientOriginalExtension();
            $request->image_file->move(public_path('uploads/cat'), $imageName);
            $data = array(
                'cat_name' => request()->input('cat_name'),
                'image' => $imageName,
                'cat_parent_id' => 0

            );
        } else {
            $child = request()->input('cat_child');
            if (isset($child)) {

                $data = array(
                    'cat_name' => request()->input('cat_name'),
                    'image' => '',
                    'cat_parent_id' => request()->input('cat_child')
                );
            } else {
                $data = array(
                    'cat_name' => request()->input('cat_name'),
                    'image' => '',
                    'cat_parent_id' => request()->input('cat')
                );
            }


        }

        category_md::add_category($data);
        $request->session()->flash('alert-success', 'با موفقیت ثبت شد!');
        return redirect(route('Admin.Category'));
    }

    function delete(Request $request, $id)
    {
        category_md::del_category($id);
        $request->session()->flash('alert-success', 'با موفقیت حذف شد!');
        return redirect(route('Admin.Category'));
    }

    function load_child($id)
    {

        $child = category_md::load_child($id);
        echo json_encode($child);

    }
}
