<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\sentence_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ManagersTextCardController extends Controller
{
    function index()
    {
        $sen = sentence_md::load_sentence();
        return view('admin.ManagersTextCard', compact('sen'));
    }

    function Add_sentece(Request $request)
    {
        $data = array(
            'Sentences_title' => $request['sentence']
        );
        sentence_md::Add_sentece($data);
        $request->session()->flash('alert-success', 'ثبت با موفقیت انجام شد!');
        return redirect(route('Admin.TextCard'));
    }

    function TextCard_delete(Request $request, $id = 0)
    {
        sentence_md::TextCard_delete($id);
        $request->session()->flash('alert-success', 'حذف با موفقیت انجام شد!');
        return redirect(route('Admin.TextCard'));
    }

    function TextCard_edit($id = 0)
    {
        $res = sentence_md::load_Adv($id);
        echo json_encode($res);

    }

    function edit_sentece(Request $request)
    {
        sentence_md::TextCard_edit_modal($request);
        $request->session()->flash('alert-success', 'ویرایش با موفقیت انجام شد!');
        return redirect(route('Admin.TextCard'));
    }
}
