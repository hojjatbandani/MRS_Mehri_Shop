<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\List_transaction_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ListTransactionController extends Controller
{
    function index()
    {
        $list=List_transaction_md::load_transaction();
        return view('admin.ListTransaction',compact('list'));
    }
}
