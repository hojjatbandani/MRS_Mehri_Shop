<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\ManagersComments_md;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ManagersCommentController extends Controller
{
    function index()
    {
        $res = ManagersComments_md::LoadComments();
        return view('admin.ManagersComment', compact('res'));
    }

    function ChangeComment($id, $status)
    {
        ManagersComments_md::ChangeComment($id, $status);
        return redirect(route('Admin.Comment'));
    }

    function DeleteComment(Request $request,$id)
    {
       $res= ManagersComments_md::DeleteComment($id);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت حذف شد!');
            return redirect(route('Admin.Comment'));
        } else {
            $request->session()->flash('alert-success', ' حذف نشد!');
            return redirect(route('Admin.Comment'));
        }
    }
}
