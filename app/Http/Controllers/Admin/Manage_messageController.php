<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\message_md;
use App\Http\Requests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Manage_messageController extends Controller
{
    function index()
    {
        $user = message_md::load_user();
        return view('admin.Send_message', compact('user'));

    }

   public function mail(Request $request)
    {
        ini_set('max_execution_time', 300);
        $emails = $request['target'];
//        $emails = 'hojjat.bandani@gmail.com';
        $subject = $request['email_text'];
        $text = $request['email_subject'];
//        foreach ($emails as $eml) {
            $email = Mail::send('emails.email', ['data' => $text], function ($message) use ($text,$emails,$subject) {
                $message->from('soshajoon@gmail.com', $text);
                $message->to($emails)->subject($subject);
            });
//        }
        if ($email==1) {
            $request->session()->flash('alert-success', 'ایمیل با موفقیت ارسال شد.');
            return redirect(route('Admin.Manage_message'));
        }
       $request->session()->flash('alert-success', 'ایمیل با موفقیت ارسال شد.');
       return redirect(route('Admin.Manage_message'));
    }

    function send_sms(Request $request)
    {
      $res =  message_md::send_msg($request);
        if ($res) {
            $request->session()->flash('alert-success', 'پیامک با موفقیت ارسال شد.');
            return redirect(route('Admin.Manage_message'));
        }else{
            $request->session()->flash('alert-success', 'خطا در ارسال پیامک ...');
            return redirect(route('Admin.Manage_message'));
        }
    }
}