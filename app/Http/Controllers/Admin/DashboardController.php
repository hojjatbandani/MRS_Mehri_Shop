<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\dashboard_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class DashboardController extends Controller
{
    function index()
    {
        $count_user = dashboard_md::load_dashboard();
        $count_trans = dashboard_md::load_transaction();
        $count_cat = dashboard_md::load_cat();
        $count_order = dashboard_md::load_orders();

        return view('admin.Dashboard',compact('count_user','count_trans','count_cat','count_order'));
    }
}
