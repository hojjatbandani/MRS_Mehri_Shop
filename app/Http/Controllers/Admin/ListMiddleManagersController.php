<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\manager_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ListMiddleManagersController extends Controller
{
    function index()
    {
        $load = manager_md::load_manager();
        return view('admin.ListMiddleManagers', compact('load'));
    }

    function create_manager(Request $request)
    {
        manager_md::Add_manager($request);
        $request->session()->flash('alert-success', 'ثبت با موفقیت انجام شد!');
        return redirect(route('Admin.MiddleManagers'));

    }

    function load_info($id)
    {
        $info = manager_md::load_info($id);
        echo json_encode($info);
    }

    function MiddleManagers_edit(Request $request)
    {
        manager_md::edit_manager($request);
        $request->session()->flash('alert-success', 'ویرایش با موفقیت انجام شد!');
        return redirect(route('Admin.MiddleManagers'));
    }

    function change_pass_manager(Request $request, $user_id, $user_pass)
    {
        $pass = manager_md::change_pass($user_id, $user_pass);
        $request->session()->flash('alert-success', 'ویرایش با موفقیت انجام شد!');
        return redirect(route('Admin.MiddleManagers'));
    }

    function manager_delete(Request $request, $id)
    {
        $del = manager_md::delete_manager($id);
        $request->session()->flash('alert-success', 'حذف با موفقیت انجام شد!');
        return redirect(route('Admin.MiddleManagers'));
    }
}
