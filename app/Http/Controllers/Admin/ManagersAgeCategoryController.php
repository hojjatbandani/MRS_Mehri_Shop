<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\age_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ManagersAgeCategoryController extends Controller
{
    function index()
    {

        $res = age_md::load_age();
        return view('admin.ManagersAgeCategory',compact('res'));
    }

    function age_delete(Request $request,$id = 0)
    {
        age_md::delete_age($id);
        $request->session()->flash('alert-success', 'حذف با موفقیت انجام شد!');
        return redirect(route('Admin.AgeCategory'));
    }

    function Add_age(Request $request)
    {
        $data = [
            'YearGroup_title' => request()->input('age_group'),
        ];

        age_md::add_age($data);
        $request->session()->flash('alert-success', 'ثبت با موفقیت انجام شد!');
        return redirect(route('Admin.AgeCategory'));

    }

    function AgeEdit(Request $request){
        $this->validate($request, [
            'EditTextAge' => 'required'
        ], [
            'EditTextAge.required' => 'لطفا نام گروه را وارد کنید'
        ]);
        $id = request()->input('EditId');
       $data=[
           'YearGroup_title'=>$request->EditTextAge
       ];
        $res = age_md::EditCat($id, $data);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت ویرایش شد!');
            return redirect(route('Admin.AgeCategory'));
        } else {
            $request->session()->flash('alert-success', ' ویرایش نشد!');
            return redirect(route('Admin.AgeCategory'));
        }
    }

    function AgeData($id)
    {
        $res = age_md::AgeData($id);
        echo json_encode($res);
    }

}
