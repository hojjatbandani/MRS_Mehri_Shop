<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\ManagersCards_md;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ManagersCardsController extends Controller
{
    function index()
    {
        $Cat = ManagersCards_md::LoadSelectCat();
        $Oca = ManagersCards_md::LoadSelectOca();
        $YG = ManagersCards_md::LoadSelectYG();
        $list = ManagersCards_md::load_Cards();
        return view('admin.ManagersCards', compact('Cat', 'Oca', 'YG', 'list'));
    }

    function AddCard(Request $request)
    {
        $this->validate($request, [
            'pic_front' => 'required|image|mimes:jpeg,png,jpg',
            'pic_behind' => 'required|image|mimes:jpeg,png,jpg'
        ], [
            'pic_front.required' => 'لطفا عکس کارت را ارسال کنید ',
            'pic_behind.required' => 'لطفا عکس پشت کارت را ارسال کنید ',
            'pic_front.image' => 'فایل ارسال شده باید عکس باشد',
            'pic_behind.image' => 'فایل ارسال شده باید عکس باشد ',
            'pic_front.mimes' => 'فرمت فایل ارسال شده باید jpeg,png,jpg باشد ',
            'pic_behind.mimes' => ' فرمت فایل ارسال شده باید jpeg,png,jpg باشد ',
        ]);
        $PicFrontName = time() . '.' . $request->pic_front->getClientOriginalExtension();
        $PicBehindName = time() . '.' . $request->pic_behind->getClientOriginalExtension();
        $request->pic_front->move(public_path('uploads/cards/front'), $PicFrontName);
        $request->pic_behind->move(public_path('uploads/cards/behind'), $PicBehindName);
        if ( request()->input('season')==0){
            $data = array(
                'card_occasion' => request()->input('occasion'),
                'card_category' => request()->input('category'),
                'card_age_category' => request()->input('AgeCategory'),
                'card_sexuality' => request()->input('Sex'),
                'card_title' => request()->input('TextCard'),
                'card_front_image' => $PicFrontName,
                'card_behind_image' => $PicBehindName,
            );
        }else{
            $data = array(
                'card_occasion' => request()->input('occasion'),
                'card_category' => request()->input('category'),
                'card_age_category' => request()->input('AgeCategory'),
                'card_sexuality' => request()->input('Sex'),
                'card_title' => request()->input('TextCard'),
                'card_season' => request()->input('season'),
                'card_front_image' => $PicFrontName,
                'card_behind_image' => $PicBehindName,
            );
        }

        $res = ManagersCards_md::InsertCard($data);
        if ($res == 'true') {
            $request->session()->flash('alert-success', 'با موفقیت ثبت شد!');
            return redirect(route('Admin.Cards'));
        } else {
            $request->session()->flash('alert-success', ' ثبت نشد!');
            return redirect(route('Admin.Cards'));
        }
    }

    function DataCardAjax($id)
    {
        $res = ManagersCards_md::DataCard($id);
        echo json_encode($res);
    }

    function UpData(Request $request)
    {
        if ( request()->input('season_edit')==0){
            if ($request->pic_front_edit > 0) {
                $this->validate($request, [
                    'pic_front_edit' => 'image|mimes:jpeg,png,jpg',
                ], [
                    'pic_front_edit.image' => 'فایل ارسال شده باید عکس باشد',
                    'pic_front_edit.mimes' => 'فرمت فایل ارسال شده باید jpeg,png,jpg باشد ',
                ]);
                $PicFrontName = time() . '.' . $request->pic_front_edit->getClientOriginalExtension();
                $request->pic_front_edit->move(public_path('uploads/cards/front'), $PicFrontName);
                $card_id = request()->input('Card_id');
                $data = array(
                    'card_occasion' => request()->input('occasion_edit'),
                    'card_category' => request()->input('category_edit'),
                    'card_age_category' => request()->input('AgeCategory_edit'),
                    'card_sexuality' => request()->input('Sex_edit'),
                    'card_title' => request()->input('TextCard_edit'),
                    'card_front_image' => $PicFrontName);
                $res = ManagersCards_md::UPDataCard($data, $card_id);
                if ($res == true) {
                    $request->session()->flash('alert-success', 'ویرایش انجام شد!');
                    return redirect(route('Admin.Cards'));
                } else {
                    $request->session()->flash('alert-success', ' ویرایش انجام نشد!');
                    return redirect(route('Admin.Cards'));
                }
            }
            elseif ($request->pic_behind_edit > 0) {
                $this->validate($request, [
                    'pic_behind_edit' => 'image|mimes:jpeg,png,jpg'
                ], [
                    'pic_behind_edit.image' => 'فایل ارسال شده باید عکس باشد ',
                    'pic_behind_edit.mimes' => ' فرمت فایل ارسال شده باید jpeg,png,jpg باشد '
                ]);
                $PicBehindName = time() . '.' . $request->pic_behind_edit->getClientOriginalExtension();
                $request->pic_behind_edit->move(public_path('uploads/cards/behind'), $PicBehindName);
                $card_id = request()->input('Card_id');
                $data = array(
                    'card_occasion' => request()->input('occasion_edit'),
                    'card_category' => request()->input('category_edit'),
                    'card_age_category' => request()->input('AgeCategory_edit'),
                    'card_sexuality' => request()->input('Sex_edit'),
                    'card_title' => request()->input('TextCard_edit'),
                    'card_behind_image' => $PicBehindName,
                );
                $res = ManagersCards_md::UPDataCard($data, $card_id);
                if ($res == true) {
                    $request->session()->flash('alert-success', 'ویرایش انجام شد!');
                    return redirect(route('Admin.Cards'));
                } else {
                    $request->session()->flash('alert-success', ' ویرایش انجام نشد!');
                    return redirect(route('Admin.Cards'));
                }
            }
            else{
                $card_id = request()->input('Card_id');
                $data = array(
                    'card_occasion' => request()->input('occasion_edit'),
                    'card_category' => request()->input('category_edit'),
                    'card_age_category' => request()->input('AgeCategory_edit'),
                    'card_sexuality' => request()->input('Sex_edit'),
                    'card_title' => request()->input('TextCard_edit')
                );
                $res = ManagersCards_md::UPDataCard($data, $card_id);
                if ($res == true) {
                    $request->session()->flash('alert-success', 'ویرایش انجام شد!');
                    return redirect(route('Admin.Cards'));
                } else {
                    $request->session()->flash('alert-success', ' ویرایش انجام نشد!');
                    return redirect(route('Admin.Cards'));
                }
            }
        }else{
            if ($request->pic_front_edit > 0) {
                $this->validate($request, [
                    'pic_front_edit' => 'image|mimes:jpeg,png,jpg',
                ], [
                    'pic_front_edit.image' => 'فایل ارسال شده باید عکس باشد',
                    'pic_front_edit.mimes' => 'فرمت فایل ارسال شده باید jpeg,png,jpg باشد ',
                ]);
                $PicFrontName = time() . '.' . $request->pic_front_edit->getClientOriginalExtension();
                $request->pic_front_edit->move(public_path('uploads/cards/front'), $PicFrontName);
                $card_id = request()->input('Card_id');
                $data = array(
                    'card_occasion' => request()->input('occasion_edit'),
                    'card_category' => request()->input('category_edit'),
                    'card_age_category' => request()->input('AgeCategory_edit'),
                    'card_sexuality' => request()->input('Sex_edit'),
                    'card_season' =>  request()->input('season_edit'),
                    'card_title' => request()->input('TextCard_edit'),
                    'card_front_image' => $PicFrontName);
                $res = ManagersCards_md::UPDataCard($data, $card_id);
                if ($res == true) {
                    $request->session()->flash('alert-success', 'ویرایش انجام شد!');
                    return redirect(route('Admin.Cards'));
                } else {
                    $request->session()->flash('alert-success', ' ویرایش انجام نشد!');
                    return redirect(route('Admin.Cards'));
                }
            }
            elseif ($request->pic_behind_edit > 0) {
                $this->validate($request, [
                    'pic_behind_edit' => 'image|mimes:jpeg,png,jpg'
                ], [
                    'pic_behind_edit.image' => 'فایل ارسال شده باید عکس باشد ',
                    'pic_behind_edit.mimes' => ' فرمت فایل ارسال شده باید jpeg,png,jpg باشد '
                ]);
                $PicBehindName = time() . '.' . $request->pic_behind_edit->getClientOriginalExtension();
                $request->pic_behind_edit->move(public_path('uploads/cards/behind'), $PicBehindName);
                $card_id = request()->input('Card_id');
                $data = array(
                    'card_occasion' => request()->input('occasion_edit'),
                    'card_category' => request()->input('category_edit'),
                    'card_age_category' => request()->input('AgeCategory_edit'),
                    'card_sexuality' => request()->input('Sex_edit'),
                    'card_season' =>  request()->input('season_edit'),
                    'card_title' => request()->input('TextCard_edit'),
                    'card_behind_image' => $PicBehindName,
                );
                $res = ManagersCards_md::UPDataCard($data, $card_id);
                if ($res == true) {
                    $request->session()->flash('alert-success', 'ویرایش انجام شد!');
                    return redirect(route('Admin.Cards'));
                } else {
                    $request->session()->flash('alert-success', ' ویرایش انجام نشد!');
                    return redirect(route('Admin.Cards'));
                }
            }
            else{
                $card_id = request()->input('Card_id');
                $data = array(
                    'card_occasion' => request()->input('occasion_edit'),
                    'card_category' => request()->input('category_edit'),
                    'card_age_category' => request()->input('AgeCategory_edit'),
                    'card_sexuality' => request()->input('Sex_edit'),
                    'card_season' =>  request()->input('season_edit'),
                    'card_title' => request()->input('TextCard_edit')

                );
                $res = ManagersCards_md::UPDataCard($data, $card_id);
                if ($res == true) {
                    $request->session()->flash('alert-success', 'ویرایش انجام شد!');
                    return redirect(route('Admin.Cards'));
                } else {
                    $request->session()->flash('alert-success', ' ویرایش انجام نشد!');
                    return redirect(route('Admin.Cards'));
                }
            }
        }
    }

    function DeleteCard(Request $request, $id)
    {
        $res = ManagersCards_md::DeleteCard($id);
        if ($res == true) {
            $request->session()->flash('alert-success', 'با موفقیت حذف شد!');
            return redirect(route('Admin.Cards'));
        } else {
            $request->session()->flash('alert-success', ' حذف نشد!');
            return redirect(route('Admin.Cards'));
        }
    }

}
