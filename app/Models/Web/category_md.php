<?php

namespace App\Models\Web;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class category_md extends Model
{
    public static function Load_category()
    {

        $res = DB::table('category')->get();
        $str = '';
        $i = 0;
        foreach ($res as $cat) {
            $res1 = DB::table('category')->where('cat_parent_id', $cat->cat_id)->get();
            if($i==0) {
                $str .= "<div class='col-md-4 col-sm-4 col-xs-12 col-xxs-12'>
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img1'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('custom') . "' class='ptitle'>" . $cat->cat_name . "</a>";
            }else if($i==1) {
                $str .= "<div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                            <div class='ge-img2'></div>
                            <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('custom') ."' class='ptitle'>".$cat->cat_name ."</a>";
            }else if($i==2) {
                $str .= " <div class='col-md-4 col-sm-4 col-xs-6 col-xxs-12 zpad grid-vertical-rectangle'><div>
                        <div class='gvr-img1'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('custom') ."' class='ptitle'>".$cat->cat_name ."</a>";
            }else if($i==3) {
                $str .= " <div class='col-md-4 col-sm-4 col-xs-12 col-xxs-12'>
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img3'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('custom') ."' class='ptitle'>".$cat->cat_name ."</a>";
            }else if($i==4)  {
                $str .= "
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img4'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('custom') ."' class='ptitle'>".$cat->cat_name ."</a>";
            }
            if($res1) {
                $str .= "<ul>";
                foreach ($res1 as $cat1) {
                    $str .= "<li><a href='" . route('custom') . "'>" . $cat1->cat_name . "</a>";
                    $res2 = DB::table('category')->where('cat_parent_id', $cat1->cat_id)->get();
                    if($res2){
                        $str.="<ul>";
                        foreach ($res2 as $cat2) {
                            $str .= "<li><a href='" . route('custom') . "'>" . $cat2->cat_name . "</a></li>";
                        }
                        $str.="</ul></li>";
                    }else{
                        $str.="</li>";
                    }
                }
                $str .= "</ul></div></div>";
            }else{
                switch ($i) {
                    case 0:
                        $str .= "</div></div>";
                        break;
                    case 1:
                        $str .= "</div></div></div>";
                        break;
                    case 2:
                        $str .= "</div></div>";
                        break;
                    case 3:
                        $str .= "</div></div>";
                        break;
                    case 4:
                        $str .= "</div></div></div>";
                        break;
                }
            }
            switch ($i) {
                case 0:
                    $str .= "</div>";
                    break;
                case 1:
                    $str .= "</div></div>";
                    break;
                case 2:
                    $str .= "</div></div>";
                    break;
                case 3:
                    $str .= "</div>";
                    break;
                case 4:
                    $str .= "</div></div></div>";
                    break;
            }
            if($i==4){ $i=0;}else{$i++;}

        }
        return $str;
    }


    public static function Load_category_in()
    {
        $res = DB::table('category')->get();
        $str = '';
        $i = 0;
        foreach ($res as $cat) {
            $res1 = DB::table('category')->where('cat_parent_id', $cat->cat_id)->get();
                if($i==0) {
                    $str .= "<div class='col-md-4 col-sm-4 col-xs-12 col-xxs-12'>
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img1'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('occasion_about') . "' class='ptitle'>" . $cat->cat_name . "</a>";
                }else if($i==1) {
                    $str .= "<div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                            <div class='ge-img2'></div>
                            <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('occasion_about') ."' class='ptitle'>".$cat->cat_name ."</a>";
                }else if($i==2) {
                    $str .= " <div class='col-md-4 col-sm-4 col-xs-6 col-xxs-12 zpad grid-vertical-rectangle'><div>
                        <div class='gvr-img1'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('occasion_about') ."' class='ptitle'>".$cat->cat_name ."</a>";
                }else if($i==3) {
                    $str .= " <div class='col-md-4 col-sm-4 col-xs-12 col-xxs-12'>
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img3'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('occasion_about') ."' class='ptitle'>".$cat->cat_name ."</a>";
                }else if($i==4)  {
                    $str .= "
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img4'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='" . route('occasion_about') ."' class='ptitle'>".$cat->cat_name ."</a>";
                }
            if($res1) {
                $str .= "<ul>";
                foreach ($res1 as $cat1) {
                    $str .= "<li><a href='" . route('occasion_about') . "'>" . $cat1->cat_name . "</a>";
                    $res2 = DB::table('category')->where('cat_parent_id', $cat1->cat_id)->get();
                    if($res2){
                        $str.="<ul>";
                        foreach ($res2 as $cat2) {
                            $str .= "<li><a href='" . route('occasion_about') . "'>" . $cat2->cat_name . "</a></li>";
                        }
                        $str.="</ul></li>";
                    }else{
                        $str.="</li>";
                    }
                }
                $str .= "</ul></div></div>";
            }else{
                switch ($i) {
                    case 0:
                        $str .= "</div></div>";
                        break;
                    case 1:
                        $str .= "</div></div></div>";
                        break;
                    case 2:
                        $str .= "</div></div>";
                        break;
                    case 3:
                        $str .= "</div></div>";
                        break;
                    case 4:
                        $str .= "</div></div></div>";
                        break;
                }
            }
            switch ($i) {
                case 0:
                    $str .= "</div>";
                    break;
                case 1:
                    $str .= "</div></div>";
                    break;
                case 2:
                    $str .= "</div></div>";
                    break;
                case 3:
                    $str .= "</div>";
                    break;
                case 4:
                    $str .= "</div></div></div>";
                    break;
            }
            if($i==4){ $i=0;}else{$i++;}

        }
        return $str;
    }

    public static function Load_category_empty()
    {
        $res = DB::table('category')->get();
        $str = '';
        $i = 0;
        foreach ($res as $cat) {
            $res1 = DB::table('category')->where('cat_parent_id', $cat->cat_id)->get();
            if($i==0) {
                $str .= "<div class='col-md-4 col-sm-4 col-xs-12 col-xxs-12'>
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img1'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='#' class='ptitle'>" . $cat->cat_name . "</a>";
            }else if($i==1) {
                $str .= "<div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                            <div class='ge-img2'></div>
                            <div class='overlay'>
                            <div class='text'>
                                <a href='#' class='ptitle'>".$cat->cat_name ."</a>";
            }else if($i==2) {
                $str .= " <div class='col-md-4 col-sm-4 col-xs-6 col-xxs-12 zpad grid-vertical-rectangle'><div>
                        <div class='gvr-img1'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='#' class='ptitle'>".$cat->cat_name ."</a>";
            }else if($i==3) {
                $str .= " <div class='col-md-4 col-sm-4 col-xs-12 col-xxs-12'>
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img3'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='#' class='ptitle'>".$cat->cat_name ."</a>";
            }else if($i==4)  {
                $str .= "
                        <div class='col-md-12 col-sm-12 col-xs-6 col-xxs-12 zpad grid-Equal'>
                        <div class='ge-img4'></div>
                        <div class='overlay'>
                            <div class='text'>
                                <a href='#' class='ptitle'>".$cat->cat_name ."</a>";
            }
            if($res1) {
                $str .= "<ul>";
                foreach ($res1 as $cat1) {
                    $str .= "<li><a href='#'>" . $cat1->cat_name . "</a>";
                    $res2 = DB::table('category')->where('cat_parent_id', $cat1->cat_id)->get();
                    if($res2){
                        $str.="<ul>";
                        foreach ($res2 as $cat2) {
                            $str .= "<li><a href='#'>" . $cat2->cat_name . "</a></li>";
                        }
                        $str.="</ul></li>";
                    }else{
                        $str.="</li>";
                    }
                }
                $str .= "</ul></div></div>";
            }else{
                switch ($i) {
                    case 0:
                        $str .= "</div></div>";
                        break;
                    case 1:
                        $str .= "</div></div></div>";
                        break;
                    case 2:
                        $str .= "</div></div>";
                        break;
                    case 3:
                        $str .= "</div></div>";
                        break;
                    case 4:
                        $str .= "</div></div></div>";
                        break;
                }
            }
            switch ($i) {
                case 0:
                    $str .= "</div>";
                    break;
                case 1:
                    $str .= "</div></div>";
                    break;
                case 2:
                    $str .= "</div></div>";
                    break;
                case 3:
                    $str .= "</div>";
                    break;
                case 4:
                    $str .= "</div></div></div>";
                    break;
            }
            if($i==4){ $i=0;}else{$i++;}

        }
        return $str;
    }


    public static function load_eid ()
    {
        $res = DB::table('eyd')->get();
        return $res;
    }

}
