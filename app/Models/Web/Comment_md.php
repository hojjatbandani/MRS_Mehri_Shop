<?php

namespace App\Models\Web;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Comment_md extends Model
{
    public static function load_com($id)
    {
        $category=DB::table('category')->where('cat_id',$id)->get();
        $data=array();
        if(count($category)>0){
            foreach ($category as $cat)
            $data=array(
                'image'=>$cat->image,
                'cat_name'=>$cat->cat_name,
            );
        }
return $data;
    }
}
