<?php

namespace App\Models\Web;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Login_md extends Model
{
    public static function login($data)
    {

        $res = DB::table('users')->where('user_email', $data['user_email'])->where('user_password', $data['user_password'])->get();
        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }
}
