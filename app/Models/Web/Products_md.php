<?php

namespace App\Models\Web;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Products_md extends Model
{
    public static function Load_pro()
    {
        $res = DB::table('category')->where('cat_parent_id','0')->get();
        $str ="";
        $rot=route('nazarat');
        foreach ($res as $count) {
            $res1 = DB::table('category')->where('cat_parent_id', $count->cat_id)->get();
            $str .= "  <div class='row'>
                            <div class='col-md-6 col-sm-6 col-xs-6 col-xxs-12 zpad'>
                                <img class='img-responsive' src='uploads/category/{$count->image}'/>
                            </div>
                            <div class='col-md-6 col-sm-6 col-xs-6 col-xxs-12 zpad'>
                                <h2><a href='".route('custom').'/'.$id=$count->cat_id."'>{$count->cat_name}</a></h2>
                                <ul class='ul-defualt'>";
            foreach ($res1 as $count1) {
                $str .= "<li><a href='".route('custom').'/'.$id=$count1->cat_id."'>{$count1->cat_name}</a>";
                $res2 = DB::table('category')->where('cat_parent_id', $count1->cat_id)->get();
                if (count($res2) > 0) {
                    $str.="<ul class='ul-defualt'>";
                    foreach ($res2 as $count2) {
                        $str .= "<li><a href='".route('custom').'/'.$id=$count2->cat_id."'>{$count2->cat_name}</a></li>";
                    }
                    $str.="</ul>";
                }
                $str.="</li>";
            }
            $str.="</ul></div><div class=\"bottom-links\">
                                <a href=".route('nazarat').'/'.$id=$count->cat_id." class=\"btn-2\">مشاهده جزئیات و نظرات</a>
                            </div></div>";
        }
        return $str;
    }
}
