<?php

namespace App\Models\Web;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Home_md extends Model
{
    public static function about_us()
    {

        $res = DB::table('about_us')->get();
        if (count($res) > 0) {
            return $res;
        }
    }

    public static function load_ostan()
    {
        $res = DB::table('province')->get();
        $str = "";
        foreach ($res as $row) {
            $str.="<option value='".$row->id."'>".$row->name."</option>";
        }
        return $str;
    }

    public static function load_code($id)
    {
        $res = DB::table('codeoff')->where('codeoff_code',$id)->get();
        $str = 0;
        foreach ($res as $row) {
            $str=$row->codeoff_percent;
        }
        return $str;
    }

    public static function load_city($id)
    {
        $res = DB::table('city')->where('province',$id)->get();
        $str = "";
        foreach ($res as $row) {
            $str.="<option value='".$row->id."'>".$row->name."</option>";
        }
        return $str;
    }

    public static function savecity($id)
    {
        $res = DB::table('city')->where('id',$id)->get();
        $str = "";
        foreach ($res as $row) {
            session(['city_price' => $row->send_price]);
        }
    }

    public static function saveKado($id)
    {
        session(['Kado_Price' => $id]);
    }

    public static function savecharge($id)
    {
        session(['savecharge' => $id]);
    }

    public static function load_occasion()
    {
        $res = DB::table('occasion')->orderBy('occasion_priority', 'asc')->get();
        $str = array();
        $time = 0.25;
        foreach ($res as $occasion) {

            array_push($str, array(
                'occasion_id' => $occasion->occasion_id,
                'occasion_text' => $occasion->occasion_text,
                'occasion_parent_id' => $occasion->occasion_parent_id
            ));

        }
        return $str;

    }

    public static function load_cards()
    {
        $res = DB::table('cards')->limit('5')->get();
        $str = "";
        if (count($res) > 0) {
            foreach ($res as $count) {
                $str .= "
                <div class=\"frame\" data-mightyslider=\"
				        width: 1585,
				        height: 500
				    \">
                    <!-- SLIDEELEMENT -->
                    <div class=\"slide_element\">

                        <!-- SLIDES -->
                        <div class=\"slide prev_2\" data-mightyslider=\"
				                cover: 'img/challenge_1.jpg',
				                thumbnail: 'img/challenge_1.jpg',
				                link: {
				                    url: '#',
				                    target: '_blank'
				                }
				            \"></div>
                        <div class=\"slide prev_1\" data-mightyslider=\"
				                cover: 'img/11.jpg',
				                thumbnail: 'img/11.jpg',
				                link: {
				                    url: '#',
				                    target: '_blank'
				                }
				            \"></div>
                        <div class=\"slide active\" data-mightyslider=\"
				                cover: 'img/11.jpg',
				                thumbnail: 'img/11.jpg',
				                link: {
				                    url: '#',
				                    target: '_blank'
				                }
				            \">
                            <!-- LAYER -->
                            <div class=\"mSCaption\">
                                You can use direct video url<br />for full-sized videos & covers
                            </div>
                        </div>
                        <div class=\"slide next_1\" data-mightyslider=\"
				                cover: 'img/11.jpg',
				                thumbnail: 'img/11.jpg',
				                link: {
				                    url: '#',
				                    target: '_blank'
				                }
				            \"></div>
                        <div class=\"slide next_2\" data-mightyslider=\"
				                cover: 'img/11.jpg',
				                thumbnail: 'img/11.jpg',
				                link: {
				                    url: '#',
				                    target: '_blank'
				                }
				            \"></div>
                        <div class=\"slide\" data-mightyslider=\"
				                cover: 'img/11.jpg',
				                thumbnail: 'img/11.jpg',
				                link: {
				                    url: '#',
				                    target: '_blank'
				                }
				            \"></div>
                        <!-- END OF SLIDES -->
                    </div>
                    <!-- END OF SLIDEELEMENT -->
                    <!-- ARROW BUTTONS -->
                    <a class=\"mSButtons mSPrev\"></a>
                    <a class=\"mSButtons mSNext\"></a>
                </div>
                <!-- END OF FRAME -->
                <!-- SLIDER TIMER -->
                <canvas id=\"progress\" width=\"160\" height=\"160\"></canvas>
                <!-- END OF SLIDER TIMER -->
                <!-- THUMBNAILS -->
                <div id=\"thumbnails\">
                    <div>
                        <ul></ul>
                    </div>
                </div>

        ";
            }
        }
        return $str;
    }

    public static function load_age()
    {
        $res = DB::table('yeargroup')->get();
        return $res;
    }
}
