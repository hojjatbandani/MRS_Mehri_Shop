<?php

namespace App\Models\Web;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Register_md extends Model
{
    public static function insert_user($data)
    {
        $check = DB::table('users')->where('user_email', $data['user_email'])->get();
        if(count($check)>0){
            return 'false1';
        }else{

            $res = DB::table('users')->insert(
                $data
            );
            if (is_null($res)) {
                return 'false';
            } else {
                return 'true';
            }
        }

    }
}
