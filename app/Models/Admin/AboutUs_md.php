<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class AboutUs_md extends Model
{
    public static function loadAbout()
    {
        $res = DB::table('about_us')->get();
        $data = array();
        if (count($res) > 0) {
            $data = array(
                'about_id' => $res[0]->about_id,
                'about_text' => $res[0]->about_text
            );
        }
        return $data;
    }

    public static function EditAbout($id, $text)
    {
        $res = DB::table('about_us')
            ->where('about_id', $id)
            ->update(['about_text' => $text]);
        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function GetData($id)
    {
        $res = DB::table('about_us')->where('about_id', $id)->get();
          $data= array(
                'about_id' => $res[0]->about_id,
                'about_text' => $res[0]->about_text
            );
        return $data;
    }
}
