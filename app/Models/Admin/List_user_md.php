<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class List_user_md extends Model
{
    public static function load_users()
    {
        $list = DB::table('users')->Paginate(10);
        $data = array();
        foreach ($list as $count) {
            array_push($data, array(
                'user_id' => $count->user_id,
                'user_name' => $count->user_name,
                'user_family' => $count->user_family,
                'user_email' => $count->user_email,
                'user_status' => $count->user_status,
                'user_phone' => $count->user_phone,
            ));
        }
        return $data;
    }

    public static function DataUser($user_id)
    {
        $res = DB::table('users')->where('user_id', $user_id)->get();
            $data = array(
                'user_id' => $res[0]->user_id,
                'user_name' => $res[0]->user_name,
                'user_family' => $res[0]->user_family,
                'user_email' => $res[0]->user_email,
                'user_phone' => $res[0]->user_phone,
            );
        return $data;
    }
    public static function UPDataUser($data,$user_id)
    {
        $res = DB::table('users')->where('user_id', $user_id)->update($data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    public static function change_status($user_id,$status)
    {
        if ($status == '1') {
            $res = DB::table('users')->where('user_id', $user_id)->update(['user_status' => 'deactive']);
            if ($res) {
                return true;
            } else {
                return false;
            }
        } elseif ($status == '0') {
            $res = DB::table('users')->where('user_id', $user_id)->update(['user_status' => 'active']);
            if ($res) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function delete_user ($user_id)
    {
        $res = DB::table('users')->where('user_id', $user_id)->delete();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
}
