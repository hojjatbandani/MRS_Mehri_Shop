<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class SendPrice_md extends Model
{

    public static function load_code_off()
    {
        $query = DB::table('codeoff')->get();
        return $query;
    }

    public static function add_code_off($request)
    {
        $length = 10;
        $pool = '0123456789abcdefghijklmnyz';
        $code = substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
        $data = array(
            'codeoff_percent' => $request['code_off'],
            'codeoff_code' => $code,
        );
        $query = DB::table('codeoff')->insert($data);
        return $query;

    }

    public static function del_code_off($id)
    {
        $query = DB::table('codeoff')->where('codeoff_id', $id)->delete();
        return $query;
    }

    public static function edit_code_off($id)
    {
        $query = DB::table('codeoff')->where('codeoff_id', $id)->get();
        foreach ($query as $row) {
            $str = array(
                'codeoff_percent' => $row->codeoff_percent,
                'codeoff_id' => $row->codeoff_id
            );
        }
        return $str;
    }

    public static function code_off_edit($request)
    {
        $id = $request['code_off_ajax'];
        $data = array(
            'codeoff_percent'=>$request['code_off']
        );
        $query = DB::table('codeoff')->where('codeoff_id', $id)->update($data);
        return $query;

    }

}
