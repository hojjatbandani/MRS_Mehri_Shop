<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ManagersComments_md extends Model
{
    public static function LoadComments()
    {
        $query = DB::table('comment')
            ->join('users', 'users.user_id', '=', 'comment.user_id')
            ->join('category', 'category.cat_id', '=', 'comment.cat_id')
            ->get();
        $data = array();
        if (count($query) > 0) {
            foreach ($query as $count) {
                array_push($data, array(
                    'comment_id' => $count->comment_id,
                    'user_name' => $count->user_name,
                    'user_family' => $count->user_family,
                    'cat_name' => $count->cat_name,
                    'comment_name' => $count->comment_name,
                    'comment_status' => $count->comment_status,
                ));
            }
        }
        return $data;
    }

    public static function ChangeComment($id, $status)
    {

        if ($status == '1') {
            $res = DB::table('comment')->where('comment_id', $id)->update(['comment_status' => 'deactive']);
            if ($res) {
                return true;
            } else {
                return false;
            }
        } elseif ($status == '0') {
            $res = DB::table('comment')->where('comment_id', $id)->update(['comment_status' => 'active']);
            if ($res) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function DeleteComment($id)
    {
        $res = DB::table('comment')->where('comment_id', $id)->delete();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
}
