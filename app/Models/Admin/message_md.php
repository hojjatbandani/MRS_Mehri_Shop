<?php

namespace App\Models\Admin;
use phplusir\smsir\Smsir;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;



class message_md extends Model
{

    public static function load_user()
    {
        $query = DB::table('users')->get();
        return $query;
    }

    public static function send_msg($request)
    {
        $numbers = $request['target'];
        $text = $request['sms_text'];
        $res = Smsir::send([$text], $numbers);
        return $res;
    }

    public static function del_code_off($id)
    {
        $query = DB::table('codeoff')->where('codeoff_id', $id)->delete();
        return $query;
    }

    public static function edit_code_off($id)
    {
        $query = DB::table('codeoff')->where('codeoff_id', $id)->get();
        foreach ($query as $row) {
            $str = array(
                'codeoff_percent' => $row->codeoff_percent,
                'codeoff_id' => $row->codeoff_id
            );
        }
        return $str;
    }

    public static function code_off_edit($request)
    {
        $id = $request['code_off_ajax'];
        $data = array(
            'codeoff_percent' => $request['code_off']
        );
        $query = DB::table('codeoff')->where('codeoff_id', $id)->update($data);
        return $query;

    }

}
