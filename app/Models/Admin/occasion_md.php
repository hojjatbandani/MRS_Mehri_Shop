<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class occasion_md extends Model
{

    public static function load_occasion()
    {
        $query = DB::table('occasion')->orderby('occasion_priority','ASC')->get();
        return $query;
    }

    public static function load_info()
    {
        $query = DB::table('occasion')->orderby('occasion_priority', 'ASC')->get();

        foreach ($query as $row){
            $str ="<tr><tddata-title = 'ردیف'></td>
            <td data-title = 'نام'></td>
            <tddata-title = 'اولویت بندی'><input type='number' class='input-form wide' value='' id='col_id_by_ajax_in_filenab' data-id='' ></td>
            </tr>";
        }
        return $query;
    }

    public static function add_occasion($data)
    {
        $res = DB::table('occasion')->insert($data);
        $id = DB::getPdo()->lastInsertId();
        $data = array(
            'occasion_priority' => $id
        );
        DB::table('occasion')->where('occasion_id', $id)->update($data);

        return $res;
    }


    public static function delete_oca($id)
    {
        $query = DB::table('occasion')->where('occasion_id', $id)->delete();
        return $query;

    }

    public static function load_parity($id,$priority)
    {
        $data = array(
            'occasion_priority'=>$priority
        );
        $res = DB::table('occasion')->where('occasion_id',$id)->update($data);

        return $res;

    }
//

//
//
//    public static function load_orders()
//    {
//        $query = DB::table('orders')->count('Orders_id');
//        return $query;
//    }
}
