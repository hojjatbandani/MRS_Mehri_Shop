<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class manager_md extends Model
{

    public static function Add_manager()
    {
        if (request()->input('dashboard_page') == 'on') {
            $dashboard = 'on';
        } else {
            $dashboard = 'off';
        }
        if (request()->input('list_user') == 'on') {
            $listUser = 'on';
        } else {
            $listUser = 'off';
        }
        if (request()->input('orders_list') == 'on') {
            $orders = 'on';
        } else {
            $orders = 'off';
        }
        if (request()->input('modir_miani') == 'on') {
            $manager = 'on';
        } else {
            $manager = 'off';
        }
        if (request()->input('manage_cat') == 'on') {
            $category = 'on';
        } else {
            $category = 'off';
        }
        if (request()->input('manage_card') == 'on') {
            $cards = 'on';
        } else {
            $cards = 'off';
        }
        if (request()->input('manage_occasion') == 'on') {
            $occasion = 'on';
        } else {
            $occasion = 'off';
        }
        if (request()->input('manage_age') == 'on') {
            $age = 'on';
        } else {
            $age = 'off';
        }
        if (request()->input('manage_Sentence') == 'on') {
            $text = 'on';
        } else {
            $text = 'off';
        }
        if (request()->input('manage_comment') == 'on') {
            $comment = 'on';
        } else {
            $comment = 'off';
        }
        if (request()->input('manage_adv') == 'on') {
            $advertising = 'on';
        } else {
            $advertising = 'off';
        }
        if (request()->input('manage_transaction') == 'on') {
            $trans = 'on';
        } else {
            $trans = 'off';
        }
        if (request()->input('manage_pages') == 'on') {
            $about = 'on';
        } else {
            $about = 'off';
        }
        $data = [
            'Admin_family' => request()->input('family'),
            'Admin_email' => request()->input('user_email'),
            'Admin_password' => bcrypt(request()->input('password')),
            'Admin_username' => request()->input('username'),
            'Admin_dashboard' => $dashboard,
            'Admin_listUser' => $listUser,
            'Admin_orders' => $orders,
            'Admin_middle_managers' => $manager,
            'Admin_category' => $category,
            'Admin_cards' => $cards,
            'Admin_occasion' => $occasion,
            'Admin_age' => $age,
            'Admin_textCard' => $text,
            'Admin_comment' => $comment,
            'Admin_advertising' => $advertising,
            'Admin_transaction' => $trans,
            'Admin_page_settings' => $about,

        ];

        $res = DB::table('admins')->insert($data);
        return $res;
    }

    public static function load_manager()
    {
        $query = DB::table('admins')->get();
        return $query;
    }

    public static function load_cat()
    {
        $query = DB::table('category')->where('cat_parent_id', 0)->count('cat_id');
        return $query;
    }

    public static function load_orders()
    {
        $query = DB::table('orders')->count('Orders_id');
        return $query;
    }

    public static function load_info($id)
    {
        $query = DB::table('admins')->where('Admin_id', $id)->get();
        foreach ($query as $row) {
            $data = array(
                'Admin_dashboard' => $row->Admin_dashboard,
                'Admin_listUser' => $row->Admin_listUser,
                'Admin_orders' => $row->Admin_orders,
                'Admin_middle_managers' => $row->Admin_middle_managers,
                'Admin_category' => $row->Admin_category,
                'Admin_cards' => $row->Admin_cards,
                'Admin_occasion' => $row->Admin_occasion,
                'Admin_age' => $row->Admin_age,
                'Admin_textCard' => $row->Admin_textCard,
                'Admin_comment' => $row->Admin_comment,
                'Admin_advertising' => $row->Admin_advertising,
                'Admin_transaction' => $row->Admin_transaction,
                'Admin_page_settings' => $row->Admin_page_settings,

            );

            return $data;
        }
    }

    public static function edit_manager($request)
    {
        if (request()->input('dashboard_page') == 'on') {
            $dashboard = 'on';
        } else {
            $dashboard = 'off';
        }
        if (request()->input('list_user') == 'on') {
            $listUser = 'on';
        } else {
            $listUser = 'off';
        }
        if (request()->input('orders_list') == 'on') {
            $orders = 'on';
        } else {
            $orders = 'off';
        }
        if (request()->input('modir_miani') == 'on') {
            $manager = 'on';
        } else {
            $manager = 'off';
        }
        if (request()->input('manage_cat') == 'on') {
            $category = 'on';
        } else {
            $category = 'off';
        }
        if (request()->input('manage_card') == 'on') {
            $cards = 'on';
        } else {
            $cards = 'off';
        }
        if (request()->input('manage_occasion') == 'on') {
            $occasion = 'on';
        } else {
            $occasion = 'off';
        }
        if (request()->input('manage_age') == 'on') {
            $age = 'on';
        } else {
            $age = 'off';
        }
        if (request()->input('manage_Sentence') == 'on') {
            $text = 'on';
        } else {
            $text = 'off';
        }
        if (request()->input('manage_comment') == 'on') {
            $comment = 'on';
        } else {
            $comment = 'off';
        }
        if (request()->input('manage_adv') == 'on') {
            $advertising = 'on';
        } else {
            $advertising = 'off';
        }
        if (request()->input('manage_transaction') == 'on') {
            $trans = 'on';
        } else {
            $trans = 'off';
        }
        if (request()->input('manage_pages') == 'on') {
            $about = 'on';
        } else {
            $about = 'off';
        }
        $data = [
            'Admin_dashboard' => $dashboard,
            'Admin_listUser' => $listUser,
            'Admin_orders' => $orders,
            'Admin_middle_managers' => $manager,
            'Admin_category' => $category,
            'Admin_cards' => $cards,
            'Admin_occasion' => $occasion,
            'Admin_age' => $age,
            'Admin_textCard' => $text,
            'Admin_comment' => $comment,
            'Admin_advertising' => $advertising,
            'Admin_transaction' => $trans,
            'Admin_page_settings' => $about,

        ];

        $id = $request['hidden_id'];

        $query = DB::table('admins')->where('Admin_id', $id)->update($data);
        return $query;
    }

    public static function change_pass($user_id, $user_pass)
    {
        $data = array(
            'Admin_password' => bcrypt($user_pass),
        );
        $query = DB::table('admins')->where('Admin_id', $user_id)->update($data);
        return $query;

    }

    public static function delete_manager($id)
    {
        $query = DB::table('admins')->where('Admin_id', $id)->delete();
        return $query;

    }
}
