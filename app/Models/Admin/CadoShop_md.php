<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class CadoShop_md extends Model
{
    public static function loadCadoShop()
    {
        $res = DB::table('cadoshop')->get();
        $data = array();
        if (count($res) > 0) {
            $data = array(
                'Cadoshop_id' => $res[0]->Cadoshop_id,
                'Cadoshop_text' => $res[0]->Cadoshop_text
            );
        }
        return $data;
    }

    public static function EditCadoShop($id, $text)
    {
        $res = DB::table('cadoshop')
            ->where('Cadoshop_id', $id)
            ->update(['Cadoshop_text' => $text]);
        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function GetData($id)
    {
        $res = DB::table('cadoshop')->where('Cadoshop_id', $id)->get();
          $data= array(
                'Cadoshop_id' => $res[0]->Cadoshop_id,
                'Cadoshop_text' => $res[0]->Cadoshop_text
            );
        return $data;
    }
}
