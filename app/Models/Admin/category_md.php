<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class category_md extends Model
{

    public static function load_cat()
    {
        $query = DB::table('category')->where('cat_parent_id', 0)->get();

        $str = '';
        foreach ($query as $cats) {
            $str .= "<option value=" . $cats->cat_id . ">" . $cats->cat_name . "</option>";
        }
        return $str;
    }

    public static function add_category($data)
    {


        $res = DB::table('category')->insert($data);
        return $res;
    }

    public static function CategoryData($id)
    {
        $res = DB::table('category')->where('cat_id', $id)
            ->get();
        $data=array(
            'cat_id' => $res[0]->cat_id,
            'image' => $res[0]->image,
            'cat_name' => $res[0]->cat_name,
            'cat_parent_id' => $res[0]->cat_parent_id ,
        );
        return $data;
    }

    public static function EditCat($id, $data)
    {
        $res = DB::table('category')
            ->where('cat_id', $id)
            ->update($data);
        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function load_category()
    {
        $res = DB::table('category')->get();
        return $res;
    }

    public static function del_category($id)
    {
        $query = DB::table('category')->where('cat_id', $id)->delete();

        return $query;
    }

    public static function load_child($id)
    {
        $query = DB::table('category')->where('cat_parent_id', $id)->get();
        $str = '';
        foreach ($query as $row) {
            $str .="<option value=".$row->cat_id.">".$row->cat_name."</option>";
        }
        return $str;
    }
//
//    public static function load_transaction()
//    {
//        $query = DB::table('transactions')->where('transaction_type',1)->sum('transaction_amount');
//        return $query;
//    }
//
//
//    public static function load_orders()
//    {
//        $query = DB::table('orders')->count('Orders_id');
//        return $query;
//    }
}
