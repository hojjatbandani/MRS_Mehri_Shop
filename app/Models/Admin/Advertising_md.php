<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Advertising_md extends Model
{

    public static function load_Adv()
    {
        $query = DB::table('ads')->get();
        return $query;
    }

    public static function add_adv($data)
    {

        $res = DB::table('ads')->insert($data);
        return $res;
    }

    public static function load_advertising($id)
    {
        $res = DB::table('ads')->where('Ads_id', $id)->get();
        foreach ($res as $row) {
            $adv = array(
                'adv_id' => $row->Ads_id,
                'Ads_position' => $row->Ads_position,
                'Ads_address' => $row->Ads_address,
                'Ads_image' => $row->Ads_image,

            );
        }
        return $adv;
    }

    public static function delete_adv($id)
    {
        $query = DB::table('ads')->where('Ads_id', $id)->delete();

        return $query;
    }


    public static function edit_adv($id,$data)
    {
        $query = DB::table('ads')->where('Ads_id', $id)->update($data);
        if (count($query) > 0) {
            return true;
        } else {
            return false;
        }

    }


    public
    static function load_child($id)
    {
        $query = DB::table('category')->where('cat_parent_id', $id)->get();
        $str = '';
        foreach ($query as $row) {
            $str .= "<option value=" . $row->cat_id . ">" . $row->cat_name . "</option>";
        }
        return $str;
    }

//
//    public static function load_transaction()
//    {
//        $query = DB::table('transactions')->where('transaction_type',1)->sum('transaction_amount');
//        return $query;
//    }
//
//
//    public static function load_orders()
//    {
//        $query = DB::table('orders')->count('Orders_id');
//        return $query;
//    }
}
