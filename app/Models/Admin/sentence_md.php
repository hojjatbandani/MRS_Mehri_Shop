<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class sentence_md extends Model
{

    public static function load_sentence()
    {
        $query = DB::table('sentences')->get();
        return $query;
    }

    public static function Add_sentece($data)
    {
        $res = DB::table('sentences')->insert($data);
        return $res;
    }


    public static function TextCard_delete($id)
    {
        $query = DB::table('sentences')->where('Sentences_id', $id)->delete();
        return $query;

    }

    public static function load_Adv($id)
    {
        $query = DB::table('sentences')->where('Sentences_id', $id)->get();
        foreach ($query as $row) {
            $data = array(
                'Sentences_id' => $row->Sentences_id,
                'Sentences_title' => $row->Sentences_title
            );
            return $data;

        }

    }

    public  static function TextCard_edit_modal($request)
    {
        $id = $request['sentence_ajax'];
        $data = array(
            'Sentences_title' => $request['sentence'],
        );
        $query = DB::table('sentences')->where('Sentences_id', $id)->update($data);
        return $query;

    }

}
