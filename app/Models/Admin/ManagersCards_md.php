<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ManagersCards_md extends Model
{
    public static function LoadSelectCat()
    {
        $cat = DB::table('category')->get();
        $data = array();
        if (count($cat) > 0) {
            foreach ($cat as $R_Cat) {
                array_push($data, array(
                    'cat_id' => $R_Cat->cat_id,
                    'cat_name' => $R_Cat->cat_name,
                ));
            }
        }return $data;
    }

    public static function LoadSelectOca()
    {
        $oca = DB::table('occasion')->get();
        $data=array();
        if (count($oca) > 0) {
            foreach ($oca as $R_Oca) {
                array_push($data, array(
                    'occasion_id' => $R_Oca->occasion_id,
                    'occasion_text' => $R_Oca->occasion_text,
                ));
            }
        }return $data;
    }

    public static function LoadSelectYG()
    {
        $yg = DB::table('yeargroup')->get();
        $data=array();
        if (count($yg) > 0) {
            foreach ($yg as $R_Yg) {
                array_push($data, array(
                    'YearGroup_id' => $R_Yg->YearGroup_id,
                    'YearGroup_title' => $R_Yg->YearGroup_title,
                ));
            }
        }
        return $data;
    }

    public static function load_Cards()
    {
        $cards = DB::table('cards')
            ->join('occasion', 'cards.card_occasion', '=', 'occasion.occasion_id')
            ->join('category', 'cards.card_category', '=', 'category.cat_id')
            ->join('yeargroup', 'cards.card_age_category', '=', 'yeargroup.YearGroup_id')
            ->get();
        $data = array();
        if (count($cards) > 0) {
            foreach ($cards as $count) {
                array_push($data, array(
                    'card_id' => $count->card_id,
                    'card_front_image' => $count->card_front_image,
                    'card_behind_image' => $count->card_behind_image,
                    'card_category' => $count->cat_name,
                    'card_occasion' => $count->occasion_text,
                    'card_age_category' => $count->YearGroup_title,
                    'card_title' => $count->card_title,
                    'card_sexuality' => $count->card_sexuality,
                ));
            }

        }
        return $data;
    }

    public static function DataCard($card_id)
    {
        $res = DB::table('cards')->where('card_id', $card_id)
        ->join('occasion', 'cards.card_occasion', '=', 'occasion.occasion_id')
        ->join('category', 'cards.card_category', '=', 'category.cat_id')
        ->join('yeargroup', 'cards.card_age_category', '=', 'yeargroup.YearGroup_id')
        ->get();
        $data=array(
                    'card_id' => $res[0]->card_id,
                    'card_front_image' => $res[0]->card_front_image,
                    'card_behind_image' => $res[0]->card_behind_image,
                    'card_category' => $res[0]->cat_name,
                    'card_category_id' => $res[0]->cat_id,
                    'card_occasion' => $res[0]->occasion_text,
                    'card_occasion_id' => $res[0]->occasion_id,
                    'card_age_category' => $res[0]->YearGroup_title,
                    'card_age_category_id' => $res[0]->YearGroup_id,
                    'card_title' => $res[0]->card_title,
                    'card_season' => $res[0]->card_season,
                    'card_sexuality' => $res[0]->card_sexuality,
        );
        return $data;
    }

    public static function UPDataCard($data,$card_id)
    {
        $res = DB::table('cards')->where('card_id', $card_id)->update($data);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    public static function InsertCard($data)
    {
        $res = DB::table('cards')->insert($data);
        if (is_null($res)) {
            return 'false';
        } else {
            return 'true';
        }
    }

    public static function DeleteCard ($card_id)
    {
        $res = DB::table('cards')->where('card_id', $card_id)->delete();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
}
