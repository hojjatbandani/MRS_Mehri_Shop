<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class List_transaction_md extends Model
{
    public static function load_transaction()
    {
        $list = DB::table('transactions')
            ->join('users', 'transactions.user_id', '=', 'users.user_id')
            ->join('orders', 'transactions.order_id', '=', 'orders.Orders_id')
            ->get();
        $data = array();
        if (count($list) > 0) {
            foreach ($list as $count) {
                array_push($data, array(
                    'transaction_id' => $count->transaction_id,
                    'user_name' => $count->user_name,
                    'user_family' => $count->user_family,
                    'Orders_cardtext' => $count->Orders_cardtext,
                    'Orders_send_type' => $count->Orders_send_type,
                    'Orders_price_card' => $count->Orders_price_card,
                    'transaction_amount' => $count->transaction_amount,
                    'authorycode_refid' => $count->authorycode_refid,
                    'transantion_date' => $count->transantion_date,
                    'transaction_type' => $count->transantion_type
                ));
            }
        }
        return $data;
    }

}
