<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ContactUs_md extends Model
{
    public static function loadContact()
    {
        $res = DB::table('contact_us')->get();
        $data = array();
        if (count($res) > 0) {
            $data = array(
                'contact_id' => $res[0]->contact_id,
                'contact_text' => $res[0]->contact_text
            );
        }
        return $data;
    }

    public static function EditContact($id, $text)
    {
        $res = DB::table('contact_us')
            ->where('contact_id', $id)
            ->update(['contact_text' => $text]);
        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function GetData($id)
    {
        $res = DB::table('contact_us')->where('contact_id', $id)->get();
          $data= array(
                'contact_id' => $res[0]->contact_id,
                'contact_text' => $res[0]->contact_text
            );
        return $data;
    }
}
