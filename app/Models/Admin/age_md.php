<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class age_md extends Model
{

    public static function load_age()
    {
        $query = DB::table('yeargroup')->get();
        return $query;
    }

    public static function add_age($data)
    {


        $res = DB::table('yeargroup')->insert($data);
        return $res;
    }

    public static function load_category()
    {
        $res = DB::table('category')->get();
        return $res;
    }

    public static function delete_age($id)
    {
        $query = DB::table('yeargroup')->where('yeargroup_id', $id)->delete();

        return $query;
    }

    public static function load_child($id)
    {
        $query = DB::table('category')->where('cat_parent_id', $id)->get();
        $str = '';
        foreach ($query as $row) {
            $str .= "<option value=" . $row->cat_id . ">" . $row->cat_name . "</option>";
        }
        return $str;
    }

    public static function EditCat($id, $data)
    {
        $res = DB::table('yeargroup')
            ->where('YearGroup_id', $id)
            ->update($data);
        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function AgeData($id)
    {
        $res = DB::table('yeargroup')->where('yeargroup_id', $id)
            ->get();
        $data=array(
            'YearGroup_id' => $res[0]->YearGroup_id,
            'YearGroup_title' => $res[0]->YearGroup_title,
        );
        return $data;
    }

//
//    public static function load_transaction()
//    {
//        $query = DB::table('transactions')->where('transaction_type',1)->sum('transaction_amount');
//        return $query;
//    }
//
//
//    public static function load_orders()
//    {
//        $query = DB::table('orders')->count('Orders_id');
//        return $query;
//    }
}
