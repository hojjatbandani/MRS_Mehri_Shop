<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class List_orders_md extends Model
{
    public static function load_orders()
    {
       $query= DB::table('orders')
            ->join('cards', 'cards.card_id', '=', 'orders.card_id')
            ->join('category', 'category.cat_id', '=', 'orders.cat_id')
            ->get();
       $data=array();
        if(count($query)>0){
            foreach ($query as $count){
                array_push($data,array(
                    'Orders_id'=>$count->Orders_id,
                    'cat_name'=>$count->cat_name,
                    'Orders_cardtext'=>$count->Orders_cardtext,
                    'Orders_receiver_phone'=>$count->Orders_receiver_phone,
                    'Orders_receiver_address'=>$count->Orders_receiver_address,
                    'Orders_send_type'=>$count->Orders_send_type
                ));
            }
        }
        return$data;
    }

    public static function DeleteOrders($id)
    {
        $res = DB::table('orders')->where('Orders_id', $id)->delete();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
}
