<?php

namespace App\Models\Admin;

use  Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class dashboard_md extends Model
{

    public static function load_dashboard()
    {
        $res = DB::table('users')->count('user_id');
        return $res;
    }

    public static function load_transaction()
    {
        $query = DB::table('transactions')->where('transantion_type',1)->sum('transaction_amount');
        return $query;
    }
   public static function load_cat()
    {
        $query = DB::table('category')->where('cat_parent_id',0)->count('cat_id');
        return $query;
    }

    public static function load_orders()
    {
        $query = DB::table('orders')->count('Orders_id');
        return $query;
    }
}
